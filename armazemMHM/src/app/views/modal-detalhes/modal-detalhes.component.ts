import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-modal-detalhes',
  templateUrl: './modal-detalhes.component.html',
  styleUrls: ['./modal-detalhes.component.css']
})
export class ModalDetalhesComponent {
  pageTitle: string = "";
  BagsAla: any[] = [];
  lotes: any;
  isLoading: boolean = false;

  constructor(private _apiService: ApiService, @Inject(MAT_DIALOG_DATA) private _data: any) {
    this.pageTitle = _data.ala;

    this.isLoading = true;

    this._apiService.getByConsultaLote(_data.ala).then(res => {
      this.BagsAla = res;
      this.isLoading = false;
      this.MostraModal();
    }, reject => {
      this.isLoading = false;
      console.log(reject)
    });

  }

  MostraModal() {
    this.lotes = [...new Set(this.BagsAla.map(x => x))];

    this.lotes = this.lotes.map((x: any) => {
      return {
        lote: x.lote,
        saldo_kg: x.saldo_kg,
        qtd_bag: x.qtd_bag,
        saldo_scs: x.saldo_scs,
        qtd_fracionado: x.qtd_fracionado
      }
    });
  }


}
