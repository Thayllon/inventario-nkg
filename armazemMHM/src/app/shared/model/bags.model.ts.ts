type ordens = {
    idOs: number;
    cliente: string;
    qtdKgsBag: number;
}

export class Bag {
    idEndereco: number;
    altura?: number;
    // titular?: string;
    // propriedade?: string;
    // dataEntradaLote?: any;
    lote?: string;
    // saldoBag?: number;
    // saldoBagReservado?: number;
    // ordens?: ordens[];
    // padrao?: string;
    certificacao?: string;
    // situacaoBag?: string;
    constructor(value?: any) {
        value = value || {};
        this.idEndereco = value.idEndereco || 0;
        this.altura = value.altura || 0;
        // this.titular = value.titular || '';
        // this.propriedade = value.propriedade || '';
        // this.dataEntradaLote = value.dataEntradaLote || '';
        this.lote = value.lote || '';
        // this.saldoBag = value.saldoBag || '';
        // this.saldoBagReservado = value.saldoBagReservado || '';
        // this.padrao = value.padrao || '';
        this.certificacao = value.certificacao || '';
        // this.situacaoBag = value.situacaoBag || '';
        // this.ordens = value.ordens;
    }
}
