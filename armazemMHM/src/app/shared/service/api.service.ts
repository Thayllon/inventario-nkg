import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private HttpClient: HttpClient
  ) { }

  //traz todas as localizações
  getAll(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/endereco`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  getByOne(idend: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/endereco?idend=${idend}`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }


  getByConsultaLote(ala: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/numlote/consulta-lotes-ala?ala=${ala}`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  getByConsultaResumoAla(ala: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/numlote/consulta-resumo-ala?ala=${ala}`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  getByLote(lote: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/numlote/por-lote?lote=${lote}`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  getByExport(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/numlote/log-inventario`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  getByBoxRec(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/caixas?tipo=1`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  getByBoxLiga(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.get(`${environment.apiEndereco}/caixas?tipo=2`).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  postValidacaoCaixa(id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.post(`${environment.apiEndereco}/caixas/confirmar/${id}`, null).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  postInvalidarCaixa(id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.post(`${environment.apiEndereco}/caixas/invalidar/${id}`, null).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  //update status endereco para 20
  postValidacaoAla(id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.post(`${environment.apiEndereco}/endereco/confirmar/${id}`, null).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  //update status endereco para 1
  postInvalidarAla(id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.post(`${environment.apiEndereco}/endereco/invalidar/${id}`, null).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }

  //insert logInventario
  postSalvarValidacaoAla(obj: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.post(`${environment.apiEndereco}/numlote/log-inventario/`, obj).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }


  //invalidar logInventario
  postInvalidarAlaLogInventario(obj: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.HttpClient.post(`${environment.apiEndereco}/numlote/inativar-log-inventario?alaInvent=${obj}`, null).subscribe((response: any) => {
        // this.Arraydata = response;
        // this.onDataChanged.next(this.Arraydata);                
        resolve(response);
      }, reject);
    });
  }
}

