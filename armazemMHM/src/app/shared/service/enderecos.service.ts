import { Mapa } from "../model/mapa.model";

export class MapaService {

    data: Mapa[] = [
        //Matriz 8
        {
            "coordx": 1,
            "coordy": 1,
            "matriz": 8,
            "idend": 100000,
            "ala": "1X25",
            "bloco": "C",
            "status": 1,
            "altura": 0
        },
        // //BLOCOS AMARELOS   
        {
            "coordx": 5,
            "coordy": 1,
            "matriz": 1,
            "idend": 1,
            "ala": "2R34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 2,
            "matriz": 1,
            "idend": 2,
            "ala": "2R33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 3,
            "matriz": 1,
            "idend": 3,
            "ala": "2R32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 4,
            "matriz": 1,
            "idend": 4,
            "ala": "2R31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 5,
            "matriz": 1,
            "idend": 5,
            "ala": "2R30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 6,
            "matriz": 1,
            "idend": 6,
            "ala": "2R29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 7,
            "matriz": 1,
            "idend": 7,
            "ala": "2R28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 8,
            "matriz": 1,
            "idend": 8,
            "ala": "2R27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 9,
            "matriz": 1,
            "idend": 9,
            "ala": "2R26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 10,
            "matriz": 1,
            "idend": 10,
            "ala": "2R25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 11,
            "matriz": 1,
            "idend": 11,
            "ala": "2R24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 12,
            "matriz": 1,
            "idend": 12,
            "ala": "2R23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 13,
            "matriz": 1,
            "idend": 13,
            "ala": "2R22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 14,
            "matriz": 1,
            "idend": 14,
            "ala": "2R21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 15,
            "matriz": 1,
            "idend": 15,
            "ala": "2R20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 31,
            "matriz": 1,
            "idend": 16,
            "ala": "2R19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 32,
            "matriz": 1,
            "idend": 17,
            "ala": "2R18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 33,
            "matriz": 1,
            "idend": 18,
            "ala": "2R17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 34,
            "matriz": 1,
            "idend": 19,
            "ala": "2R16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 35,
            "matriz": 1,
            "idend": 20,
            "ala": "2R15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 1,
            "matriz": 1,
            "idend": 21,
            "ala": "2R35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 2,
            "matriz": 1,
            "idend": 22,
            "ala": "2R36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 3,
            "matriz": 1,
            "idend": 23,
            "ala": "2R37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 4,
            "matriz": 1,
            "idend": 24,
            "ala": "2R38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 5,
            "matriz": 1,
            "idend": 25,
            "ala": "2R39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 6,
            "matriz": 1,
            "idend": 26,
            "ala": "2R40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 7,
            "matriz": 1,
            "idend": 27,
            "ala": "2R41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 8,
            "matriz": 1,
            "idend": 28,
            "ala": "2R42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 9,
            "matriz": 1,
            "idend": 29,
            "ala": "2R43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 10,
            "matriz": 1,
            "idend": 30,
            "ala": "2R44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 11,
            "matriz": 1,
            "idend": 31,
            "ala": "2R45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 30,
            "matriz": 1,
            "idend": 32,
            "ala": "2R9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 31,
            "matriz": 1,
            "idend": 33,
            "ala": "2R10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 32,
            "matriz": 1,
            "idend": 34,
            "ala": "2R11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 33,
            "matriz": 1,
            "idend": 35,
            "ala": "2R12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 34,
            "matriz": 1,
            "idend": 36,
            "ala": "2R13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 35,
            "matriz": 1,
            "idend": 37,
            "ala": "2R14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçcar Matriz 1
        {
            "coordx": 1,
            "coordy": 36,
            "matriz": 1,
            "idend": 1000,
            "ala": "2R20",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 1,
            "matriz": 1,
            "idend": 1001,
            "ala": "2R20",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        //Matriz 2
        {
            "coordx": 1,
            "coordy": 7,
            "matriz": 2,
            "idend": 1,
            "ala": "2A40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 7,
            "matriz": 2,
            "idend": 2,
            "ala": "2A41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 7,
            "matriz": 2,
            "idend": 3,
            "ala": "2A42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 7,
            "matriz": 2,
            "idend": 4,
            "ala": "2A43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 7,
            "matriz": 2,
            "idend": 5,
            "ala": "2A44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 7,
            "matriz": 2,
            "idend": 6,
            "ala": "2A45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 7,
            "matriz": 2,
            "idend": 7,
            "ala": "2A46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 7,
            "matriz": 2,
            "idend": 8,
            "ala": "2A47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 7,
            "matriz": 2,
            "idend": 9,
            "ala": "2A48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 7,
            "matriz": 2,
            "idend": 10,
            "ala": "2A49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 7,
            "matriz": 2,
            "idend": 11,
            "ala": "2A50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 7,
            "matriz": 2,
            "idend": 12,
            "ala": "2A51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 7,
            "matriz": 2,
            "idend": 13,
            "ala": "2A52",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 7,
            "matriz": 2,
            "idend": 14,
            "ala": "2A1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 7,
            "matriz": 2,
            "idend": 15,
            "ala": "2A2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 7,
            "matriz": 2,
            "idend": 16,
            "ala": "2A3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 7,
            "matriz": 2,
            "idend": 17,
            "ala": "2A4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 7,
            "matriz": 2,
            "idend": 18,
            "ala": "2A5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 7,
            "matriz": 2,
            "idend": 19,
            "ala": "2A6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 7,
            "matriz": 2,
            "idend": 20,
            "ala": "2A7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 7,
            "matriz": 2,
            "idend": 21,
            "ala": "2A8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 7,
            "matriz": 2,
            "idend": 22,
            "ala": "2A9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 7,
            "matriz": 2,
            "idend": 23,
            "ala": "2A10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 7,
            "matriz": 2,
            "idend": 24,
            "ala": "2A11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 7,
            "matriz": 2,
            "idend": 25,
            "ala": "2A12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 7,
            "matriz": 2,
            "idend": 26,
            "ala": "2A13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 30,
            "coordy": 7,
            "matriz": 2,
            "idend": 27,
            "ala": "2A14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 11,
            "matriz": 2,
            "idend": 28,
            "ala": "2A39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 11,
            "matriz": 2,
            "idend": 29,
            "ala": "2A38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 11,
            "matriz": 2,
            "idend": 30,
            "ala": "2A37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 11,
            "matriz": 2,
            "idend": 31,
            "ala": "2A36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 11,
            "matriz": 2,
            "idend": 32,
            "ala": "2A35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 11,
            "matriz": 2,
            "idend": 33,
            "ala": "2A34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 11,
            "matriz": 2,
            "idend": 34,
            "ala": "2A33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 11,
            "matriz": 2,
            "idend": 35,
            "ala": "2A32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 11,
            "matriz": 2,
            "idend": 36,
            "ala": "2A31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 11,
            "matriz": 2,
            "idend": 37,
            "ala": "2A30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 11,
            "matriz": 2,
            "idend": 38,
            "ala": "2A29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 11,
            "matriz": 2,
            "idend": 39,
            "ala": "2A28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 11,
            "matriz": 2,
            "idend": 40,
            "ala": "2A53",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 11,
            "matriz": 2,
            "idend": 41,
            "ala": "2A27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 11,
            "matriz": 2,
            "idend": 42,
            "ala": "2A26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 11,
            "matriz": 2,
            "idend": 43,
            "ala": "2A25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 11,
            "matriz": 2,
            "idend": 44,
            "ala": "2A24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 11,
            "matriz": 2,
            "idend": 45,
            "ala": "2A23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 11,
            "matriz": 2,
            "idend": 46,
            "ala": "2A22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 11,
            "matriz": 2,
            "idend": 47,
            "ala": "2A21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 11,
            "matriz": 2,
            "idend": 48,
            "ala": "2A20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 11,
            "matriz": 2,
            "idend": 49,
            "ala": "2A19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 11,
            "matriz": 2,
            "idend": 50,
            "ala": "2A18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 11,
            "matriz": 2,
            "idend": 51,
            "ala": "2A17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 11,
            "matriz": 2,
            "idend": 52,
            "ala": "2A16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 11,
            "matriz": 2,
            "idend": 53,
            "ala": "2A15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 26,
            "matriz": 2,
            "idend": 68,
            "ala": "2B1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 26,
            "matriz": 2,
            "idend": 69,
            "ala": "2B2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 26,
            "matriz": 2,
            "idend": 70,
            "ala": "2B3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 26,
            "matriz": 2,
            "idend": 71,
            "ala": "2B4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 26,
            "matriz": 2,
            "idend": 72,
            "ala": "2B5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 26,
            "matriz": 2,
            "idend": 73,
            "ala": "2B6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 26,
            "matriz": 2,
            "idend": 74,
            "ala": "2B7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 26,
            "matriz": 2,
            "idend": 75,
            "ala": "2B8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 26,
            "matriz": 2,
            "idend": 76,
            "ala": "2B9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 26,
            "matriz": 2,
            "idend": 77,
            "ala": "2B10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 26,
            "matriz": 2,
            "idend": 78,
            "ala": "2B11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 26,
            "matriz": 2,
            "idend": 79,
            "ala": "2B12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 26,
            "matriz": 2,
            "idend": 54,
            "ala": "2B39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 26,
            "matriz": 2,
            "idend": 55,
            "ala": "2B13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 26,
            "matriz": 2,
            "idend": 56,
            "ala": "2B14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 26,
            "matriz": 2,
            "idend": 57,
            "ala": "2B15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 26,
            "matriz": 2,
            "idend": 58,
            "ala": "2B16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 26,
            "matriz": 2,
            "idend": 59,
            "ala": "2B17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 26,
            "matriz": 2,
            "idend": 60,
            "ala": "2B18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 26,
            "matriz": 2,
            "idend": 61,
            "ala": "2B19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 26,
            "matriz": 2,
            "idend": 62,
            "ala": "2B20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 26,
            "matriz": 2,
            "idend": 63,
            "ala": "2B21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 26,
            "matriz": 2,
            "idend": 64,
            "ala": "2B22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 26,
            "matriz": 2,
            "idend": 65,
            "ala": "2B23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 25,
            "matriz": 2,
            "idend": 66,
            "ala": "2B24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 25,
            "matriz": 2,
            "idend": 67,
            "ala": "2B25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar Matriz 2
        {
            "coordx": 1,
            "coordy": 27,
            "matriz": 2,
            "idend": 1000,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        {
            "coordx": 31,
            "coordy": 1,
            "matriz": 2,
            "idend": 1001,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        //Matriz 3
        {
            "coordx": 8,
            "coordy": 2,
            "matriz": 3,
            "idend": 1,
            "ala": "2R1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 3,
            "matriz": 3,
            "idend": 2,
            "ala": "2R2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 6,
            "matriz": 3,
            "idend": 3,
            "ala": "2R3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 7,
            "matriz": 3,
            "idend": 4,
            "ala": "2R4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 9,
            "matriz": 3,
            "idend": 5,
            "ala": "2R5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 10,
            "matriz": 3,
            "idend": 6,
            "ala": "2R6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },

        {
            "coordx": 8,
            "coordy": 13,
            "matriz": 3,
            "idend": 7,
            "ala": "2R7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 14,
            "matriz": 3,
            "idend": 8,
            "ala": "2R8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 19,
            "matriz": 3,
            "idend": 9,
            "ala": "2C1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 19,
            "matriz": 3,
            "idend": 10,
            "ala": "2C2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 19,
            "matriz": 3,
            "idend": 11,
            "ala": "2C3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 19,
            "matriz": 3,
            "idend": 12,
            "ala": "2C4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 19,
            "matriz": 3,
            "idend": 13,
            "ala": "2C5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 19,
            "matriz": 3,
            "idend": 14,
            "ala": "2C6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 19,
            "matriz": 3,
            "idend": 15,
            "ala": "2C7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 19,
            "matriz": 3,
            "idend": 16,
            "ala": "2C8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 19,
            "matriz": 3,
            "idend": 17,
            "ala": "2C9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 19,
            "matriz": 3,
            "idend": 18,
            "ala": "2C10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 19,
            "matriz": 3,
            "idend": 19,
            "ala": "2C11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 19,
            "matriz": 3,
            "idend": 20,
            "ala": "2C12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 35,
            "matriz": 3,
            "idend": 21,
            "ala": "2D38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 35,
            "matriz": 3,
            "idend": 22,
            "ala": "2D39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 35,
            "matriz": 3,
            "idend": 23,
            "ala": "2D40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 35,
            "matriz": 3,
            "idend": 24,
            "ala": "2D41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 35,
            "matriz": 3,
            "idend": 25,
            "ala": "2D42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 35,
            "matriz": 3,
            "idend": 26,
            "ala": "2D43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 35,
            "matriz": 3,
            "idend": 27,
            "ala": "2D44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 35,
            "matriz": 3,
            "idend": 28,
            "ala": "2D45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 35,
            "matriz": 3,
            "idend": 29,
            "ala": "2D46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 35,
            "matriz": 3,
            "idend": 30,
            "ala": "2D47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 35,
            "matriz": 3,
            "idend": 31,
            "ala": "2D48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 35,
            "matriz": 3,
            "idend": 32,
            "ala": "2D49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 39,
            "matriz": 3,
            "idend": 33,
            "ala": "2D37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 39,
            "matriz": 3,
            "idend": 34,
            "ala": "2D36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 39,
            "matriz": 3,
            "idend": 35,
            "ala": "2D35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 39,
            "matriz": 3,
            "idend": 36,
            "ala": "2D34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 39,
            "matriz": 3,
            "idend": 37,
            "ala": "2D33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 39,
            "matriz": 3,
            "idend": 38,
            "ala": "2D32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 39,
            "matriz": 3,
            "idend": 39,
            "ala": "2D31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 39,
            "matriz": 3,
            "idend": 40,
            "ala": "2D30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 39,
            "matriz": 3,
            "idend": 41,
            "ala": "2D29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 39,
            "matriz": 3,
            "idend": 42,
            "ala": "2D28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 39,
            "matriz": 3,
            "idend": 43,
            "ala": "2D27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 39,
            "matriz": 3,
            "idend": 44,
            "ala": "2D26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 0,
            "matriz": 3,
            "idend": 45,
            "ala": "2B38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 0,
            "matriz": 3,
            "idend": 46,
            "ala": "2B37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 0,
            "matriz": 3,
            "idend": 47,
            "ala": "2B36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 0,
            "matriz": 3,
            "idend": 48,
            "ala": "2B35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 0,
            "matriz": 3,
            "idend": 49,
            "ala": "2B34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 0,
            "matriz": 3,
            "idend": 50,
            "ala": "2B33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 0,
            "matriz": 3,
            "idend": 51,
            "ala": "2B32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 0,
            "matriz": 3,
            "idend": 52,
            "ala": "2B31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 0,
            "matriz": 3,
            "idend": 53,
            "ala": "2B30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 0,
            "matriz": 3,
            "idend": 54,
            "ala": "2B29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 0,
            "matriz": 3,
            "idend": 55,
            "ala": "2B28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 0,
            "matriz": 3,
            "idend": 56,
            "ala": "2B27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 0,
            "matriz": 3,
            "idend": 57,
            "ala": "2B26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 15,
            "matriz": 3,
            "idend": 58,
            "ala": "2C37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 15,
            "matriz": 3,
            "idend": 59,
            "ala": "2C36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 15,
            "matriz": 3,
            "idend": 60,
            "ala": "2C35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 15,
            "matriz": 3,
            "idend": 61,
            "ala": "2C34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 15,
            "matriz": 3,
            "idend": 62,
            "ala": "2C33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 15,
            "matriz": 3,
            "idend": 63,
            "ala": "2C32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 15,
            "matriz": 3,
            "idend": 64,
            "ala": "2C31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 15,
            "matriz": 3,
            "idend": 65,
            "ala": "2C30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 15,
            "matriz": 3,
            "idend": 66,
            "ala": "2C29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 15,
            "matriz": 3,
            "idend": 67,
            "ala": "2C28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 15,
            "matriz": 3,
            "idend": 68,
            "ala": "2C27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 15,
            "matriz": 3,
            "idend": 69,
            "ala": "2C26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 15,
            "matriz": 3,
            "idend": 70,
            "ala": "2C25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 19,
            "matriz": 3,
            "idend": 71,
            "ala": "2C38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 19,
            "matriz": 3,
            "idend": 72,
            "ala": "2C13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 19,
            "matriz": 3,
            "idend": 73,
            "ala": "2C14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 19,
            "matriz": 3,
            "idend": 74,
            "ala": "2C15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 19,
            "matriz": 3,
            "idend": 75,
            "ala": "2C16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 19,
            "matriz": 3,
            "idend": 76,
            "ala": "2C17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 19,
            "matriz": 3,
            "idend": 77,
            "ala": "2C18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 19,
            "matriz": 3,
            "idend": 78,
            "ala": "2C19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 19,
            "matriz": 3,
            "idend": 79,
            "ala": "2C20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 19,
            "matriz": 3,
            "idend": 80,
            "ala": "2C21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 19,
            "matriz": 3,
            "idend": 81,
            "ala": "2C22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 19,
            "matriz": 3,
            "idend": 82,
            "ala": "2C23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 19,
            "matriz": 3,
            "idend": 83,
            "ala": "2C24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 35,
            "matriz": 3,
            "idend": 84,
            "ala": "2D50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 35,
            "matriz": 3,
            "idend": 85,
            "ala": "2D1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 35,
            "matriz": 3,
            "idend": 86,
            "ala": "2D2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 35,
            "matriz": 3,
            "idend": 87,
            "ala": "2D3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 35,
            "matriz": 3,
            "idend": 88,
            "ala": "2D4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 35,
            "matriz": 3,
            "idend": 89,
            "ala": "2D5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 35,
            "matriz": 3,
            "idend": 90,
            "ala": "2D6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 35,
            "matriz": 3,
            "idend": 91,
            "ala": "2D7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 35,
            "matriz": 3,
            "idend": 92,
            "ala": "2D8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 35,
            "matriz": 3,
            "idend": 93,
            "ala": "2D9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 35,
            "matriz": 3,
            "idend": 94,
            "ala": "2D10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 35,
            "matriz": 3,
            "idend": 95,
            "ala": "2D11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 35,
            "matriz": 3,
            "idend": 96,
            "ala": "2D12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 39,
            "matriz": 3,
            "idend": 97,
            "ala": "2D51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 39,
            "matriz": 3,
            "idend": 98,
            "ala": "2D25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 39,
            "matriz": 3,
            "idend": 99,
            "ala": "2D24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 39,
            "matriz": 3,
            "idend": 100,
            "ala": "2D23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 39,
            "matriz": 3,
            "idend": 101,
            "ala": "2D22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 39,
            "matriz": 3,
            "idend": 102,
            "ala": "2D21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 39,
            "matriz": 3,
            "idend": 103,
            "ala": "2D20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 39,
            "matriz": 3,
            "idend": 104,
            "ala": "2D19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 39,
            "matriz": 3,
            "idend": 105,
            "ala": "2D18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 39,
            "matriz": 3,
            "idend": 106,
            "ala": "2D17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 39,
            "matriz": 3,
            "idend": 107,
            "ala": "2D16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 39,
            "matriz": 3,
            "idend": 108,
            "ala": "2D15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 39,
            "matriz": 3,
            "idend": 109,
            "ala": "2D14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 39,
            "matriz": 3,
            "idend": 110,
            "ala": "2D13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar Matriz 3
        {
            "coordx": 1,
            "coordy": 46,
            "matriz": 3,
            "idend": 1000,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 0,
            "matriz": 3,
            "idend": 1001,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        {
            "coordx": 30,
            "coordy": 39,
            "matriz": 3,
            "idend": 1002,
            "ala": "2R20",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        //Matriz 4
        {
            "coordx": 1,
            "coordy": 1,
            "matriz": 4,
            "idend": 1,
            "ala": "2F1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 2,
            "matriz": 4,
            "idend": 2,
            "ala": "2F2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 3,
            "matriz": 4,
            "idend": 3,
            "ala": "2F3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 4,
            "matriz": 4,
            "idend": 4,
            "ala": "2F4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 5,
            "matriz": 4,
            "idend": 5,
            "ala": "2F5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 6,
            "matriz": 4,
            "idend": 6,
            "ala": "2F6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 1,
            "matriz": 4,
            "idend": 7,
            "ala": "2E1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 2,
            "matriz": 4,
            "idend": 8,
            "ala": "2E2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 3,
            "matriz": 4,
            "idend": 9,
            "ala": "2E3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 4,
            "matriz": 4,
            "idend": 10,
            "ala": "2E4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 5,
            "matriz": 4,
            "idend": 11,
            "ala": "2E5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 6,
            "matriz": 4,
            "idend": 12,
            "ala": "2E6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 1,
            "matriz": 4,
            "idend": 13,
            "ala": "2E12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 2,
            "matriz": 4,
            "idend": 14,
            "ala": "2E11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 3,
            "matriz": 4,
            "idend": 15,
            "ala": "2E10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 4,
            "matriz": 4,
            "idend": 16,
            "ala": "2E9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 5,
            "matriz": 4,
            "idend": 17,
            "ala": "2E8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 6,
            "matriz": 4,
            "idend": 18,
            "ala": "2E7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 1,
            "matriz": 4,
            "idend": 19,
            "ala": "2G1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 2,
            "matriz": 4,
            "idend": 20,
            "ala": "2G2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 3,
            "matriz": 4,
            "idend": 21,
            "ala": "2G3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 4,
            "matriz": 4,
            "idend": 22,
            "ala": "2G4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 5,
            "matriz": 4,
            "idend": 23,
            "ala": "2G5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 1,
            "matriz": 4,
            "idend": 24,
            "ala": "2G10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 2,
            "matriz": 4,
            "idend": 25,
            "ala": "2G9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 3,
            "matriz": 4,
            "idend": 26,
            "ala": "2G8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 4,
            "matriz": 4,
            "idend": 27,
            "ala": "2G7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 5,
            "matriz": 4,
            "idend": 28,
            "ala": "2G6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 48,
            "coordy": 1,
            "matriz": 4,
            "idend": 29,
            "ala": "1G10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 48,
            "coordy": 2,
            "matriz": 4,
            "idend": 30,
            "ala": "1G9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 48,
            "coordy": 3,
            "matriz": 4,
            "idend": 31,
            "ala": "1G8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 48,
            "coordy": 4,
            "matriz": 4,
            "idend": 32,
            "ala": "1G7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 48,
            "coordy": 5,
            "matriz": 4,
            "idend": 33,
            "ala": "1G6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 52,
            "coordy": 1,
            "matriz": 4,
            "idend": 34,
            "ala": "1G1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 51,
            "coordy": 2,
            "matriz": 4,
            "idend": 35,
            "ala": "1G2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 51,
            "coordy": 3,
            "matriz": 4,
            "idend": 36,
            "ala": "1G3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 51,
            "coordy": 4,
            "matriz": 4,
            "idend": 37,
            "ala": "1G4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 52,
            "coordy": 5,
            "matriz": 4,
            "idend": 38,
            "ala": "1G5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 66,
            "coordy": 1,
            "matriz": 4,
            "idend": 39,
            "ala": "1E9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 66,
            "coordy": 2,
            "matriz": 4,
            "idend": 40,
            "ala": "1E8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 66,
            "coordy": 3,
            "matriz": 4,
            "idend": 41,
            "ala": "1E7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 66,
            "coordy": 4,
            "matriz": 4,
            "idend": 42,
            "ala": "1E6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 64,
            "coordy": 5,
            "matriz": 4,
            "idend": 43,
            "ala": "1E5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 68,
            "coordy": 1,
            "matriz": 4,
            "idend": 44,
            "ala": "1E1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 68,
            "coordy": 2,
            "matriz": 4,
            "idend": 45,
            "ala": "1E2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 68,
            "coordy": 3,
            "matriz": 4,
            "idend": 46,
            "ala": "1E3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 68,
            "coordy": 4,
            "matriz": 4,
            "idend": 47,
            "ala": "1E4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 83,
            "coordy": 1,
            "matriz": 4,
            "idend": 48,
            "ala": "1F1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 83,
            "coordy": 2,
            "matriz": 4,
            "idend": 49,
            "ala": "1F2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 83,
            "coordy": 3,
            "matriz": 4,
            "idend": 50,
            "ala": "1F3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 83,
            "coordy": 4,
            "matriz": 4,
            "idend": 51,
            "ala": "1F4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar Matriz 4
        {
            "coordx": 1,
            "coordy": 7,
            "matriz": 4,
            "idend": 1000,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        {
            "coordx": 84,
            "coordy": 1,
            "matriz": 4,
            "idend": 1001,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        //Matriz 5
        {
            "coordx": 1,
            "coordy": 0,
            "matriz": 5,
            "idend": 1,
            "ala": "1B37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 0,
            "matriz": 5,
            "idend": 2,
            "ala": "1B36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 0,
            "matriz": 5,
            "idend": 3,
            "ala": "1B35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 0,
            "matriz": 5,
            "idend": 4,
            "ala": "1B34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 0,
            "matriz": 5,
            "idend": 5,
            "ala": "1B33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 0,
            "matriz": 5,
            "idend": 6,
            "ala": "1B32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 0,
            "matriz": 5,
            "idend": 7,
            "ala": "1B31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 0,
            "matriz": 5,
            "idend": 8,
            "ala": "1B30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 0,
            "matriz": 5,
            "idend": 9,
            "ala": "1B29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 0,
            "matriz": 5,
            "idend": 10,
            "ala": "1B28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 0,
            "matriz": 5,
            "idend": 11,
            "ala": "1B27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 15,
            "matriz": 5,
            "idend": 12,
            "ala": "1C38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 15,
            "matriz": 5,
            "idend": 13,
            "ala": "1C39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 15,
            "matriz": 5,
            "idend": 14,
            "ala": "1C40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 15,
            "matriz": 5,
            "idend": 15,
            "ala": "1C41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 15,
            "matriz": 5,
            "idend": 16,
            "ala": "1C42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 15,
            "matriz": 5,
            "idend": 17,
            "ala": "1C43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 15,
            "matriz": 5,
            "idend": 18,
            "ala": "1C44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 15,
            "matriz": 5,
            "idend": 19,
            "ala": "1C45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 15,
            "matriz": 5,
            "idend": 20,
            "ala": "1C46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 15,
            "matriz": 5,
            "idend": 21,
            "ala": "1C47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 15,
            "matriz": 5,
            "idend": 22,
            "ala": "1C48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 19,
            "matriz": 5,
            "idend": 23,
            "ala": "1C37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 19,
            "matriz": 5,
            "idend": 24,
            "ala": "1C36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 19,
            "matriz": 5,
            "idend": 25,
            "ala": "1C35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 19,
            "matriz": 5,
            "idend": 26,
            "ala": "1C34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 19,
            "matriz": 5,
            "idend": 27,
            "ala": "1C33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 19,
            "matriz": 5,
            "idend": 28,
            "ala": "1C32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 19,
            "matriz": 5,
            "idend": 29,
            "ala": "1C31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 19,
            "matriz": 5,
            "idend": 30,
            "ala": "1C30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 19,
            "matriz": 5,
            "idend": 31,
            "ala": "1C29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 19,
            "matriz": 5,
            "idend": 32,
            "ala": "1C28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 19,
            "matriz": 5,
            "idend": 33,
            "ala": "1C27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 35,
            "matriz": 5,
            "idend": 34,
            "ala": "1D39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 35,
            "matriz": 5,
            "idend": 35,
            "ala": "1D40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 35,
            "matriz": 5,
            "idend": 36,
            "ala": "1D41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 35,
            "matriz": 5,
            "idend": 37,
            "ala": "1D42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 35,
            "matriz": 5,
            "idend": 38,
            "ala": "1D43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 35,
            "matriz": 5,
            "idend": 39,
            "ala": "1D44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 35,
            "matriz": 5,
            "idend": 40,
            "ala": "1D45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 35,
            "matriz": 5,
            "idend": 41,
            "ala": "1D46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 35,
            "matriz": 5,
            "idend": 42,
            "ala": "1D47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 35,
            "matriz": 5,
            "idend": 43,
            "ala": "1D48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 35,
            "matriz": 5,
            "idend": 44,
            "ala": "1D49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 0,
            "coordy": 39,
            "matriz": 5,
            "idend": 45,
            "ala": "1D38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 39,
            "matriz": 5,
            "idend": 46,
            "ala": "1D37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 39,
            "matriz": 5,
            "idend": 47,
            "ala": "1D36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 39,
            "matriz": 5,
            "idend": 48,
            "ala": "1D35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 39,
            "matriz": 5,
            "idend": 49,
            "ala": "1D34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 39,
            "matriz": 5,
            "idend": 50,
            "ala": "1D33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 39,
            "matriz": 5,
            "idend": 51,
            "ala": "1D32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 39,
            "matriz": 5,
            "idend": 52,
            "ala": "1D31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 39,
            "matriz": 5,
            "idend": 53,
            "ala": "1D30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 39,
            "matriz": 5,
            "idend": 54,
            "ala": "1D29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 39,
            "matriz": 5,
            "idend": 55,
            "ala": "1D28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 39,
            "matriz": 5,
            "idend": 56,
            "ala": "1D27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 0,
            "matriz": 5,
            "idend": 57,
            "ala": "1B50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 0,
            "matriz": 5,
            "idend": 58,
            "ala": "1B26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 0,
            "matriz": 5,
            "idend": 59,
            "ala": "1B25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 0,
            "matriz": 5,
            "idend": 60,
            "ala": "1B24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 0,
            "matriz": 5,
            "idend": 61,
            "ala": "1B23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 0,
            "matriz": 5,
            "idend": 62,
            "ala": "1B22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 0,
            "matriz": 5,
            "idend": 63,
            "ala": "1B21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 0,
            "matriz": 5,
            "idend": 64,
            "ala": "1B20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 0,
            "matriz": 5,
            "idend": 65,
            "ala": "1B19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 0,
            "matriz": 5,
            "idend": 66,
            "ala": "1B18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 0,
            "matriz": 5,
            "idend": 67,
            "ala": "1B17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 0,
            "matriz": 5,
            "idend": 68,
            "ala": "1B16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 0,
            "matriz": 5,
            "idend": 69,
            "ala": "1B15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 0,
            "matriz": 5,
            "idend": 70,
            "ala": "1B14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 15,
            "matriz": 5,
            "idend": 71,
            "ala": "1C49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 15,
            "matriz": 5,
            "idend": 72,
            "ala": "1C1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 15,
            "matriz": 5,
            "idend": 73,
            "ala": "1C2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 15,
            "matriz": 5,
            "idend": 74,
            "ala": "1C3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 15,
            "matriz": 5,
            "idend": 75,
            "ala": "1C4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 15,
            "matriz": 5,
            "idend": 76,
            "ala": "1C5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 15,
            "matriz": 5,
            "idend": 77,
            "ala": "1C6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 15,
            "matriz": 5,
            "idend": 78,
            "ala": "1C7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 15,
            "matriz": 5,
            "idend": 79,
            "ala": "1C8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 15,
            "matriz": 5,
            "idend": 80,
            "ala": "1C9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 15,
            "matriz": 5,
            "idend": 81,
            "ala": "1C10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 15,
            "matriz": 5,
            "idend": 82,
            "ala": "1C11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 15,
            "matriz": 5,
            "idend": 83,
            "ala": "1C12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 15,
            "matriz": 5,
            "idend": 84,
            "ala": "1C13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 19,
            "matriz": 5,
            "idend": 85,
            "ala": "1C50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 19,
            "matriz": 5,
            "idend": 86,
            "ala": "1C26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 19,
            "matriz": 5,
            "idend": 87,
            "ala": "1C25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 19,
            "matriz": 5,
            "idend": 88,
            "ala": "1C24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 19,
            "matriz": 5,
            "idend": 89,
            "ala": "1C23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 19,
            "matriz": 5,
            "idend": 90,
            "ala": "1C22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 19,
            "matriz": 5,
            "idend": 91,
            "ala": "1C21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 19,
            "matriz": 5,
            "idend": 92,
            "ala": "1C20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 19,
            "matriz": 5,
            "idend": 93,
            "ala": "1C19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 19,
            "matriz": 5,
            "idend": 94,
            "ala": "1C18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 19,
            "matriz": 5,
            "idend": 95,
            "ala": "1C17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 19,
            "matriz": 5,
            "idend": 96,
            "ala": "1C16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 19,
            "matriz": 5,
            "idend": 97,
            "ala": "1C15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 19,
            "matriz": 5,
            "idend": 98,
            "ala": "1C14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 35,
            "matriz": 5,
            "idend": 99,
            "ala": "1D50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 35,
            "matriz": 5,
            "idend": 100,
            "ala": "1D1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 35,
            "matriz": 5,
            "idend": 101,
            "ala": "1D2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 35,
            "matriz": 5,
            "idend": 102,
            "ala": "1D3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 35,
            "matriz": 5,
            "idend": 103,
            "ala": "1D4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 35,
            "matriz": 5,
            "idend": 104,
            "ala": "1D5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 35,
            "matriz": 5,
            "idend": 105,
            "ala": "1D6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 35,
            "matriz": 5,
            "idend": 106,
            "ala": "1D7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 35,
            "matriz": 5,
            "idend": 107,
            "ala": "1D8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 35,
            "matriz": 5,
            "idend": 108,
            "ala": "1D9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 35,
            "matriz": 5,
            "idend": 109,
            "ala": "1D10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 35,
            "matriz": 5,
            "idend": 110,
            "ala": "1D11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 35,
            "matriz": 5,
            "idend": 111,
            "ala": "1D12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 35,
            "matriz": 5,
            "idend": 112,
            "ala": "1D13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 39,
            "matriz": 5,
            "idend": 113,
            "ala": "1D51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 39,
            "matriz": 5,
            "idend": 114,
            "ala": "1D26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 39,
            "matriz": 5,
            "idend": 115,
            "ala": "1D25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 39,
            "matriz": 5,
            "idend": 116,
            "ala": "1D24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 39,
            "matriz": 5,
            "idend": 117,
            "ala": "1D23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 39,
            "matriz": 5,
            "idend": 118,
            "ala": "1D22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 39,
            "matriz": 5,
            "idend": 119,
            "ala": "1D21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 39,
            "matriz": 5,
            "idend": 120,
            "ala": "1D20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 39,
            "matriz": 5,
            "idend": 121,
            "ala": "1D19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 39,
            "matriz": 5,
            "idend": 122,
            "ala": "1D18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 39,
            "matriz": 5,
            "idend": 123,
            "ala": "1D17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 39,
            "matriz": 5,
            "idend": 124,
            "ala": "1D16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 39,
            "matriz": 5,
            "idend": 125,
            "ala": "1D15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 39,
            "matriz": 5,
            "idend": 126,
            "ala": "1D14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar Matriz 5
        {
            "coordx": 29,
            "coordy": 46,
            "matriz": 5,
            "idend": 1001,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        //Matriz 6
        {
            "coordx": 0,
            "coordy": 8,
            "matriz": 6,
            "idend": 1,
            "ala": "1A60",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 8,
            "matriz": 6,
            "idend": 2,
            "ala": "1A61",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 8,
            "matriz": 6,
            "idend": 3,
            "ala": "1A62",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 8,
            "matriz": 6,
            "idend": 4,
            "ala": "1A63",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 8,
            "matriz": 6,
            "idend": 5,
            "ala": "1A64",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 8,
            "matriz": 6,
            "idend": 6,
            "ala": "1A65",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 8,
            "matriz": 6,
            "idend": 7,
            "ala": "1A66",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 8,
            "matriz": 6,
            "idend": 8,
            "ala": "1A39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 8,
            "matriz": 6,
            "idend": 9,
            "ala": "1A40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 8,
            "matriz": 6,
            "idend": 10,
            "ala": "1A41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 8,
            "matriz": 6,
            "idend": 11,
            "ala": "1A42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 8,
            "matriz": 6,
            "idend": 12,
            "ala": "1A43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 8,
            "matriz": 6,
            "idend": 13,
            "ala": "1A44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 8,
            "matriz": 6,
            "idend": 14,
            "ala": "1A45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 8,
            "matriz": 6,
            "idend": 15,
            "ala": "1A46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 8,
            "matriz": 6,
            "idend": 16,
            "ala": "1A47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 8,
            "matriz": 6,
            "idend": 17,
            "ala": "1A48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 8,
            "matriz": 6,
            "idend": 18,
            "ala": "1A49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 8,
            "matriz": 6,
            "idend": 19,
            "ala": "1A50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 8,
            "matriz": 6,
            "idend": 20,
            "ala": "1A1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 8,
            "matriz": 6,
            "idend": 21,
            "ala": "1A2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 8,
            "matriz": 6,
            "idend": 22,
            "ala": "1A3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 8,
            "matriz": 6,
            "idend": 23,
            "ala": "1A4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 8,
            "matriz": 6,
            "idend": 24,
            "ala": "1A5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 8,
            "matriz": 6,
            "idend": 25,
            "ala": "1A6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 30,
            "coordy": 8,
            "matriz": 6,
            "idend": 26,
            "ala": "1A7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 31,
            "coordy": 8,
            "matriz": 6,
            "idend": 27,
            "ala": "1A8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 32,
            "coordy": 8,
            "matriz": 6,
            "idend": 28,
            "ala": "1A9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 8,
            "matriz": 6,
            "idend": 29,
            "ala": "1A10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 34,
            "coordy": 8,
            "matriz": 6,
            "idend": 30,
            "ala": "1A11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 35,
            "coordy": 8,
            "matriz": 6,
            "idend": 31,
            "ala": "1A12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 8,
            "matriz": 6,
            "idend": 32,
            "ala": "1A13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 0,
            "coordy": 12,
            "matriz": 6,
            "idend": 33,
            "ala": "1A59",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 12,
            "matriz": 6,
            "idend": 34,
            "ala": "1A58",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 12,
            "matriz": 6,
            "idend": 35,
            "ala": "1A57",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 12,
            "matriz": 6,
            "idend": 36,
            "ala": "1A56",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 12,
            "matriz": 6,
            "idend": 37,
            "ala": "1A55",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 12,
            "matriz": 6,
            "idend": 38,
            "ala": "1A54",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 12,
            "matriz": 6,
            "idend": 39,
            "ala": "1A53",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 12,
            "matriz": 6,
            "idend": 40,
            "ala": "1A52",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 12,
            "matriz": 6,
            "idend": 41,
            "ala": "1A38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 12,
            "matriz": 6,
            "idend": 42,
            "ala": "1A37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 12,
            "matriz": 6,
            "idend": 43,
            "ala": "1A36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 12,
            "matriz": 6,
            "idend": 44,
            "ala": "1A35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 12,
            "matriz": 6,
            "idend": 45,
            "ala": "1A34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 12,
            "matriz": 6,
            "idend": 46,
            "ala": "1A33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 12,
            "matriz": 6,
            "idend": 47,
            "ala": "1A32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 12,
            "matriz": 6,
            "idend": 48,
            "ala": "1A31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 12,
            "matriz": 6,
            "idend": 49,
            "ala": "1A30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 12,
            "matriz": 6,
            "idend": 50,
            "ala": "1A29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 12,
            "matriz": 6,
            "idend": 51,
            "ala": "1A28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 12,
            "matriz": 6,
            "idend": 52,
            "ala": "1A27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 12,
            "matriz": 6,
            "idend": 53,
            "ala": "1A51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 12,
            "matriz": 6,
            "idend": 54,
            "ala": "1A26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 12,
            "matriz": 6,
            "idend": 55,
            "ala": "1A25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 12,
            "matriz": 6,
            "idend": 56,
            "ala": "1A24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 12,
            "matriz": 6,
            "idend": 57,
            "ala": "1A23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 12,
            "matriz": 6,
            "idend": 58,
            "ala": "1A22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 12,
            "matriz": 6,
            "idend": 59,
            "ala": "1A21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 30,
            "coordy": 12,
            "matriz": 6,
            "idend": 60,
            "ala": "1A20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 31,
            "coordy": 12,
            "matriz": 6,
            "idend": 61,
            "ala": "1A19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 32,
            "coordy": 12,
            "matriz": 6,
            "idend": 62,
            "ala": "1A18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 12,
            "matriz": 6,
            "idend": 63,
            "ala": "1A17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 34,
            "coordy": 12,
            "matriz": 6,
            "idend": 64,
            "ala": "1A16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 35,
            "coordy": 12,
            "matriz": 6,
            "idend": 65,
            "ala": "1A15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 12,
            "matriz": 6,
            "idend": 66,
            "ala": "1A14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 25,
            "matriz": 6,
            "idend": 67,
            "ala": "1B56",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 25,
            "matriz": 6,
            "idend": 68,
            "ala": "1B55",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 25,
            "matriz": 6,
            "idend": 69,
            "ala": "1B54",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 25,
            "matriz": 6,
            "idend": 70,
            "ala": "1B53",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 25,
            "matriz": 6,
            "idend": 71,
            "ala": "1B52",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 25,
            "matriz": 6,
            "idend": 72,
            "ala": "1B51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 26,
            "matriz": 6,
            "idend": 73,
            "ala": "1B38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 26,
            "matriz": 6,
            "idend": 74,
            "ala": "1B39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 26,
            "matriz": 6,
            "idend": 75,
            "ala": "1B40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 26,
            "matriz": 6,
            "idend": 76,
            "ala": "1B41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 26,
            "matriz": 6,
            "idend": 77,
            "ala": "1B42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 26,
            "matriz": 6,
            "idend": 78,
            "ala": "1B43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 26,
            "matriz": 6,
            "idend": 79,
            "ala": "1B44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 26,
            "matriz": 6,
            "idend": 80,
            "ala": "1B45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 26,
            "matriz": 6,
            "idend": 81,
            "ala": "1B46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 26,
            "matriz": 6,
            "idend": 82,
            "ala": "1B47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 26,
            "matriz": 6,
            "idend": 84,
            "ala": "1B48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 26,
            "matriz": 6,
            "idend": 85,
            "ala": "1B49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 26,
            "matriz": 6,
            "idend": 86,
            "ala": "1B1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 26,
            "matriz": 6,
            "idend": 87,
            "ala": "1B2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 26,
            "matriz": 6,
            "idend": 88,
            "ala": "1B3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 26,
            "matriz": 6,
            "idend": 89,
            "ala": "1B4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 26,
            "matriz": 6,
            "idend": 90,
            "ala": "1B5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 26,
            "matriz": 6,
            "idend": 91,
            "ala": "1B6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 30,
            "coordy": 26,
            "matriz": 6,
            "idend": 92,
            "ala": "1B7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 31,
            "coordy": 26,
            "matriz": 6,
            "idend": 93,
            "ala": "1B8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 32,
            "coordy": 26,
            "matriz": 6,
            "idend": 94,
            "ala": "1B9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 26,
            "matriz": 6,
            "idend": 95,
            "ala": "1B10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 34,
            "coordy": 26,
            "matriz": 6,
            "idend": 96,
            "ala": "1B11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 35,
            "coordy": 26,
            "matriz": 6,
            "idend": 97,
            "ala": "1B12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 36,
            "coordy": 26,
            "matriz": 6,
            "idend": 98,
            "ala": "1B13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar Matriz 6
        {
            "coordx": 37,
            "coordy": 1,
            "matriz": 6,
            "idend": 1000,
            "ala": "",
            "bloco": "",
            "status": 5,
            "altura": 0
        },

        //Matriz 7
        {
            "coordx": 1,
            "coordy": 3,
            "matriz": 7,
            "idend": 1,
            "ala": "L1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 3,
            "matriz": 7,
            "idend": 2,
            "ala": "L2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 3,
            "matriz": 7,
            "idend": 3,
            "ala": "L3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 3,
            "matriz": 7,
            "idend": 4,
            "ala": "L4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 3,
            "matriz": 7,
            "idend": 5,
            "ala": "L5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 3,
            "matriz": 7,
            "idend": 6,
            "ala": "L6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 3,
            "matriz": 7,
            "idend": 7,
            "ala": "L7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 3,
            "matriz": 7,
            "idend": 8,
            "ala": "L8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 3,
            "matriz": 7,
            "idend": 9,
            "ala": "L9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 3,
            "matriz": 7,
            "idend": 10,
            "ala": "L10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 3,
            "matriz": 7,
            "idend": 11,
            "ala": "L11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 12,
            "coordy": 3,
            "matriz": 7,
            "idend": 12,
            "ala": "L12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 13,
            "coordy": 3,
            "matriz": 7,
            "idend": 13,
            "ala": "L13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 3,
            "matriz": 7,
            "idend": 14,
            "ala": "L14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 3,
            "matriz": 7,
            "idend": 15,
            "ala": "L15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 3,
            "matriz": 7,
            "idend": 16,
            "ala": "L16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 3,
            "matriz": 7,
            "idend": 17,
            "ala": "L17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 3,
            "matriz": 7,
            "idend": 18,
            "ala": "L18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 3,
            "matriz": 7,
            "idend": 19,
            "ala": "L19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 3,
            "matriz": 7,
            "idend": 20,
            "ala": "L20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 3,
            "matriz": 7,
            "idend": 21,
            "ala": "L21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 3,
            "matriz": 7,
            "idend": 22,
            "ala": "L22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 3,
            "matriz": 7,
            "idend": 23,
            "ala": "L23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 3,
            "matriz": 7,
            "idend": 24,
            "ala": "L24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 3,
            "matriz": 7,
            "idend": 25,
            "ala": "L25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 3,
            "matriz": 7,
            "idend": 26,
            "ala": "L26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 3,
            "matriz": 7,
            "idend": 27,
            "ala": "L27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 28,
            "coordy": 3,
            "matriz": 7,
            "idend": 28,
            "ala": "L28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 3,
            "matriz": 7,
            "idend": 29,
            "ala": "L29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 30,
            "coordy": 3,
            "matriz": 7,
            "idend": 30,
            "ala": "L30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 31,
            "coordy": 3,
            "matriz": 7,
            "idend": 1,
            "ala": "L31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 32,
            "coordy": 3,
            "matriz": 7,
            "idend": 32,
            "ala": "L32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 33,
            "coordy": 3,
            "matriz": 7,
            "idend": 33,
            "ala": "L33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar Matriz 7
        {
            "coordx": 33,
            "coordy": 7,
            "matriz": 7,
            "idend": 1000,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
    ];
    constructor() {

    }

}