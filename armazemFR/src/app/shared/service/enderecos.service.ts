import { Mapa } from "../model/mapa.model";

export class MapaService {

    data: Mapa[] = [
        //Matriz 4
        {
            "coordx": 14,
            "coordy": 7,
            "matriz": 4,
            "idend": 5243,
            "ala": "1X25",
            "bloco": "C",
            "status": 1,
            "altura": 0
        },
        //BLOCOS AMARELOS   
        //MAtriz 1
        {
            "coordx": 7,
            "coordy": 1,
            "matriz": 1,
            "idend": 1,
            "ala": "1D1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 2,
            "matriz": 1,
            "idend": 1,
            "ala": "1D2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 3,
            "matriz": 1,
            "idend": 1,
            "ala": "1D3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 4,
            "matriz": 1,
            "idend": 1,
            "ala": "1D4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 5,
            "matriz": 1,
            "idend": 1,
            "ala": "1D5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 6,
            "matriz": 1,
            "idend": 1,
            "ala": "1D6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 7,
            "matriz": 1,
            "idend": 1,
            "ala": "1D7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 8,
            "matriz": 1,
            "idend": 1,
            "ala": "F12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 9,
            "matriz": 1,
            "idend": 1,
            "ala": "1D8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 10,
            "matriz": 1,
            "idend": 1,
            "ala": "1D9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 11,
            "matriz": 1,
            "idend": 1,
            "ala": "1D10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 12,
            "matriz": 1,
            "idend": 1,
            "ala": "1D11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 13,
            "matriz": 1,
            "idend": 1,
            "ala": "1D12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 14,
            "matriz": 1,
            "idend": 1,
            "ala": "1D13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 15,
            "matriz": 1,
            "idend": 1,
            "ala": "1D14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 16,
            "matriz": 1,
            "idend": 1,
            "ala": "1D15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 17,
            "matriz": 1,
            "idend": 1,
            "ala": "1D16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 18,
            "matriz": 1,
            "idend": 1,
            "ala": "1D17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 19,
            "matriz": 1,
            "idend": 1,
            "ala": "1D18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 20,
            "matriz": 1,
            "idend": 1,
            "ala": "1D19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 21,
            "matriz": 1,
            "idend": 1,
            "ala": "1D20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 22,
            "matriz": 1,
            "idend": 1,
            "ala": "1D21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 23,
            "matriz": 1,
            "idend": 1,
            "ala": "1D22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 24,
            "matriz": 1,
            "idend": 1,
            "ala": "F10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 25,
            "matriz": 1,
            "idend": 1,
            "ala": "1D23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 26,
            "matriz": 1,
            "idend": 1,
            "ala": "1D24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 27,
            "matriz": 1,
            "idend": 1,
            "ala": "1D25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 28,
            "matriz": 1,
            "idend": 1,
            "ala": "1D26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 29,
            "matriz": 1,
            "idend": 1,
            "ala": "1D27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 30,
            "matriz": 1,
            "idend": 1,
            "ala": "1D28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 31,
            "matriz": 1,
            "idend": 1,
            "ala": "1D29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 32,
            "matriz": 1,
            "idend": 1,
            "ala": "1D30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 33,
            "matriz": 1,
            "idend": 1,
            "ala": "1D31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 34,
            "matriz": 1,
            "idend": 1,
            "ala": "1D32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 35,
            "matriz": 1,
            "idend": 1,
            "ala": "1D33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 36,
            "matriz": 1,
            "idend": 1,
            "ala": "1D34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 37,
            "matriz": 1,
            "idend": 1,
            "ala": "1D35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 38,
            "matriz": 1,
            "idend": 1,
            "ala": "1D36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 39,
            "matriz": 1,
            "idend": 1,
            "ala": "1D37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 40,
            "matriz": 1,
            "idend": 1,
            "ala": "1D38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 41,
            "matriz": 1,
            "idend": 1,
            "ala": "1D39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 42,
            "matriz": 1,
            "idend": 1,
            "ala": "1D40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 43,
            "matriz": 1,
            "idend": 1,
            "ala": "1D41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 44,
            "matriz": 1,
            "idend": 1,
            "ala": "1D42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 45,
            "matriz": 1,
            "idend": 1,
            "ala": "1D43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 46,
            "matriz": 1,
            "idend": 1,
            "ala": "F8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 47,
            "matriz": 1,
            "idend": 1,
            "ala": "1D44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 48,
            "matriz": 1,
            "idend": 1,
            "ala": "1D45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 49,
            "matriz": 1,
            "idend": 1,
            "ala": "1D46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 50,
            "matriz": 1,
            "idend": 1,
            "ala": "1D47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 51,
            "matriz": 1,
            "idend": 1,
            "ala": "1D48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 52,
            "matriz": 1,
            "idend": 1,
            "ala": "1D49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 53,
            "matriz": 1,
            "idend": 1,
            "ala": "1D50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 54,
            "matriz": 1,
            "idend": 1,
            "ala": "1D51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 55,
            "matriz": 1,
            "idend": 1,
            "ala": "1D52",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 56,
            "matriz": 1,
            "idend": 1,
            "ala": "1D53",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 57,
            "matriz": 1,
            "idend": 1,
            "ala": "1D54",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 58,
            "matriz": 1,
            "idend": 1,
            "ala": "1D55",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 59,
            "matriz": 1,
            "idend": 1,
            "ala": "1D56",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 60,
            "matriz": 1,
            "idend": 1,
            "ala": "1D57",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 61,
            "matriz": 1,
            "idend": 1,
            "ala": "1D58",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 62,
            "matriz": 1,
            "idend": 1,
            "ala": "1D59",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 63,
            "matriz": 1,
            "idend": 1,
            "ala": "1D60",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //PARTE 2 
        {
            "coordx": 11,
            "coordy": 7,
            "matriz": 1,
            "idend": 1,
            "ala": "1C52",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 9,
            "matriz": 1,
            "idend": 1,
            "ala": "1C1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 10,
            "matriz": 1,
            "idend": 1,
            "ala": "1C2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 11,
            "matriz": 1,
            "idend": 1,
            "ala": "1C3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 12,
            "matriz": 1,
            "idend": 1,
            "ala": "1C4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 13,
            "matriz": 1,
            "idend": 1,
            "ala": "1C5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 14,
            "matriz": 1,
            "idend": 1,
            "ala": "1C6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 15,
            "matriz": 1,
            "idend": 1,
            "ala": "1C7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 16,
            "matriz": 1,
            "idend": 1,
            "ala": "1C8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 17,
            "matriz": 1,
            "idend": 1,
            "ala": "1C9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 18,
            "matriz": 1,
            "idend": 1,
            "ala": "1C10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 19,
            "matriz": 1,
            "idend": 1,
            "ala": "1C11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 20,
            "matriz": 1,
            "idend": 1,
            "ala": "1C12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 21,
            "matriz": 1,
            "idend": 1,
            "ala": "1C13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 22,
            "matriz": 1,
            "idend": 1,
            "ala": "1C14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 23,
            "matriz": 1,
            "idend": 1,
            "ala": "1C15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 24,
            "matriz": 1,
            "idend": 1,
            "ala": "1C16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 25,
            "matriz": 1,
            "idend": 1,
            "ala": "1C17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 27,
            "matriz": 1,
            "idend": 1,
            "ala": "1C18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 28,
            "matriz": 1,
            "idend": 1,
            "ala": "1C19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 29,
            "matriz": 1,
            "idend": 1,
            "ala": "1C20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 30,
            "matriz": 1,
            "idend": 1,
            "ala": "1C21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 31,
            "matriz": 1,
            "idend": 1,
            "ala": "1C22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 32,
            "matriz": 1,
            "idend": 1,
            "ala": "1C23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 33,
            "matriz": 1,
            "idend": 1,
            "ala": "1C24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 34,
            "matriz": 1,
            "idend": 1,
            "ala": "1C25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 35,
            "matriz": 1,
            "idend": 1,
            "ala": "1C26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 36,
            "matriz": 1,
            "idend": 1,
            "ala": "1C27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 37,
            "matriz": 1,
            "idend": 1,
            "ala": "1C28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 38,
            "matriz": 1,
            "idend": 1,
            "ala": "1C29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 39,
            "matriz": 1,
            "idend": 1,
            "ala": "1C30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 40,
            "matriz": 1,
            "idend": 1,
            "ala": "1C31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 41,
            "matriz": 1,
            "idend": 1,
            "ala": "1C32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 42,
            "matriz": 1,
            "idend": 1,
            "ala": "1C33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 43,
            "matriz": 1,
            "idend": 1,
            "ala": "1C34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 44,
            "matriz": 1,
            "idend": 1,
            "ala": "1C35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 45,
            "matriz": 1,
            "idend": 1,
            "ala": "1C36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 47,
            "matriz": 1,
            "idend": 1,
            "ala": "1C37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 48,
            "matriz": 1,
            "idend": 1,
            "ala": "1C38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 49,
            "matriz": 1,
            "idend": 1,
            "ala": "1C39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 50,
            "matriz": 1,
            "idend": 1,
            "ala": "1C40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 51,
            "matriz": 1,
            "idend": 1,
            "ala": "1C41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 52,
            "matriz": 1,
            "idend": 1,
            "ala": "1C42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 53,
            "matriz": 1,
            "idend": 1,
            "ala": "1C43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 54,
            "matriz": 1,
            "idend": 1,
            "ala": "1C44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 55,
            "matriz": 1,
            "idend": 1,
            "ala": "1C53",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 55,
            "matriz": 1,
            "idend": 2,
            "ala": "1E13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 55,
            "matriz": 1,
            "idend": 3,
            "ala": "1E12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 55,
            "matriz": 1,
            "idend": 4,
            "ala": "1E11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 57,
            "matriz": 1,
            "idend": 1,
            "ala": "1C45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 58,
            "matriz": 1,
            "idend": 1,
            "ala": "1C46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 59,
            "matriz": 1,
            "idend": 1,
            "ala": "1C47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 60,
            "matriz": 1,
            "idend": 1,
            "ala": "1C48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 61,
            "matriz": 1,
            "idend": 1,
            "ala": "1C49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 62,
            "matriz": 1,
            "idend": 1,
            "ala": "1C50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 11,
            "coordy": 63,
            "matriz": 1,
            "idend": 1,
            "ala": "1C51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 66,
            "matriz": 1,
            "idend": 1,
            "ala": " 1R3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 67,
            "matriz": 1,
            "idend": 1,
            "ala": " 1R5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 68,
            "matriz": 1,
            "idend": 1,
            "ala": " 1R6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 71,
            "matriz": 1,
            "idend": 1,
            "ala": " 1R1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar MAtriz 1
        {
            "coordx": 26,
            "coordy": 1,
            "matriz": 1,
            "idend": 1,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 73,
            "matriz": 1,
            "idend": 1,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        //MAtriz 2
        {
            "coordx": 18,
            "coordy": 9,
            "matriz": 2,
            "idend": 1,
            "ala": "1B1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 10,
            "matriz": 2,
            "idend": 2,
            "ala": "1B2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 11,
            "matriz": 2,
            "idend": 3,
            "ala": "1B3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 12,
            "matriz": 2,
            "idend": 4,
            "ala": "1B4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 13,
            "matriz": 2,
            "idend": 5,
            "ala": "1B5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 14,
            "matriz": 2,
            "idend": 6,
            "ala": "1B6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 15,
            "matriz": 2,
            "idend": 7,
            "ala": "1B7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 16,
            "matriz": 2,
            "idend": 8,
            "ala": "1B8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 17,
            "matriz": 2,
            "idend": 9,
            "ala": "1B9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 18,
            "matriz": 2,
            "idend": 10,
            "ala": "1B10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 19,
            "matriz": 2,
            "idend": 11,
            "ala": "1B11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 20,
            "matriz": 2,
            "idend": 12,
            "ala": "1B12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 21,
            "matriz": 2,
            "idend": 13,
            "ala": "1B13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 22,
            "matriz": 2,
            "idend": 14,
            "ala": "1B14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 23,
            "matriz": 2,
            "idend": 15,
            "ala": "1B15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 24,
            "matriz": 2,
            "idend": 16,
            "ala": "F4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 25,
            "matriz": 2,
            "idend": 17,
            "ala": "1B16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 26,
            "matriz": 2,
            "idend": 18,
            "ala": "1B17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 27,
            "matriz": 2,
            "idend": 19,
            "ala": "1B18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 28,
            "matriz": 2,
            "idend": 20,
            "ala": "1B19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 29,
            "matriz": 2,
            "idend": 21,
            "ala": "1B20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 30,
            "matriz": 2,
            "idend": 22,
            "ala": "1B21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 31,
            "matriz": 2,
            "idend": 23,
            "ala": "1B22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 32,
            "matriz": 2,
            "idend": 24,
            "ala": "1B23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 33,
            "matriz": 2,
            "idend": 25,
            "ala": "1B24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 34,
            "matriz": 2,
            "idend": 26,
            "ala": "1B25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 35,
            "matriz": 2,
            "idend": 27,
            "ala": "1B26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 36,
            "matriz": 2,
            "idend": 28,
            "ala": "1B27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 37,
            "matriz": 2,
            "idend": 29,
            "ala": "1B28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 38,
            "matriz": 2,
            "idend": 30,
            "ala": "1B29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 39,
            "matriz": 2,
            "idend": 31,
            "ala": "1B30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 40,
            "matriz": 2,
            "idend": 32,
            "ala": "1B31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 41,
            "matriz": 2,
            "idend": 33,
            "ala": "1B32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 42,
            "matriz": 2,
            "idend": 34,
            "ala": "1B33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 43,
            "matriz": 2,
            "idend": 35,
            "ala": "1B34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 44,
            "matriz": 2,
            "idend": 36,
            "ala": "1B35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 45,
            "matriz": 2,
            "idend": 37,
            "ala": "1B36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 47,
            "matriz": 2,
            "idend": 38,
            "ala": "1B37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 48,
            "matriz": 2,
            "idend": 39,
            "ala": "1B38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 49,
            "matriz": 2,
            "idend": 40,
            "ala": "1B39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 50,
            "matriz": 2,
            "idend": 41,
            "ala": "1B40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 51,
            "matriz": 2,
            "idend": 42,
            "ala": "1B41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 52,
            "matriz": 2,
            "idend": 43,
            "ala": "1B42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 53,
            "matriz": 2,
            "idend": 44,
            "ala": "1B43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 54,
            "matriz": 2,
            "idend": 45,
            "ala": "1B44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 55,
            "matriz": 2,
            "idend": 46,
            "ala": "1B52",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 10,
            "coordy": 55,
            "matriz": 2,
            "idend": 47,
            "ala": "1E1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 9,
            "coordy": 55,
            "matriz": 2,
            "idend": 48,
            "ala": "1E2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 8,
            "coordy": 55,
            "matriz": 2,
            "idend": 49,
            "ala": "1E3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 7,
            "coordy": 55,
            "matriz": 2,
            "idend": 50,
            "ala": "1E4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 6,
            "coordy": 55,
            "matriz": 2,
            "idend": 51,
            "ala": "1E5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 5,
            "coordy": 55,
            "matriz": 2,
            "idend": 52,
            "ala": "1E6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 4,
            "coordy": 55,
            "matriz": 2,
            "idend": 53,
            "ala": "1E7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 3,
            "coordy": 55,
            "matriz": 2,
            "idend": 54,
            "ala": "1E8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 2,
            "coordy": 55,
            "matriz": 2,
            "idend": 55,
            "ala": "1E9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 1,
            "coordy": 55,
            "matriz": 2,
            "idend": 56,
            "ala": "1E10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 57,
            "matriz": 2,
            "idend": 57,
            "ala": "1B45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 58,
            "matriz": 2,
            "idend": 58,
            "ala": "1B46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 59,
            "matriz": 2,
            "idend": 59,
            "ala": "1B47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 60,
            "matriz": 2,
            "idend": 60,
            "ala": "1B48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 61,
            "matriz": 2,
            "idend": 61,
            "ala": "1B49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 62,
            "matriz": 2,
            "idend": 62,
            "ala": "1B50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 63,
            "matriz": 2,
            "idend": 63,
            "ala": "1B51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 66,
            "matriz": 2,
            "idend": 64,
            "ala": "1R4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 71,
            "matriz": 2,
            "idend": 64,
            "ala": "1R2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 1,
            "matriz": 2,
            "idend": 65,
            "ala": "1A1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 2,
            "matriz": 2,
            "idend": 66,
            "ala": "1A2",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 3,
            "matriz": 2,
            "idend": 67,
            "ala": "1A3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 4,
            "matriz": 2,
            "idend": 68,
            "ala": "1A4",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 5,
            "matriz": 2,
            "idend": 69,
            "ala": "1A5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 6,
            "matriz": 2,
            "idend": 70,
            "ala": "1A6",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 7,
            "matriz": 2,
            "idend": 71,
            "ala": "1A7",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 8,
            "matriz": 2,
            "idend": 72,
            "ala": "F1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 9,
            "matriz": 2,
            "idend": 73,
            "ala": "1A8",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 10,
            "matriz": 2,
            "idend": 74,
            "ala": "1A9",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 11,
            "matriz": 2,
            "idend": 75,
            "ala": "1A10",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 12,
            "matriz": 2,
            "idend": 76,
            "ala": "1A11",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 13,
            "matriz": 2,
            "idend": 77,
            "ala": "1A12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 14,
            "matriz": 2,
            "idend": 78,
            "ala": "1A13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 15,
            "matriz": 2,
            "idend": 79,
            "ala": "1A14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 16,
            "matriz": 2,
            "idend": 80,
            "ala": "1A15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 17,
            "matriz": 2,
            "idend": 81,
            "ala": "1A16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 18,
            "matriz": 2,
            "idend": 82,
            "ala": "1A17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 19,
            "matriz": 2,
            "idend": 83,
            "ala": "1A18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 20,
            "matriz": 2,
            "idend": 84,
            "ala": "1A19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 21,
            "matriz": 2,
            "idend": 85,
            "ala": "1A20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 22,
            "matriz": 2,
            "idend": 86,
            "ala": "1A21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 23,
            "matriz": 2,
            "idend": 87,
            "ala": "1A22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 24,
            "matriz": 2,
            "idend": 88,
            "ala": "F3",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 25,
            "matriz": 2,
            "idend": 89,
            "ala": "1A23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 26,
            "matriz": 2,
            "idend": 90,
            "ala": "1A24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 27,
            "matriz": 2,
            "idend": 91,
            "ala": "1A25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 28,
            "matriz": 2,
            "idend": 92,
            "ala": "1A26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 29,
            "matriz": 2,
            "idend": 93,
            "ala": "1A27",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 30,
            "matriz": 2,
            "idend": 94,
            "ala": "1A28",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 31,
            "matriz": 2,
            "idend": 95,
            "ala": "1A29",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 32,
            "matriz": 2,
            "idend": 96,
            "ala": "1A30",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 33,
            "matriz": 2,
            "idend": 97,
            "ala": "1A31",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 34,
            "matriz": 2,
            "idend": 98,
            "ala": "1A32",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 35,
            "matriz": 2,
            "idend": 99,
            "ala": "1A33",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 36,
            "matriz": 2,
            "idend": 100,
            "ala": "1A34",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 37,
            "matriz": 2,
            "idend": 101,
            "ala": "1A35",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 38,
            "matriz": 2,
            "idend": 102,
            "ala": "1A36",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 39,
            "matriz": 2,
            "idend": 103,
            "ala": "1A37",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 40,
            "matriz": 2,
            "idend": 104,
            "ala": "1A38",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 41,
            "matriz": 2,
            "idend": 105,
            "ala": "1A39",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 42,
            "matriz": 2,
            "idend": 106,
            "ala": "1A40",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 43,
            "matriz": 2,
            "idend": 107,
            "ala": "1A41",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 44,
            "matriz": 2,
            "idend": 108,
            "ala": "1A42",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 45,
            "matriz": 2,
            "idend": 109,
            "ala": "1A43",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 46,
            "matriz": 2,
            "idend": 110,
            "ala": "F5",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 47,
            "matriz": 2,
            "idend": 111,
            "ala": "1A44",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 48,
            "matriz": 2,
            "idend": 112,
            "ala": "1A45",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 49,
            "matriz": 2,
            "idend": 113,
            "ala": "1A46",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 50,
            "matriz": 2,
            "idend": 114,
            "ala": "1A47",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 51,
            "matriz": 2,
            "idend": 115,
            "ala": "1A48",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 52,
            "matriz": 2,
            "idend": 116,
            "ala": "1A49",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 53,
            "matriz": 2,
            "idend": 117,
            "ala": "1A50",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 54,
            "matriz": 2,
            "idend": 118,
            "ala": "1A51",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 55,
            "matriz": 2,
            "idend": 119,
            "ala": "1A52",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 56,
            "matriz": 2,
            "idend": 120,
            "ala": "1A53",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 57,
            "matriz": 2,
            "idend": 121,
            "ala": "1A54",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 58,
            "matriz": 2,
            "idend": 122,
            "ala": "1A55",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 59,
            "matriz": 2,
            "idend": 123,
            "ala": "1A56",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 60,
            "matriz": 2,
            "idend": 124,
            "ala": "1A57",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 61,
            "matriz": 2,
            "idend": 125,
            "ala": "1A58",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 62,
            "matriz": 2,
            "idend": 126,
            "ala": "1A59",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 63,
            "matriz": 2,
            "idend": 127,
            "ala": "1A60",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar MAtriz 2  
        {
            "coordx": 1,
            "coordy": 73,
            "matriz": 2,
            "idend": 1,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        {
            "coordx": 29,
            "coordy": 73,
            "matriz": 2,
            "idend": 1,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        //MAtriz 3
        {
            "coordx": 13,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X26",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 14,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X25",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 15,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X24",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 16,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X23",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 17,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X22",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 18,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X21",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 19,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X20",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 20,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X19",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 21,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X18",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 22,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X17",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 23,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X16",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 24,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X15",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 25,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X14",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 26,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X13",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 27,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X12",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        {
            "coordx": 30,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X11",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 31,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X10",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 32,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X9",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 33,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X8",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 34,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X7",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 35,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X6",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 36,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X5",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 37,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X4",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 38,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X3",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 39,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X2",
            "bloco": "",
            "status": 2,
            "altura": 0
        }, {
            "coordx": 40,
            "coordy": 8,
            "matriz": 3,
            "idend": 1,
            "ala": "1X1",
            "bloco": "",
            "status": 2,
            "altura": 0
        },
        //Forçar MAtriz 3
        {
            "coordx": 1,
            "coordy": 9,
            "matriz": 3,
            "idend": 1,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
        // {
        //     "coordx": 1,
        //     "coordy": 10,
        //     "matriz": 3,
        //     "idend": 1,
        //     "ala": "",
        //     "bloco": "",
        //     "status": 10,
        //     "altura": 0
        // },
        {
            "coordx": 43,
            "coordy": 1,
            "matriz": 3,
            "idend": 1,
            "ala": "",
            "bloco": "",
            "status": 10,
            "altura": 0
        },
    ];
    constructor() {

    }

}