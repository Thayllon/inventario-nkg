import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/shared/service/api.service';
import { UserService } from 'src/app/shared/service/usuarios.service';

@Component({
  selector: 'app-modal-caixa',
  templateUrl: './modal-caixa.component.html',
  styleUrls: ['./modal-caixa.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ModalCaixaComponent {
  isLoading: boolean = false;
  titulo: string = '';
  BagsCaixa: any[] = [];
  lotesRecebimento: any;
  lotes: any;

  constructor(public matDialogRef: MatDialogRef<ModalCaixaComponent>, @Inject(MAT_DIALOG_DATA) private _data: any,
    private _apiService: ApiService, private _apiServiceUser: UserService) {
    this.isLoading = true;
    this.titulo = _data.titulo;

    this._apiService.getByBoxRec().then(res => {
      this.isLoading = false;
      this.BagsCaixa = res;
      this.CaixasRecebimento();
    }, reject => {
      this.isLoading = false;
      console.log(reject)
    });

  }

  CaixasRecebimento() {
    this.lotesRecebimento = [...new Set(this.BagsCaixa.map(x => x))];

    this.lotesRecebimento = this.lotesRecebimento.map((x: any) => { //objeto montado para tela de validar ala //entender melhor
      return {
        iD_Caixa: x.iD_Caixa,
        tipo_Caixa: x.tipo_Caixa,
        estoque_Caixa: x.estoque_Caixa,
        letra: x.letra,
      }
    });


  }

  ValidarAla(bag: any) {

    this._apiService.postValidacaoCaixa(bag.iD_Caixa);

    let CaixaValidada = {
      alaInvent: bag.tipo_Caixa,
      loteInvent: bag.letra,
      qtdKgLoteSist: 0,
      qtdScsLoteSist: 0,
      qtdBagsLoteInvent: 0,
      NrDispositivo: this._apiServiceUser.dadosUser.usuario,
      qtdKgLoteValid: bag.estoque_Caixa,
      qtdScsValid: 0,
      idUser: bag.iD_Caixa,
      descOperacao: "Caixa Recebimento",
      statusAla: 1,
    }

    console.log(JSON.stringify(CaixaValidada))

    this._apiService.postSalvarValidacaoAla(CaixaValidada)
      .then(res => {
        console.log(res);
      }, reject => {
        console.log(reject)
      });
  }


  InvalidarAla() {
    // let enderecos = [...new Set(this.BagsCaixa.map(data => {
    //   return data
    // }))];

    // for (let data of enderecos) {
    //   this._apiService.postInvalidarCaixa(data);
    // }
  }



}
