import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/shared/service/api.service';

@Component({
  selector: 'app-modal-caixa-reb',
  templateUrl: './modal-caixa-reb.component.html',
  styleUrls: ['./modal-caixa-reb.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModalCaixaRebComponent {
  isLoading: boolean = false;
  titulo: string = '';
  BagsCaixa: any[] = [];

  constructor(public matDialogRef: MatDialogRef<ModalCaixaRebComponent>, private _apiService: ApiService, @Inject(MAT_DIALOG_DATA) private _data: any) {
    this.isLoading = true;
    this.titulo = _data.titulo;

    this._apiService.getByBoxRec().then(res => {
      this.BagsCaixa = res;
      this.isLoading = false;
      console.log(res)
    }, reject => {
      this.isLoading = false;
      console.log(reject)
    });

  }

}
