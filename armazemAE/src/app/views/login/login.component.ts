import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from './../../shared/service/usuarios.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  login: string = '';
  senha: string = '';
  isAuth: boolean = false;
  usuarios: [] = [];

  constructor(private _matDialog: MatDialog, private _apiService: UserService, private _route: Router) {
    this.isAuth = false;
  }

  efetuarLogin() {
    this._apiService.postLogin(this.login, this.senha).then(res => {

      this.isAuth = true;
      this._route.navigate(['/armazem']);

    }, reject => {
      console.log(reject);
    });
  }

}
