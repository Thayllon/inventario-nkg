import { Mapa } from "../model/mapa.model";

export class MapaService {

  data: Mapa[] = [
    //MATRIZ 7
    {
      "coordx": 1,
      "coordy": 1,
      "matriz": 7,
      "idend": 1,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //Quadras Amarelas 
    //MATRIZ 1
    //BLOCO 3B1
    {
      "coordx": 10,
      "coordy": 10,
      "matriz": 1,
      "idend": 1,
      "ala": "3B100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 11,
      "matriz": 1,
      "idend": 1,
      "ala": "3B101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 12,
      "matriz": 1,
      "idend": 1,
      "ala": "3B102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 13,
      "matriz": 1,
      "idend": 1,
      "ala": "3B103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 14,
      "matriz": 1,
      "idend": 1,
      "ala": "3B104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 15,
      "matriz": 1,
      "idend": 1,
      "ala": "3B105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 16,
      "matriz": 1,
      "idend": 1,
      "ala": "3B106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 17,
      "matriz": 1,
      "idend": 1,
      "ala": "3B107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 18,
      "matriz": 1,
      "idend": 1,
      "ala": "3B108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 19,
      "matriz": 1,
      "idend": 1,
      "ala": "3B109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 20,
      "matriz": 1,
      "idend": 1,
      "ala": "3B110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 21,
      "matriz": 1,
      "idend": 1,
      "ala": "3B111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 22,
      "matriz": 1,
      "idend": 1,
      "ala": "3B112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 23,
      "matriz": 1,
      "idend": 1,
      "ala": "3B113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 24,
      "matriz": 1,
      "idend": 1,
      "ala": "3B114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 25,
      "matriz": 1,
      "idend": 1,
      "ala": "3B115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B1
    {
      "coordx": 9,
      "coordy": 28,
      "matriz": 1,
      "idend": 1,
      "ala": "4B100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 29,
      "matriz": 1,
      "idend": 1,
      "ala": "4B101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 30,
      "matriz": 1,
      "idend": 1,
      "ala": "4B102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 31,
      "matriz": 1,
      "idend": 1,
      "ala": "4B103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 32,
      "matriz": 1,
      "idend": 1,
      "ala": "4B104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 33,
      "matriz": 1,
      "idend": 1,
      "ala": "4B105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 34,
      "matriz": 1,
      "idend": 1,
      "ala": "4B106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 35,
      "matriz": 1,
      "idend": 1,
      "ala": "4B107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 36,
      "matriz": 1,
      "idend": 1,
      "ala": "4B108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 37,
      "matriz": 1,
      "idend": 1,
      "ala": "4B109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 38,
      "matriz": 1,
      "idend": 1,
      "ala": "4B110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 39,
      "matriz": 1,
      "idend": 1,
      "ala": "4B111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 40,
      "matriz": 1,
      "idend": 1,
      "ala": "4B112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 41,
      "matriz": 1,
      "idend": 1,
      "ala": "4B113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 42,
      "matriz": 1,
      "idend": 1,
      "ala": "4B114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 43,
      "matriz": 1,
      "idend": 1,
      "ala": "4B115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 44,
      "matriz": 1,
      "idend": 1,
      "ala": "4B116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 45,
      "matriz": 1,
      "idend": 1,
      "ala": "4B117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 46,
      "matriz": 1,
      "idend": 1,
      "ala": "4B118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },

    //BLOCO 3B2
    {
      "coordx": 12,
      "coordy": 10,
      "matriz": 1,
      "idend": 1,
      "ala": "3B200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 11,
      "matriz": 1,
      "idend": 1,
      "ala": "3B201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 12,
      "matriz": 1,
      "idend": 1,
      "ala": "3B202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 13,
      "matriz": 1,
      "idend": 1,
      "ala": "3B203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 14,
      "matriz": 1,
      "idend": 1,
      "ala": "3B204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 15,
      "matriz": 1,
      "idend": 1,
      "ala": "3B205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 16,
      "matriz": 1,
      "idend": 1,
      "ala": "3B206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 17,
      "matriz": 1,
      "idend": 1,
      "ala": "3B207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 18,
      "matriz": 1,
      "idend": 1,
      "ala": "3B208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 19,
      "matriz": 1,
      "idend": 1,
      "ala": "3B209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 20,
      "matriz": 1,
      "idend": 1,
      "ala": "3B210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 21,
      "matriz": 1,
      "idend": 1,
      "ala": "3B211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 22,
      "matriz": 1,
      "idend": 1,
      "ala": "3B212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 23,
      "matriz": 1,
      "idend": 1,
      "ala": "3B213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 24,
      "matriz": 1,
      "idend": 1,
      "ala": "3B214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 25,
      "matriz": 1,
      "idend": 1,
      "ala": "3B215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B2
    {
      "coordx": 13,
      "coordy": 28,
      "matriz": 1,
      "idend": 4,
      "ala": "4B200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 29,
      "matriz": 1,
      "idend": 1,
      "ala": "4B201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 30,
      "matriz": 1,
      "idend": 1,
      "ala": "4B202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 31,
      "matriz": 1,
      "idend": 1,
      "ala": "4B203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 32,
      "matriz": 1,
      "idend": 1,
      "ala": "4B204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 33,
      "matriz": 1,
      "idend": 1,
      "ala": "4B205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 34,
      "matriz": 1,
      "idend": 1,
      "ala": "4B206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 35,
      "matriz": 1,
      "idend": 1,
      "ala": "4B207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 36,
      "matriz": 1,
      "idend": 1,
      "ala": "4B208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 37,
      "matriz": 1,
      "idend": 1,
      "ala": "4B209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 38,
      "matriz": 1,
      "idend": 1,
      "ala": "4B210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 39,
      "matriz": 1,
      "idend": 1,
      "ala": "4B211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 40,
      "matriz": 1,
      "idend": 1,
      "ala": "4B212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 41,
      "matriz": 1,
      "idend": 1,
      "ala": "4B213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 42,
      "matriz": 1,
      "idend": 1,
      "ala": "4B214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 43,
      "matriz": 1,
      "idend": 1,
      "ala": "4B215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 44,
      "matriz": 1,
      "idend": 1,
      "ala": "4B216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 45,
      "matriz": 1,
      "idend": 1,
      "ala": "4B217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 46,
      "matriz": 1,
      "idend": 1,
      "ala": "4B218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 3B3   
    {
      "coordx": 23,
      "coordy": 1,
      "matriz": 1,
      "idend": 1,
      "ala": "3B300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 2,
      "matriz": 1,
      "idend": 1,
      "ala": "3B301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 3,
      "matriz": 1,
      "idend": 1,
      "ala": "3B302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 4,
      "matriz": 1,
      "idend": 1,
      "ala": "3B303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 5,
      "matriz": 1,
      "idend": 1,
      "ala": "3B304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 6,
      "matriz": 1,
      "idend": 1,
      "ala": "3B305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 7,
      "matriz": 1,
      "idend": 1,
      "ala": "3B306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 8,
      "matriz": 1,
      "idend": 1,
      "ala": "3B307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 9,
      "matriz": 1,
      "idend": 1,
      "ala": "3B308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 10,
      "matriz": 1,
      "idend": 1,
      "ala": "3B309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 11,
      "matriz": 1,
      "idend": 1,
      "ala": "3B310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 12,
      "matriz": 1,
      "idend": 1,
      "ala": "3B311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 13,
      "matriz": 1,
      "idend": 1,
      "ala": "3B312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 14,
      "matriz": 1,
      "idend": 1,
      "ala": "3B313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 15,
      "matriz": 1,
      "idend": 1,
      "ala": "3B314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 16,
      "matriz": 1,
      "idend": 1,
      "ala": "3B315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 17,
      "matriz": 1,
      "idend": 1,
      "ala": "3B316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 18,
      "matriz": 1,
      "idend": 1,
      "ala": "3B317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 19,
      "matriz": 1,
      "idend": 1,
      "ala": "3B318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 20,
      "matriz": 1,
      "idend": 1,
      "ala": "3B319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 21,
      "matriz": 1,
      "idend": 1,
      "ala": "3B320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 22,
      "matriz": 1,
      "idend": 1,
      "ala": "3B321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 23,
      "matriz": 1,
      "idend": 1,
      "ala": "3B322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 24,
      "matriz": 1,
      "idend": 1,
      "ala": "3B323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 25,
      "matriz": 1,
      "idend": 1,
      "ala": "3B324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 28,
      "coordy": 9,
      "matriz": 1,
      "idend": 1,
      "ala": "3B325",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B3
    {
      "coordx": 25,
      "coordy": 28,
      "matriz": 1,
      "idend": 6,
      "ala": "4B300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 29,
      "matriz": 1,
      "idend": 6,
      "ala": "4B301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 30,
      "matriz": 1,
      "idend": 6,
      "ala": "4B302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 31,
      "matriz": 1,
      "idend": 6,
      "ala": "4B303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 32,
      "matriz": 1,
      "idend": 6,
      "ala": "4B304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 33,
      "matriz": 1,
      "idend": 6,
      "ala": "4B305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 34,
      "matriz": 1,
      "idend": 6,
      "ala": "4B306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 35,
      "matriz": 1,
      "idend": 6,
      "ala": "4B307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 36,
      "matriz": 1,
      "idend": 6,
      "ala": "4B308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 37,
      "matriz": 1,
      "idend": 6,
      "ala": "4B309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 38,
      "matriz": 1,
      "idend": 6,
      "ala": "4B310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 39,
      "matriz": 1,
      "idend": 6,
      "ala": "4B311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 40,
      "matriz": 1,
      "idend": 6,
      "ala": "4B312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 41,
      "matriz": 1,
      "idend": 6,
      "ala": "4B313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 42,
      "matriz": 1,
      "idend": 6,
      "ala": "4B314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 43,
      "matriz": 1,
      "idend": 6,
      "ala": "4B315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 44,
      "matriz": 1,
      "idend": 6,
      "ala": "4B316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 45,
      "matriz": 1,
      "idend": 6,
      "ala": "4B317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 46,
      "matriz": 1,
      "idend": 6,
      "ala": "4B318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 47,
      "matriz": 1,
      "idend": 6,
      "ala": "4B319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 48,
      "matriz": 1,
      "idend": 6,
      "ala": "4B320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //FORÇAR MATRIZ 1
    {
      "coordx": 26,
      "coordy": 49,
      "matriz": 1,
      "idend": 6,
      "ala": "4B321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //MATRIZ 2
    //BLOCO 1A4
    {
      "coordx": 12,
      "coordy": 6,
      "matriz": 2,
      "idend": 7,
      "ala": "1A400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 7,
      "matriz": 2,
      "idend": 201,
      "ala": "1A401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 8,
      "matriz": 2,
      "idend": 30,
      "ala": "1A402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 9,
      "matriz": 2,
      "idend": 4,
      "ala": "1A403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 10,
      "matriz": 2,
      "idend": 50,
      "ala": "1A404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 1A3
    {
      "coordx": 4,
      "coordy": 19,
      "matriz": 2,
      "idend": 1,
      "ala": "1A300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 19,
      "matriz": 2,
      "idend": 1,
      "ala": "1A301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 6,
      "coordy": 19,
      "matriz": 2,
      "idend": 1,
      "ala": "1A302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 7,
      "coordy": 19,
      "matriz": 2,
      "idend": 1,
      "ala": "1A303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 19,
      "matriz": 2,
      "idend": 1,
      "ala": "1A304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 19,
      "matriz": 2,
      "idend": 20,
      "ala": "1A305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 19,
      "matriz": 2,
      "idend": 21,
      "ala": "1A306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 19,
      "matriz": 2,
      "idend": 22,
      "ala": "1A307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 19,
      "matriz": 2,
      "idend": 23,
      "ala": "1A308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 19,
      "matriz": 2,
      "idend": 24,
      "ala": "1A309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 19,
      "matriz": 2,
      "idend": 25,
      "ala": "1A310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 19,
      "matriz": 2,
      "idend": 26,
      "ala": "1A311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 19,
      "matriz": 2,
      "idend": 8,
      "ala": "1A312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 19,
      "matriz": 2,
      "idend": 9,
      "ala": "1A313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 18,
      "coordy": 19,
      "matriz": 2,
      "idend": 10,
      "ala": "1A314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 19,
      "matriz": 2,
      "idend": 6,
      "ala": "1A315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 1A5
    {
      "coordx": 7,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A500",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A501",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A502",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A503",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A504",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A505",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A506",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A507",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A508",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 1A6
    {
      "coordx": 18,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A600",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A601",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 20,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A602",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 21,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A603",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 22,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A604",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 1A7
    {
      "coordx": 27,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A700",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 28,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A701",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 29,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A702",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 30,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A703",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A704",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 32,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A705",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 33,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A706",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A707",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 35,
      "coordy": 21,
      "matriz": 2,
      "idend": 1,
      "ala": "1A708",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 1A8
    {
      "coordx": 48,
      "coordy": 21,
      "matriz": 2,
      "idend": 30,
      "ala": "1A800",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 49,
      "coordy": 21,
      "matriz": 2,
      "idend": 30,
      "ala": "1A801",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 50,
      "coordy": 21,
      "matriz": 2,
      "idend": 30,
      "ala": "1A802",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 51,
      "coordy": 21,
      "matriz": 2,
      "idend": 30,
      "ala": "1A803",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 52,
      "coordy": 21,
      "matriz": 2,
      "idend": 30,
      "ala": "1A804",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 53,
      "coordy": 21,
      "matriz": 2,
      "idend": 30,
      "ala": "1A805",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 1A2
    {
      "coordx": 22,
      "coordy": 19,
      "matriz": 2,
      "idend": 50,
      "ala": "1A200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 19,
      "matriz": 2,
      "idend": 51,
      "ala": "1A201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 19,
      "matriz": 2,
      "idend": 52,
      "ala": "1A202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 25,
      "coordy": 19,
      "matriz": 2,
      "idend": 53,
      "ala": "1A203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 19,
      "matriz": 2,
      "idend": 54,
      "ala": "1A204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 27,
      "coordy": 19,
      "matriz": 2,
      "idend": 55,
      "ala": "1A205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 28,
      "coordy": 19,
      "matriz": 2,
      "idend": 56,
      "ala": "1A206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 29,
      "coordy": 19,
      "matriz": 2,
      "idend": 57,
      "ala": "1A207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 30,
      "coordy": 19,
      "matriz": 2,
      "idend": 58,
      "ala": "1A208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 19,
      "matriz": 2,
      "idend": 59,
      "ala": "1A209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 32,
      "coordy": 19,
      "matriz": 2,
      "idend": 60,
      "ala": "1A210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 33,
      "coordy": 19,
      "matriz": 2,
      "idend": 61,
      "ala": "1A211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 19,
      "matriz": 2,
      "idend": 62,
      "ala": "1A212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 35,
      "coordy": 19,
      "matriz": 2,
      "idend": 63,
      "ala": "1A213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 22,
      "coordy": 9,
      "matriz": 2,
      "idend": 64,
      "ala": "1A214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 9,
      "matriz": 2,
      "idend": 65,
      "ala": "1A215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 9,
      "matriz": 2,
      "idend": 66,
      "ala": "1A216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 25,
      "coordy": 9,
      "matriz": 2,
      "idend": 67,
      "ala": "1A217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 9,
      "matriz": 2,
      "idend": 68,
      "ala": "1A218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 27,
      "coordy": 9,
      "matriz": 2,
      "idend": 69,
      "ala": "1A219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 28,
      "coordy": 9,
      "matriz": 2,
      "idend": 70,
      "ala": "1A220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 29,
      "coordy": 9,
      "matriz": 2,
      "idend": 71,
      "ala": "1A221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 30,
      "coordy": 9,
      "matriz": 2,
      "idend": 72,
      "ala": "1A222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 9,
      "matriz": 2,
      "idend": 73,
      "ala": "1A223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 32,
      "coordy": 9,
      "matriz": 2,
      "idend": 74,
      "ala": "1A224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 33,
      "coordy": 9,
      "matriz": 2,
      "idend": 75,
      "ala": "1A225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 9,
      "matriz": 2,
      "idend": 76,
      "ala": "1A226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 35,
      "coordy": 9,
      "matriz": 2,
      "idend": 77,
      "ala": "1A227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 1A1
    {
      "coordx": 40,
      "coordy": 9,
      "matriz": 2,
      "idend": 78,
      "ala": "1A116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },

    {
      "coordx": 41,
      "coordy": 9,
      "matriz": 2,
      "idend": 79,
      "ala": "1A117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 42,
      "coordy": 9,
      "matriz": 2,
      "idend": 80,
      "ala": "1A118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 43,
      "coordy": 9,
      "matriz": 2,
      "idend": 81,
      "ala": "1A119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 44,
      "coordy": 9,
      "matriz": 2,
      "idend": 82,
      "ala": "1A120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 45,
      "coordy": 9,
      "matriz": 2,
      "idend": 83,
      "ala": "1A121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 46,
      "coordy": 9,
      "matriz": 2,
      "idend": 84,
      "ala": "1A122",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 40,
      "coordy": 19,
      "matriz": 2,
      "idend": 85,
      "ala": "1A100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 41,
      "coordy": 19,
      "matriz": 2,
      "idend": 87,
      "ala": "1A101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 42,
      "coordy": 19,
      "matriz": 2,
      "idend": 88,
      "ala": "1A102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 43,
      "coordy": 19,
      "matriz": 2,
      "idend": 89,
      "ala": "1A103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 44,
      "coordy": 19,
      "matriz": 2,
      "idend": 90,
      "ala": "1A104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 45,
      "coordy": 19,
      "matriz": 2,
      "idend": 85,
      "ala": "1A105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 46,
      "coordy": 19,
      "matriz": 2,
      "idend": 91,
      "ala": "1A106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 47,
      "coordy": 19,
      "matriz": 2,
      "idend": 92,
      "ala": "1A107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 48,
      "coordy": 19,
      "matriz": 2,
      "idend": 93,
      "ala": "1A108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 49,
      "coordy": 17,
      "matriz": 2,
      "idend": 94,
      "ala": "1A109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 50,
      "coordy": 17,
      "matriz": 2,
      "idend": 95,
      "ala": "1A110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 51,
      "coordy": 17,
      "matriz": 2,
      "idend": 96,
      "ala": "1A111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 52,
      "coordy": 15,
      "matriz": 2,
      "idend": 97,
      "ala": "1A112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 53,
      "coordy": 15,
      "matriz": 2,
      "idend": 98,
      "ala": "1A113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 15,
      "matriz": 2,
      "idend": 99,
      "ala": "1A114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 55,
      "coordy": 15,
      "matriz": 2,
      "idend": 100,
      "ala": "1A115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //FORÇAR MATRIZ 2
    {
      "coordx": 56,
      "coordy": 1,
      "matriz": 2,
      "idend": 101,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 57,
      "coordy": 1,
      "matriz": 2,
      "idend": 102,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 30,
      "matriz": 2,
      "idend": 103,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },

    //BLOCOS VERTICAIS DIREITA
    {
      "coordx": 40,
      "coordy": 20,
      "matriz": 2,
      "idend": 86,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 11,
      "matriz": 2,
      "idend": 40,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 12,
      "matriz": 2,
      "idend": 41,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 13,
      "matriz": 2,
      "idend": 42,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 14,
      "matriz": 2,
      "idend": 43,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 15,
      "matriz": 2,
      "idend": 44,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 16,
      "matriz": 2,
      "idend": 45,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 17,
      "matriz": 2,
      "idend": 46,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 18,
      "matriz": 2,
      "idend": 47,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 22,
      "matriz": 2,
      "idend": 1,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 23,
      "matriz": 2,
      "idend": 1,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 24,
      "matriz": 2,
      "idend": 1,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 25,
      "matriz": 2,
      "idend": 1,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 26,
      "matriz": 2,
      "idend": 1,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 27,
      "matriz": 2,
      "idend": 1,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //MATRIZ 3
    //BLOCO 3B4
    {
      "coordx": 1,
      "coordy": 4,
      "matriz": 3,
      "idend": 1,
      "ala": "3B400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 5,
      "matriz": 3,
      "idend": 1,
      "ala": "3B401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 6,
      "matriz": 3,
      "idend": 1,
      "ala": "3B402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 7,
      "matriz": 3,
      "idend": 1,
      "ala": "3B403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 8,
      "matriz": 3,
      "idend": 1,
      "ala": "3B404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 9,
      "matriz": 3,
      "idend": 1,
      "ala": "3B405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 10,
      "matriz": 3,
      "idend": 1,
      "ala": "3B406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 11,
      "matriz": 3,
      "idend": 1,
      "ala": "3B407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 12,
      "matriz": 3,
      "idend": 1,
      "ala": "3B408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 13,
      "matriz": 3,
      "idend": 1,
      "ala": "3B409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 14,
      "matriz": 3,
      "idend": 1,
      "ala": "3B410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 15,
      "matriz": 3,
      "idend": 1,
      "ala": "3B411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 16,
      "matriz": 3,
      "idend": 1,
      "ala": "3B412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 17,
      "matriz": 3,
      "idend": 1,
      "ala": "3B413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 18,
      "matriz": 3,
      "idend": 1,
      "ala": "3B414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B4
    {
      "coordx": 2,
      "coordy": 21,
      "matriz": 3,
      "idend": 8,
      "ala": "4B400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 22,
      "matriz": 3,
      "idend": 1,
      "ala": "4B401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 23,
      "matriz": 3,
      "idend": 1,
      "ala": "4B402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 24,
      "matriz": 3,
      "idend": 1,
      "ala": "4B403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 25,
      "matriz": 3,
      "idend": 1,
      "ala": "4B404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 26,
      "matriz": 3,
      "idend": 1,
      "ala": "4B405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 27,
      "matriz": 3,
      "idend": 1,
      "ala": "4B406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 28,
      "matriz": 3,
      "idend": 1,
      "ala": "4B407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 29,
      "matriz": 3,
      "idend": 1,
      "ala": "4B408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 30,
      "matriz": 3,
      "idend": 1,
      "ala": "4B409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 31,
      "matriz": 3,
      "idend": 1,
      "ala": "4B410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 32,
      "matriz": 3,
      "idend": 1,
      "ala": "4B411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 33,
      "matriz": 3,
      "idend": 1,
      "ala": "4B412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 34,
      "matriz": 3,
      "idend": 1,
      "ala": "4B413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 35,
      "matriz": 3,
      "idend": 1,
      "ala": "4B414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 36,
      "matriz": 3,
      "idend": 1,
      "ala": "4B415",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 37,
      "matriz": 3,
      "idend": 1,
      "ala": "4B416",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 38,
      "matriz": 3,
      "idend": 1,
      "ala": "4B417",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 39,
      "matriz": 3,
      "idend": 1,
      "ala": "4B418",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 40,
      "matriz": 3,
      "idend": 1,
      "ala": "4B419",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 41,
      "matriz": 3,
      "idend": 99,
      "ala": "4B420",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 3B5
    {
      "coordx": 17,
      "coordy": 4,
      "matriz": 3,
      "idend": 1,
      "ala": "3B500",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 5,
      "matriz": 3,
      "idend": 1,
      "ala": "3B501",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 6,
      "matriz": 3,
      "idend": 1,
      "ala": "3B502",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 7,
      "matriz": 3,
      "idend": 1,
      "ala": "3B503",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 8,
      "matriz": 3,
      "idend": 1,
      "ala": "3B504",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 9,
      "matriz": 3,
      "idend": 1,
      "ala": "3B505",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 10,
      "matriz": 3,
      "idend": 1,
      "ala": "3B506",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 11,
      "matriz": 3,
      "idend": 1,
      "ala": "3B507",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 12,
      "matriz": 3,
      "idend": 1,
      "ala": "3B508",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 13,
      "matriz": 3,
      "idend": 1,
      "ala": "3B509",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 14,
      "matriz": 3,
      "idend": 1,
      "ala": "3B510",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 15,
      "matriz": 3,
      "idend": 1,
      "ala": "3B511",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 16,
      "matriz": 3,
      "idend": 1,
      "ala": "3B512",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 17,
      "matriz": 3,
      "idend": 1,
      "ala": "3B513",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 18,
      "matriz": 3,
      "idend": 1,
      "ala": "3B514",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B5
    {
      "coordx": 16,
      "coordy": 21,
      "matriz": 3,
      "idend": 1,
      "ala": "4B500",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 22,
      "matriz": 3,
      "idend": 1,
      "ala": "4B501",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 23,
      "matriz": 3,
      "idend": 1,
      "ala": "4B502",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 24,
      "matriz": 3,
      "idend": 1,
      "ala": "4B503",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 25,
      "matriz": 3,
      "idend": 1,
      "ala": "4B504",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 26,
      "matriz": 3,
      "idend": 1,
      "ala": "4B505",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 27,
      "matriz": 3,
      "idend": 1,
      "ala": "4B506",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 28,
      "matriz": 3,
      "idend": 1,
      "ala": "4B507",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 29,
      "matriz": 3,
      "idend": 1,
      "ala": "4B508",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 30,
      "matriz": 3,
      "idend": 1,
      "ala": "4B509",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 31,
      "matriz": 3,
      "idend": 1,
      "ala": "4B510",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 32,
      "matriz": 3,
      "idend": 1,
      "ala": "4B511",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 33,
      "matriz": 3,
      "idend": 1,
      "ala": "4B512",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 34,
      "matriz": 3,
      "idend": 1,
      "ala": "4B513",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 35,
      "matriz": 3,
      "idend": 1,
      "ala": "4B514",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 36,
      "matriz": 3,
      "idend": 1,
      "ala": "4B515",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 37,
      "matriz": 3,
      "idend": 1,
      "ala": "4B516",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 38,
      "matriz": 3,
      "idend": 1,
      "ala": "4B517",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 39,
      "matriz": 3,
      "idend": 1,
      "ala": "4B518",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 40,
      "matriz": 3,
      "idend": 1,
      "ala": "4B519",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 41,
      "matriz": 3,
      "idend": 1,
      "ala": "4B520",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 3B6
    {
      "coordx": 19,
      "coordy": 1,
      "matriz": 3,
      "idend": 1,
      "ala": "3B600",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 2,
      "matriz": 3,
      "idend": 1,
      "ala": "3B601",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 3,
      "matriz": 3,
      "idend": 1,
      "ala": "3B602",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 4,
      "matriz": 3,
      "idend": 1,
      "ala": "3B603",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 5,
      "matriz": 3,
      "idend": 1,
      "ala": "3B604",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 6,
      "matriz": 3,
      "idend": 1,
      "ala": "3B605",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 7,
      "matriz": 3,
      "idend": 1,
      "ala": "3B606",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 8,
      "matriz": 3,
      "idend": 1,
      "ala": "3B607",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 9,
      "matriz": 3,
      "idend": 1,
      "ala": "3B608",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 10,
      "matriz": 3,
      "idend": 1,
      "ala": "3B609",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 11,
      "matriz": 3,
      "idend": 1,
      "ala": "3B610",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 12,
      "matriz": 3,
      "idend": 1,
      "ala": "3B611",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 13,
      "matriz": 3,
      "idend": 1,
      "ala": "3B612",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 14,
      "matriz": 3,
      "idend": 1,
      "ala": "3B613",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 15,
      "matriz": 3,
      "idend": 1,
      "ala": "3B614",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 16,
      "matriz": 3,
      "idend": 1,
      "ala": "3B615",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 17,
      "matriz": 3,
      "idend": 1,
      "ala": "3B616",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 18,
      "matriz": 3,
      "idend": 1,
      "ala": "3B617",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B6
    {
      "coordx": 20,
      "coordy": 21,
      "matriz": 3,
      "idend": 30,
      "ala": "4B600",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 22,
      "matriz": 3,
      "idend": 1,
      "ala": "4B601",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 23,
      "matriz": 3,
      "idend": 1,
      "ala": "4B602",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 24,
      "matriz": 3,
      "idend": 1,
      "ala": "4B603",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 25,
      "matriz": 3,
      "idend": 1,
      "ala": "4B604",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 26,
      "matriz": 3,
      "idend": 1,
      "ala": "4B605",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 27,
      "matriz": 3,
      "idend": 1,
      "ala": "4B606",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 28,
      "matriz": 3,
      "idend": 1,
      "ala": "4B607",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 29,
      "matriz": 3,
      "idend": 1,
      "ala": "4B608",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 30,
      "matriz": 3,
      "idend": 1,
      "ala": "4B609",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 31,
      "matriz": 3,
      "idend": 1,
      "ala": "4B610",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 32,
      "matriz": 3,
      "idend": 1,
      "ala": "4B611",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 33,
      "matriz": 3,
      "idend": 1,
      "ala": "4B612",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 34,
      "matriz": 3,
      "idend": 1,
      "ala": "4B613",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 35,
      "matriz": 3,
      "idend": 1,
      "ala": "4B614",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 36,
      "matriz": 3,
      "idend": 1,
      "ala": "4B615",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 37,
      "matriz": 3,
      "idend": 1,
      "ala": "4B616",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 38,
      "matriz": 3,
      "idend": 1,
      "ala": "4B617",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 39,
      "matriz": 3,
      "idend": 1,
      "ala": "4B618",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 40,
      "matriz": 3,
      "idend": 1,
      "ala": "4B619",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 41,
      "matriz": 3,
      "idend": 1,
      "ala": "4B620",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 3B7
    {
      "coordx": 31,
      "coordy": 1,
      "matriz": 3,
      "idend": 1,
      "ala": "3B700",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 2,
      "matriz": 3,
      "idend": 1,
      "ala": "3B701",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 3,
      "matriz": 3,
      "idend": 1,
      "ala": "3B702",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 4,
      "matriz": 3,
      "idend": 1,
      "ala": "3B703",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 5,
      "matriz": 3,
      "idend": 1,
      "ala": "3B704",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 6,
      "matriz": 3,
      "idend": 1,
      "ala": "3B705",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 7,
      "matriz": 3,
      "idend": 1,
      "ala": "3B706",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 8,
      "matriz": 3,
      "idend": 1,
      "ala": "3B707",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 9,
      "matriz": 3,
      "idend": 1,
      "ala": "3B708",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 10,
      "matriz": 3,
      "idend": 1,
      "ala": "3B709",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 11,
      "matriz": 3,
      "idend": 1,
      "ala": "3B710",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 12,
      "matriz": 3,
      "idend": 1,
      "ala": "3B711",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 13,
      "matriz": 3,
      "idend": 1,
      "ala": "3B712",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 14,
      "matriz": 3,
      "idend": 1,
      "ala": "3B713",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 15,
      "matriz": 3,
      "idend": 1,
      "ala": "3B714",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 16,
      "matriz": 3,
      "idend": 1,
      "ala": "3B715",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 17,
      "matriz": 3,
      "idend": 1,
      "ala": "3B716",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 18,
      "matriz": 3,
      "idend": 1,
      "ala": "3B717",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B7
    {
      "coordx": 33,
      "coordy": 21,
      "matriz": 3,
      "idend": 110,
      "ala": "4B700",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 22,
      "matriz": 3,
      "idend": 1,
      "ala": "4B701",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 23,
      "matriz": 3,
      "idend": 1,
      "ala": "4B702",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 24,
      "matriz": 3,
      "idend": 1,
      "ala": "4B703",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 25,
      "matriz": 3,
      "idend": 1,
      "ala": "4B704",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 26,
      "matriz": 3,
      "idend": 1,
      "ala": "4B705",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 27,
      "matriz": 3,
      "idend": 1,
      "ala": "4B706",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 28,
      "matriz": 3,
      "idend": 1,
      "ala": "4B707",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 29,
      "matriz": 3,
      "idend": 1,
      "ala": "4B708",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 30,
      "matriz": 3,
      "idend": 1,
      "ala": "4B709",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 31,
      "matriz": 3,
      "idend": 1,
      "ala": "4B710",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 32,
      "matriz": 3,
      "idend": 1,
      "ala": "4B711",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 33,
      "matriz": 3,
      "idend": 1,
      "ala": "4B712",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 34,
      "matriz": 3,
      "idend": 1,
      "ala": "4B713",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 35,
      "matriz": 3,
      "idend": 1,
      "ala": "4B714",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 36,
      "matriz": 3,
      "idend": 1,
      "ala": "4B715",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 37,
      "matriz": 3,
      "idend": 1,
      "ala": "4B716",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 38,
      "matriz": 3,
      "idend": 1,
      "ala": "4B717",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 39,
      "matriz": 3,
      "idend": 1,
      "ala": "4B718",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 40,
      "matriz": 3,
      "idend": 1,
      "ala": "4B719",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 41,
      "matriz": 3,
      "idend": 1,
      "ala": "4B720",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 3B8
    {
      "coordx": 37,
      "coordy": 1,
      "matriz": 3,
      "idend": 120,
      "ala": "3B800",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 2,
      "matriz": 3,
      "idend": 121,
      "ala": "3B801",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 3,
      "matriz": 3,
      "idend": 122,
      "ala": "3B802",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 4,
      "matriz": 3,
      "idend": 123,
      "ala": "3B803",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 5,
      "matriz": 3,
      "idend": 124,
      "ala": "3B804",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 6,
      "matriz": 3,
      "idend": 125,
      "ala": "3B805",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 7,
      "matriz": 3,
      "idend": 126,
      "ala": "3B806",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 8,
      "matriz": 3,
      "idend": 127,
      "ala": "3B807",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 9,
      "matriz": 3,
      "idend": 1,
      "ala": "3B808",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 10,
      "matriz": 3,
      "idend": 1,
      "ala": "3B809",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 11,
      "matriz": 3,
      "idend": 1,
      "ala": "3B810",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 12,
      "matriz": 3,
      "idend": 1,
      "ala": "3B811",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 13,
      "matriz": 3,
      "idend": 1,
      "ala": "3B812",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 14,
      "matriz": 3,
      "idend": 1,
      "ala": "3B813",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 15,
      "matriz": 3,
      "idend": 1,
      "ala": "3B814",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 16,
      "matriz": 3,
      "idend": 1,
      "ala": "3B815",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 17,
      "matriz": 3,
      "idend": 1,
      "ala": "3B816",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 18,
      "matriz": 3,
      "idend": 1,
      "ala": "3B817",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B8
    {
      "coordx": 37,
      "coordy": 21,
      "matriz": 3,
      "idend": 128,
      "ala": "4B800",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 22,
      "matriz": 3,
      "idend": 1,
      "ala": "4B801",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 23,
      "matriz": 3,
      "idend": 1,
      "ala": "4B802",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 24,
      "matriz": 3,
      "idend": 1,
      "ala": "4B803",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 25,
      "matriz": 3,
      "idend": 1,
      "ala": "4B804",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 26,
      "matriz": 3,
      "idend": 1,
      "ala": "4B805",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 27,
      "matriz": 3,
      "idend": 1,
      "ala": "4B806",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 28,
      "matriz": 3,
      "idend": 1,
      "ala": "4B807",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 29,
      "matriz": 3,
      "idend": 1,
      "ala": "4B808",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 30,
      "matriz": 3,
      "idend": 1,
      "ala": "4B809",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 31,
      "matriz": 3,
      "idend": 1,
      "ala": "4B810",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 32,
      "matriz": 3,
      "idend": 1,
      "ala": "4B811",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 33,
      "matriz": 3,
      "idend": 1,
      "ala": "4B812",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 34,
      "matriz": 3,
      "idend": 1,
      "ala": "4B813",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 35,
      "matriz": 3,
      "idend": 1,
      "ala": "4B814",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 36,
      "matriz": 3,
      "idend": 1,
      "ala": "4B815",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 37,
      "matriz": 3,
      "idend": 1,
      "ala": "4B816",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 38,
      "matriz": 3,
      "idend": 1,
      "ala": "4B817",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 39,
      "matriz": 3,
      "idend": 1,
      "ala": "4B818",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 40,
      "matriz": 3,
      "idend": 1,
      "ala": "4B819",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 36,
      "coordy": 41,
      "matriz": 3,
      "idend": 1,
      "ala": "4B820",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 3B9
    {
      "coordx": 56,
      "coordy": 1,
      "matriz": 3,
      "idend": 1,
      "ala": "3B900",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 2,
      "matriz": 3,
      "idend": 1,
      "ala": "3B901",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 3,
      "matriz": 3,
      "idend": 1,
      "ala": "3B902",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 4,
      "matriz": 3,
      "idend": 1,
      "ala": "3B903",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 5,
      "matriz": 3,
      "idend": 1,
      "ala": "3B904",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 6,
      "matriz": 3,
      "idend": 1,
      "ala": "3B905",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 7,
      "matriz": 3,
      "idend": 1,
      "ala": "3B906",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 8,
      "matriz": 3,
      "idend": 1,
      "ala": "3B907",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 9,
      "matriz": 3,
      "idend": 1,
      "ala": "3B908",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 10,
      "matriz": 3,
      "idend": 1,
      "ala": "3B909",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 11,
      "matriz": 3,
      "idend": 1,
      "ala": "3B910",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 12,
      "matriz": 3,
      "idend": 1,
      "ala": "3B911",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 13,
      "matriz": 3,
      "idend": 1,
      "ala": "3B912",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 14,
      "matriz": 3,
      "idend": 1,
      "ala": "3B913",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 15,
      "matriz": 3,
      "idend": 1,
      "ala": "3B914",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 16,
      "matriz": 3,
      "idend": 1,
      "ala": "3B915",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 17,
      "matriz": 3,
      "idend": 1,
      "ala": "3B916",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 18,
      "matriz": 3,
      "idend": 1,
      "ala": "3B917",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 4B9
    {
      "coordx": 55,
      "coordy": 21,
      "matriz": 3,
      "idend": 135,
      "ala": "4B900",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 22,
      "matriz": 3,
      "idend": 1,
      "ala": "4B901",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 23,
      "matriz": 3,
      "idend": 1,
      "ala": "4B902",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 24,
      "matriz": 3,
      "idend": 1,
      "ala": "4B903",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 25,
      "matriz": 3,
      "idend": 1,
      "ala": "4B904",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 26,
      "matriz": 3,
      "idend": 1,
      "ala": "4B905",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 27,
      "matriz": 3,
      "idend": 1,
      "ala": "4B906",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 28,
      "matriz": 3,
      "idend": 1,
      "ala": "4B907",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 29,
      "matriz": 3,
      "idend": 1,
      "ala": "4B908",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 30,
      "matriz": 3,
      "idend": 1,
      "ala": "4B909",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 31,
      "matriz": 3,
      "idend": 1,
      "ala": "4B910",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 32,
      "matriz": 3,
      "idend": 1,
      "ala": "4B911",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 33,
      "matriz": 3,
      "idend": 1,
      "ala": "4B912",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 34,
      "matriz": 3,
      "idend": 1,
      "ala": "4B913",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 35,
      "matriz": 3,
      "idend": 1,
      "ala": "4B914",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 36,
      "matriz": 3,
      "idend": 1,
      "ala": "4B915",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 37,
      "matriz": 3,
      "idend": 1,
      "ala": "4B916",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 38,
      "matriz": 3,
      "idend": 1,
      "ala": "4B917",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 39,
      "matriz": 3,
      "idend": 1,
      "ala": "4B918",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 40,
      "matriz": 3,
      "idend": 1,
      "ala": "4B919",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 56,
      "coordy": 41,
      "matriz": 3,
      "idend": 1,
      "ala": "4B920",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //FORÇAR MATRIZ 3
    {
      "coordx": 1,
      "coordy": 42,
      "matriz": 3,
      "idend": 100,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 57,
      "coordy": 42,
      "matriz": 3,
      "idend": 200,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //MATRIZ 4
    //BLOCO 6G400
    {
      "coordx": 2,
      "coordy": 2,
      "matriz": 4,
      "idend": 1,
      "ala": "6G400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 3,
      "matriz": 4,
      "idend": 1,
      "ala": "6G401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 4,
      "matriz": 4,
      "idend": 1,
      "ala": "6G402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 5,
      "matriz": 4,
      "idend": 1,
      "ala": "6G403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 6,
      "matriz": 4,
      "idend": 1,
      "ala": "6G404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 7,
      "matriz": 4,
      "idend": 1,
      "ala": "6G405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 8,
      "matriz": 4,
      "idend": 1,
      "ala": "6G406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 9,
      "matriz": 4,
      "idend": 1,
      "ala": "6G407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 10,
      "matriz": 4,
      "idend": 1,
      "ala": "6G408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 11,
      "matriz": 4,
      "idend": 1,
      "ala": "6G409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 12,
      "matriz": 4,
      "idend": 1,
      "ala": "6G410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 13,
      "matriz": 4,
      "idend": 1,
      "ala": "6G411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 14,
      "matriz": 4,
      "idend": 1,
      "ala": "6G412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 15,
      "matriz": 4,
      "idend": 1,
      "ala": "6G413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 16,
      "matriz": 4,
      "idend": 1,
      "ala": "6G414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 17,
      "matriz": 4,
      "idend": 1,
      "ala": "6G415",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 18,
      "matriz": 4,
      "idend": 1,
      "ala": "6G416",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 19,
      "matriz": 4,
      "idend": 1,
      "ala": "6G417",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 20,
      "matriz": 4,
      "idend": 1,
      "ala": "6G418",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 21,
      "matriz": 4,
      "idend": 1,
      "ala": "6G419",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 22,
      "matriz": 4,
      "idend": 1,
      "ala": "6G420",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 6G300
    {
      "coordx": 3,
      "coordy": 25,
      "matriz": 4,
      "idend": 200,
      "ala": "6G300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 26,
      "matriz": 4,
      "idend": 1,
      "ala": "6G301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 27,
      "matriz": 4,
      "idend": 1,
      "ala": "6G302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 28,
      "matriz": 4,
      "idend": 1,
      "ala": "6G303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 29,
      "matriz": 4,
      "idend": 1,
      "ala": "6G304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 30,
      "matriz": 4,
      "idend": 1,
      "ala": "6G305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 31,
      "matriz": 4,
      "idend": 1,
      "ala": "6G306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 32,
      "matriz": 4,
      "idend": 1,
      "ala": "6G307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 33,
      "matriz": 4,
      "idend": 1,
      "ala": "6G308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 34,
      "matriz": 4,
      "idend": 1,
      "ala": "6G309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 35,
      "matriz": 4,
      "idend": 1,
      "ala": "6G310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 36,
      "matriz": 4,
      "idend": 1,
      "ala": "6G311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 37,
      "matriz": 4,
      "idend": 1,
      "ala": "6G312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 38,
      "matriz": 4,
      "idend": 1,
      "ala": "6G313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 39,
      "matriz": 4,
      "idend": 1,
      "ala": "6G314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 40,
      "matriz": 4,
      "idend": 1,
      "ala": "6G315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 41,
      "matriz": 4,
      "idend": 1,
      "ala": "6G316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 42,
      "matriz": 4,
      "idend": 1,
      "ala": "6G317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 43,
      "matriz": 4,
      "idend": 1,
      "ala": "6G318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 44,
      "matriz": 4,
      "idend": 1,
      "ala": "6G319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 45,
      "matriz": 4,
      "idend": 1,
      "ala": "6G320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 46,
      "matriz": 4,
      "idend": 1,
      "ala": "6G321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 47,
      "matriz": 4,
      "idend": 1,
      "ala": "6G322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 48,
      "matriz": 4,
      "idend": 1,
      "ala": "6G323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 49,
      "matriz": 4,
      "idend": 1,
      "ala": "6G324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 6G200
    {
      "coordx": 3,
      "coordy": 52,
      "matriz": 4,
      "idend": 30,
      "ala": "6G200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 53,
      "matriz": 4,
      "idend": 1,
      "ala": "6G201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 54,
      "matriz": 4,
      "idend": 1,
      "ala": "6G202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 55,
      "matriz": 4,
      "idend": 1,
      "ala": "6G203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 56,
      "matriz": 4,
      "idend": 1,
      "ala": "6G204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 57,
      "matriz": 4,
      "idend": 1,
      "ala": "6G205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 58,
      "matriz": 4,
      "idend": 1,
      "ala": "6G206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 59,
      "matriz": 4,
      "idend": 1,
      "ala": "6G207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 60,
      "matriz": 4,
      "idend": 1,
      "ala": "6G208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 61,
      "matriz": 4,
      "idend": 1,
      "ala": "6G209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 62,
      "matriz": 4,
      "idend": 1,
      "ala": "6G210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 63,
      "matriz": 4,
      "idend": 1,
      "ala": "6G211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 64,
      "matriz": 4,
      "idend": 1,
      "ala": "6G212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 65,
      "matriz": 4,
      "idend": 1,
      "ala": "6G213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 66,
      "matriz": 4,
      "idend": 1,
      "ala": "6G214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 67,
      "matriz": 4,
      "idend": 1,
      "ala": "6G215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 68,
      "matriz": 4,
      "idend": 1,
      "ala": "6G216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 69,
      "matriz": 4,
      "idend": 1,
      "ala": "6G217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 70,
      "matriz": 4,
      "idend": 1,
      "ala": "6G218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 71,
      "matriz": 4,
      "idend": 1,
      "ala": "6G219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 72,
      "matriz": 4,
      "idend": 1,
      "ala": "6G220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 73,
      "matriz": 4,
      "idend": 1,
      "ala": "6G221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 74,
      "matriz": 4,
      "idend": 1,
      "ala": "6G222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 75,
      "matriz": 4,
      "idend": 1,
      "ala": "6G223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 76,
      "matriz": 4,
      "idend": 1,
      "ala": "6G224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 77,
      "matriz": 4,
      "idend": 1,
      "ala": "6G225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 78,
      "matriz": 4,
      "idend": 1,
      "ala": "6G226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 79,
      "matriz": 4,
      "idend": 1,
      "ala": "6G227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 80,
      "matriz": 4,
      "idend": 1,
      "ala": "6G228",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 81,
      "matriz": 4,
      "idend": 1,
      "ala": "6G229",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 82,
      "matriz": 4,
      "idend": 1,
      "ala": "6G230",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 83,
      "matriz": 4,
      "idend": 1,
      "ala": "6G231",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 6G100
    {
      "coordx": 3,
      "coordy": 86,
      "matriz": 4,
      "idend": 40,
      "ala": "6G100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 87,
      "matriz": 4,
      "idend": 1,
      "ala": "6G101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 88,
      "matriz": 4,
      "idend": 1,
      "ala": "6G102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 89,
      "matriz": 4,
      "idend": 1,
      "ala": "6G103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 90,
      "matriz": 4,
      "idend": 1,
      "ala": "6G104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 91,
      "matriz": 4,
      "idend": 1,
      "ala": "6G105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 92,
      "matriz": 4,
      "idend": 1,
      "ala": "6G106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 93,
      "matriz": 4,
      "idend": 1,
      "ala": "6G107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 94,
      "matriz": 4,
      "idend": 1,
      "ala": "6G108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 95,
      "matriz": 4,
      "idend": 1,
      "ala": "6G109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 96,
      "matriz": 4,
      "idend": 1,
      "ala": "6G110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 97,
      "matriz": 4,
      "idend": 1,
      "ala": "6G111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 98,
      "matriz": 4,
      "idend": 1,
      "ala": "6G112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 99,
      "matriz": 4,
      "idend": 1,
      "ala": "6G113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 100,
      "matriz": 4,
      "idend": 1,
      "ala": "6G114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 101,
      "matriz": 4,
      "idend": 1,
      "ala": "6G115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 102,
      "matriz": 4,
      "idend": 1,
      "ala": "6G116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 103,
      "matriz": 4,
      "idend": 1,
      "ala": "6G117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 104,
      "matriz": 4,
      "idend": 1,
      "ala": "6G118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 105,
      "matriz": 4,
      "idend": 1,
      "ala": "6G119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 106,
      "matriz": 4,
      "idend": 1,
      "ala": "6G120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 107,
      "matriz": 4,
      "idend": 1,
      "ala": "6G121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //FORÇAR MATRIZ 4
    {
      "coordx": 11,
      "coordy": 108,
      "matriz": 4,
      "idend": 101,
      "ala": "",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //MATRIZ 5
    //BLOCO 5F4
    {
      "coordx": 1,
      "coordy": 1,
      "matriz": 5,
      "idend": 10000,
      "ala": "5F400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 1,
      "matriz": 5,
      "idend": 1,
      "ala": "5F400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 2,
      "matriz": 5,
      "idend": 1,
      "ala": "5F401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 3,
      "matriz": 5,
      "idend": 1,
      "ala": "5F402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 4,
      "matriz": 5,
      "idend": 1,
      "ala": "5F403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 5,
      "matriz": 5,
      "idend": 1,
      "ala": "5F404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 6,
      "matriz": 5,
      "idend": 1,
      "ala": "5F405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 7,
      "matriz": 5,
      "idend": 1,
      "ala": "5F406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 8,
      "matriz": 5,
      "idend": 1,
      "ala": "5F407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 9,
      "matriz": 5,
      "idend": 1,
      "ala": "5F408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 10,
      "matriz": 5,
      "idend": 1,
      "ala": "5F409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 11,
      "matriz": 5,
      "idend": 1,
      "ala": "5F410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 12,
      "matriz": 5,
      "idend": 1,
      "ala": "5F411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 13,
      "matriz": 5,
      "idend": 1,
      "ala": "5F412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 14,
      "matriz": 5,
      "idend": 1,
      "ala": "5F413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 15,
      "matriz": 5,
      "idend": 1,
      "ala": "5F414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5F3
    {
      "coordx": 12,
      "coordy": 18,
      "matriz": 5,
      "idend": 200,
      "ala": "5F300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 19,
      "matriz": 5,
      "idend": 1,
      "ala": "5F301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 20,
      "matriz": 5,
      "idend": 1,
      "ala": "5F302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 21,
      "matriz": 5,
      "idend": 1,
      "ala": "5F303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 22,
      "matriz": 5,
      "idend": 1,
      "ala": "5F304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 23,
      "matriz": 5,
      "idend": 1,
      "ala": "5F305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 24,
      "matriz": 5,
      "idend": 1,
      "ala": "5F306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 25,
      "matriz": 5,
      "idend": 1,
      "ala": "5F307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 26,
      "matriz": 5,
      "idend": 1,
      "ala": "5F308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 27,
      "matriz": 5,
      "idend": 1,
      "ala": "5F309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 28,
      "matriz": 5,
      "idend": 1,
      "ala": "5F310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 29,
      "matriz": 5,
      "idend": 1,
      "ala": "5F311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 30,
      "matriz": 5,
      "idend": 1,
      "ala": "5F312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 31,
      "matriz": 5,
      "idend": 1,
      "ala": "5F313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 32,
      "matriz": 5,
      "idend": 1,
      "ala": "5F314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 33,
      "matriz": 5,
      "idend": 1,
      "ala": "5F315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 34,
      "matriz": 5,
      "idend": 1,
      "ala": "5F316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 35,
      "matriz": 5,
      "idend": 1,
      "ala": "5F317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 36,
      "matriz": 5,
      "idend": 1,
      "ala": "5F318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 37,
      "matriz": 5,
      "idend": 1,
      "ala": "5F319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 38,
      "matriz": 5,
      "idend": 1,
      "ala": "5F320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 39,
      "matriz": 5,
      "idend": 1,
      "ala": "5F321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 40,
      "matriz": 5,
      "idend": 1,
      "ala": "5F322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 41,
      "matriz": 5,
      "idend": 1,
      "ala": "5F323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 42,
      "matriz": 5,
      "idend": 1,
      "ala": "5F324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5F2
    {
      "coordx": 12,
      "coordy": 45,
      "matriz": 5,
      "idend": 300,
      "ala": "5F200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 46,
      "matriz": 5,
      "idend": 1,
      "ala": "5F201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 47,
      "matriz": 5,
      "idend": 1,
      "ala": "5F202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 48,
      "matriz": 5,
      "idend": 1,
      "ala": "5F203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 49,
      "matriz": 5,
      "idend": 1,
      "ala": "5F204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 50,
      "matriz": 5,
      "idend": 1,
      "ala": "5F205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 51,
      "matriz": 5,
      "idend": 1,
      "ala": "5F206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 52,
      "matriz": 5,
      "idend": 1,
      "ala": "5F207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 53,
      "matriz": 5,
      "idend": 1,
      "ala": "5F208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 54,
      "matriz": 5,
      "idend": 1,
      "ala": "5F209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 55,
      "matriz": 5,
      "idend": 1,
      "ala": "5F210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 56,
      "matriz": 5,
      "idend": 1,
      "ala": "5F211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 57,
      "matriz": 5,
      "idend": 1,
      "ala": "5F212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 58,
      "matriz": 5,
      "idend": 1,
      "ala": "5F213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 59,
      "matriz": 5,
      "idend": 1,
      "ala": "5F214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 60,
      "matriz": 5,
      "idend": 1,
      "ala": "5F215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 61,
      "matriz": 5,
      "idend": 1,
      "ala": "5F216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 62,
      "matriz": 5,
      "idend": 1,
      "ala": "5F217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 63,
      "matriz": 5,
      "idend": 1,
      "ala": "5F218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 64,
      "matriz": 5,
      "idend": 1,
      "ala": "5F219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 65,
      "matriz": 5,
      "idend": 1,
      "ala": "5F220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 66,
      "matriz": 5,
      "idend": 1,
      "ala": "5F221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 67,
      "matriz": 5,
      "idend": 1,
      "ala": "5F222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 68,
      "matriz": 5,
      "idend": 1,
      "ala": "5F223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 69,
      "matriz": 5,
      "idend": 1,
      "ala": "5F224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 70,
      "matriz": 5,
      "idend": 1,
      "ala": "5F225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 71,
      "matriz": 5,
      "idend": 1,
      "ala": "5F226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 72,
      "matriz": 5,
      "idend": 1,
      "ala": "5F227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 73,
      "matriz": 5,
      "idend": 1,
      "ala": "5F228",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 74,
      "matriz": 5,
      "idend": 1,
      "ala": "5F229",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 75,
      "matriz": 5,
      "idend": 1,
      "ala": "5F230",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 76,
      "matriz": 5,
      "idend": 1,
      "ala": "5F231",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5F100
    {
      "coordx": 12,
      "coordy": 79,
      "matriz": 5,
      "idend": 400,
      "ala": "5F100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 80,
      "matriz": 5,
      "idend": 1,
      "ala": "5F101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 81,
      "matriz": 5,
      "idend": 1,
      "ala": "5F102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 82,
      "matriz": 5,
      "idend": 1,
      "ala": "5F103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 83,
      "matriz": 5,
      "idend": 1,
      "ala": "5F104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 84,
      "matriz": 5,
      "idend": 1,
      "ala": "5F105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 85,
      "matriz": 5,
      "idend": 1,
      "ala": "5F106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 86,
      "matriz": 5,
      "idend": 1,
      "ala": "5F107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 87,
      "matriz": 5,
      "idend": 1,
      "ala": "5F108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 88,
      "matriz": 5,
      "idend": 1,
      "ala": "5F109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 89,
      "matriz": 5,
      "idend": 1,
      "ala": "5F110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 90,
      "matriz": 5,
      "idend": 1,
      "ala": "5F111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 91,
      "matriz": 5,
      "idend": 1,
      "ala": "5F112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 92,
      "matriz": 5,
      "idend": 1,
      "ala": "5F113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 93,
      "matriz": 5,
      "idend": 1,
      "ala": "5F114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 94,
      "matriz": 5,
      "idend": 1,
      "ala": "5F115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 95,
      "matriz": 5,
      "idend": 1,
      "ala": "5F116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 96,
      "matriz": 5,
      "idend": 1,
      "ala": "5F117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 97,
      "matriz": 5,
      "idend": 1,
      "ala": "5F118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 98,
      "matriz": 5,
      "idend": 1,
      "ala": "5F119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 99,
      "matriz": 5,
      "idend": 1,
      "ala": "5F120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 100,
      "matriz": 5,
      "idend": 1,
      "ala": "5F121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5E4
    {
      "coordx": 15,
      "coordy": 1,
      "matriz": 5,
      "idend": 1030,
      "ala": "5E400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 2,
      "matriz": 5,
      "idend": 1,
      "ala": "5E401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 3,
      "matriz": 5,
      "idend": 1,
      "ala": "5E402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 4,
      "matriz": 5,
      "idend": 1,
      "ala": "5E403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 5,
      "matriz": 5,
      "idend": 1,
      "ala": "5E404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 6,
      "matriz": 5,
      "idend": 1,
      "ala": "5E405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 7,
      "matriz": 5,
      "idend": 1,
      "ala": "5E406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 8,
      "matriz": 5,
      "idend": 1,
      "ala": "5E407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 9,
      "matriz": 5,
      "idend": 1,
      "ala": "5E408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 10,
      "matriz": 5,
      "idend": 1,
      "ala": "5E409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 11,
      "matriz": 5,
      "idend": 1,
      "ala": "5E410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 12,
      "matriz": 5,
      "idend": 1,
      "ala": "5E411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 13,
      "matriz": 5,
      "idend": 1,
      "ala": "5E412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 14,
      "matriz": 5,
      "idend": 1,
      "ala": "5E413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 15,
      "matriz": 5,
      "idend": 1,
      "ala": "5E414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5E3
    {
      "coordx": 16,
      "coordy": 18,
      "matriz": 5,
      "idend": 50,
      "ala": "5E300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 19,
      "matriz": 5,
      "idend": 1,
      "ala": "5E301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 20,
      "matriz": 5,
      "idend": 1,
      "ala": "5E302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 21,
      "matriz": 5,
      "idend": 1,
      "ala": "5E303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 22,
      "matriz": 5,
      "idend": 1,
      "ala": "5E304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 23,
      "matriz": 5,
      "idend": 1,
      "ala": "5E305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 24,
      "matriz": 5,
      "idend": 1,
      "ala": "5E306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 25,
      "matriz": 5,
      "idend": 1,
      "ala": "5E307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 26,
      "matriz": 5,
      "idend": 1,
      "ala": "5E308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 27,
      "matriz": 5,
      "idend": 105,
      "ala": "5E309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 28,
      "matriz": 5,
      "idend": 1,
      "ala": "5E310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 29,
      "matriz": 5,
      "idend": 1,
      "ala": "5E311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 30,
      "matriz": 5,
      "idend": 1,
      "ala": "5E312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 31,
      "matriz": 5,
      "idend": 1,
      "ala": "5E313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 32,
      "matriz": 5,
      "idend": 1,
      "ala": "5E314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 33,
      "matriz": 5,
      "idend": 1,
      "ala": "5E315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 34,
      "matriz": 5,
      "idend": 1,
      "ala": "5E316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 35,
      "matriz": 5,
      "idend": 1,
      "ala": "5E317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 36,
      "matriz": 5,
      "idend": 1,
      "ala": "5E318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 37,
      "matriz": 5,
      "idend": 1,
      "ala": "5E319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 38,
      "matriz": 5,
      "idend": 1,
      "ala": "5E320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 39,
      "matriz": 5,
      "idend": 1,
      "ala": "5E321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 40,
      "matriz": 5,
      "idend": 1,
      "ala": "5E322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 41,
      "matriz": 5,
      "idend": 1,
      "ala": "5E323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 42,
      "matriz": 5,
      "idend": 1,
      "ala": "5E324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5E2
    {
      "coordx": 16,
      "coordy": 45,
      "matriz": 5,
      "idend": 112,
      "ala": "5E200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 46,
      "matriz": 5,
      "idend": 1,
      "ala": "5E201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 47,
      "matriz": 5,
      "idend": 1,
      "ala": "5E202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 48,
      "matriz": 5,
      "idend": 1,
      "ala": "5E203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 49,
      "matriz": 5,
      "idend": 1,
      "ala": "5E204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 50,
      "matriz": 5,
      "idend": 1,
      "ala": "5E205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 51,
      "matriz": 5,
      "idend": 1,
      "ala": "5E206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 52,
      "matriz": 5,
      "idend": 1,
      "ala": "5E207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 53,
      "matriz": 5,
      "idend": 1,
      "ala": "5E208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 54,
      "matriz": 5,
      "idend": 1,
      "ala": "5E209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 55,
      "matriz": 5,
      "idend": 1,
      "ala": "5E210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 56,
      "matriz": 5,
      "idend": 1,
      "ala": "5E211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 57,
      "matriz": 5,
      "idend": 1,
      "ala": "5E212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 58,
      "matriz": 5,
      "idend": 1,
      "ala": "5E213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 59,
      "matriz": 5,
      "idend": 1,
      "ala": "5E214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 60,
      "matriz": 5,
      "idend": 1,
      "ala": "5E215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 61,
      "matriz": 5,
      "idend": 1,
      "ala": "5E216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 62,
      "matriz": 5,
      "idend": 1,
      "ala": "5E217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 63,
      "matriz": 5,
      "idend": 1,
      "ala": "5E218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 64,
      "matriz": 5,
      "idend": 1,
      "ala": "5E219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 65,
      "matriz": 5,
      "idend": 1,
      "ala": "5E220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 66,
      "matriz": 5,
      "idend": 1,
      "ala": "5E221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 67,
      "matriz": 5,
      "idend": 1,
      "ala": "5E222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 68,
      "matriz": 5,
      "idend": 1,
      "ala": "5E223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 69,
      "matriz": 5,
      "idend": 1,
      "ala": "5E224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 70,
      "matriz": 5,
      "idend": 1,
      "ala": "5E225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 71,
      "matriz": 5,
      "idend": 1,
      "ala": "5E226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 72,
      "matriz": 5,
      "idend": 1,
      "ala": "5E227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 73,
      "matriz": 5,
      "idend": 1,
      "ala": "5E228",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 74,
      "matriz": 5,
      "idend": 1,
      "ala": "5E229",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 75,
      "matriz": 5,
      "idend": 1,
      "ala": "5E230",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 76,
      "matriz": 5,
      "idend": 1,
      "ala": "5E231",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5E1
    {
      "coordx": 16,
      "coordy": 79,
      "matriz": 5,
      "idend": 113,
      "ala": "5E100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 80,
      "matriz": 5,
      "idend": 1,
      "ala": "5E101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 81,
      "matriz": 5,
      "idend": 1,
      "ala": "5E102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 82,
      "matriz": 5,
      "idend": 1,
      "ala": "5E103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 83,
      "matriz": 5,
      "idend": 1,
      "ala": "5E104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 84,
      "matriz": 5,
      "idend": 1,
      "ala": "5E105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 85,
      "matriz": 5,
      "idend": 1,
      "ala": "5E106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 86,
      "matriz": 5,
      "idend": 1,
      "ala": "5E107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 87,
      "matriz": 5,
      "idend": 1,
      "ala": "5E108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 88,
      "matriz": 5,
      "idend": 1,
      "ala": "5E109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 89,
      "matriz": 5,
      "idend": 1,
      "ala": "5E110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 90,
      "matriz": 5,
      "idend": 1,
      "ala": "5E111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 91,
      "matriz": 5,
      "idend": 1,
      "ala": "5E112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 92,
      "matriz": 5,
      "idend": 1,
      "ala": "5E113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 93,
      "matriz": 5,
      "idend": 1,
      "ala": "5E114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 94,
      "matriz": 5,
      "idend": 1,
      "ala": "5E115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 95,
      "matriz": 5,
      "idend": 1,
      "ala": "5E116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 96,
      "matriz": 5,
      "idend": 1,
      "ala": "5E117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 97,
      "matriz": 5,
      "idend": 1,
      "ala": "5E118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 98,
      "matriz": 5,
      "idend": 1,
      "ala": "5E119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 99,
      "matriz": 5,
      "idend": 1,
      "ala": "5E120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 100,
      "matriz": 5,
      "idend": 1,
      "ala": "5E121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5D4
    {
      "coordx": 31,
      "coordy": 1,
      "matriz": 5,
      "idend": 1,
      "ala": "5D400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 2,
      "matriz": 5,
      "idend": 1,
      "ala": "5D401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 3,
      "matriz": 5,
      "idend": 1,
      "ala": "5D402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 4,
      "matriz": 5,
      "idend": 1,
      "ala": "5D403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 5,
      "matriz": 5,
      "idend": 1,
      "ala": "5D404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 6,
      "matriz": 5,
      "idend": 1,
      "ala": "5D405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 7,
      "matriz": 5,
      "idend": 1,
      "ala": "5D406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 8,
      "matriz": 5,
      "idend": 1,
      "ala": "5D407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 9,
      "matriz": 5,
      "idend": 1,
      "ala": "5D408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 10,
      "matriz": 5,
      "idend": 1,
      "ala": "5D409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 11,
      "matriz": 5,
      "idend": 1,
      "ala": "5D410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 12,
      "matriz": 5,
      "idend": 1,
      "ala": "5D411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 13,
      "matriz": 5,
      "idend": 1,
      "ala": "5D412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 14,
      "matriz": 5,
      "idend": 1,
      "ala": "5D413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 15,
      "matriz": 5,
      "idend": 1,
      "ala": "5D414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5D3
    {
      "coordx": 30,
      "coordy": 18,
      "matriz": 5,
      "idend": 140,
      "ala": "5D300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 19,
      "matriz": 5,
      "idend": 1,
      "ala": "5D301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 20,
      "matriz": 5,
      "idend": 1,
      "ala": "5D302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 21,
      "matriz": 5,
      "idend": 1,
      "ala": "5D303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 22,
      "matriz": 5,
      "idend": 1,
      "ala": "5D304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 23,
      "matriz": 5,
      "idend": 1,
      "ala": "5D305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 24,
      "matriz": 5,
      "idend": 1,
      "ala": "5D306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 25,
      "matriz": 5,
      "idend": 1,
      "ala": "5D307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 26,
      "matriz": 5,
      "idend": 1,
      "ala": "5D308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 27,
      "matriz": 5,
      "idend": 1,
      "ala": "5D309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 28,
      "matriz": 5,
      "idend": 1,
      "ala": "5D310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 29,
      "matriz": 5,
      "idend": 1,
      "ala": "5D311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 30,
      "matriz": 5,
      "idend": 1,
      "ala": "5D312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 31,
      "matriz": 5,
      "idend": 1,
      "ala": "5D313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 32,
      "matriz": 5,
      "idend": 1,
      "ala": "5D314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 33,
      "matriz": 5,
      "idend": 1,
      "ala": "5D315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 34,
      "matriz": 5,
      "idend": 1,
      "ala": "5D316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 35,
      "matriz": 5,
      "idend": 1,
      "ala": "5D317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 36,
      "matriz": 5,
      "idend": 1,
      "ala": "5D318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 37,
      "matriz": 5,
      "idend": 1,
      "ala": "5D319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 38,
      "matriz": 5,
      "idend": 1,
      "ala": "5D320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 39,
      "matriz": 5,
      "idend": 1,
      "ala": "5D321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 40,
      "matriz": 5,
      "idend": 1,
      "ala": "5D322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 41,
      "matriz": 5,
      "idend": 1,
      "ala": "5D323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 42,
      "matriz": 5,
      "idend": 1,
      "ala": "5D324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5D2
    {
      "coordx": 30,
      "coordy": 45,
      "matriz": 5,
      "idend": 150,
      "ala": "5D200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 46,
      "matriz": 5,
      "idend": 1,
      "ala": "5D201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 47,
      "matriz": 5,
      "idend": 1,
      "ala": "5D202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 48,
      "matriz": 5,
      "idend": 1,
      "ala": "5D203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 49,
      "matriz": 5,
      "idend": 1,
      "ala": "5D204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 50,
      "matriz": 5,
      "idend": 1,
      "ala": "5D205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 51,
      "matriz": 5,
      "idend": 1,
      "ala": "5D206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 52,
      "matriz": 5,
      "idend": 1,
      "ala": "5D207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 53,
      "matriz": 5,
      "idend": 1,
      "ala": "5D208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 54,
      "matriz": 5,
      "idend": 1,
      "ala": "5D209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 55,
      "matriz": 5,
      "idend": 1,
      "ala": "5D210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 56,
      "matriz": 5,
      "idend": 1,
      "ala": "5D211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 57,
      "matriz": 5,
      "idend": 1,
      "ala": "5D212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 58,
      "matriz": 5,
      "idend": 1,
      "ala": "5D213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 59,
      "matriz": 5,
      "idend": 1,
      "ala": "5D214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 60,
      "matriz": 5,
      "idend": 1,
      "ala": "5D215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 61,
      "matriz": 5,
      "idend": 1,
      "ala": "5D216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 62,
      "matriz": 5,
      "idend": 1,
      "ala": "5D217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 63,
      "matriz": 5,
      "idend": 1,
      "ala": "5D218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 64,
      "matriz": 5,
      "idend": 1,
      "ala": "5D219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 65,
      "matriz": 5,
      "idend": 1,
      "ala": "5D220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 66,
      "matriz": 5,
      "idend": 1,
      "ala": "5D221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 67,
      "matriz": 5,
      "idend": 1,
      "ala": "5D222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 68,
      "matriz": 5,
      "idend": 1,
      "ala": "5D223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 69,
      "matriz": 5,
      "idend": 1,
      "ala": "5D224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 70,
      "matriz": 5,
      "idend": 1,
      "ala": "5D225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 71,
      "matriz": 5,
      "idend": 1,
      "ala": "5D226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 72,
      "matriz": 5,
      "idend": 1,
      "ala": "5D227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 73,
      "matriz": 5,
      "idend": 1,
      "ala": "5D228",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 74,
      "matriz": 5,
      "idend": 1,
      "ala": "5D229",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 75,
      "matriz": 5,
      "idend": 1,
      "ala": "5D230",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 76,
      "matriz": 5,
      "idend": 1,
      "ala": "5D231",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5D1
    {
      "coordx": 30,
      "coordy": 79,
      "matriz": 5,
      "idend": 160,
      "ala": "5D100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 80,
      "matriz": 5,
      "idend": 1,
      "ala": "5D101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 81,
      "matriz": 5,
      "idend": 1,
      "ala": "5D102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 82,
      "matriz": 5,
      "idend": 1,
      "ala": "5D103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 83,
      "matriz": 5,
      "idend": 1,
      "ala": "5D104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 84,
      "matriz": 5,
      "idend": 1,
      "ala": "5D105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 85,
      "matriz": 5,
      "idend": 1,
      "ala": "5D106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 86,
      "matriz": 5,
      "idend": 1,
      "ala": "5D107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 87,
      "matriz": 5,
      "idend": 1,
      "ala": "5D108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 88,
      "matriz": 5,
      "idend": 1,
      "ala": "5D109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 89,
      "matriz": 5,
      "idend": 1,
      "ala": "5D110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 90,
      "matriz": 5,
      "idend": 1,
      "ala": "5D111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 91,
      "matriz": 5,
      "idend": 1,
      "ala": "5D112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 92,
      "matriz": 5,
      "idend": 1,
      "ala": "5D113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 93,
      "matriz": 5,
      "idend": 1,
      "ala": "5D114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 94,
      "matriz": 5,
      "idend": 1,
      "ala": "5D115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 95,
      "matriz": 5,
      "idend": 1,
      "ala": "5D116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 96,
      "matriz": 5,
      "idend": 1,
      "ala": "5D117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 97,
      "matriz": 5,
      "idend": 1,
      "ala": "5D118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 98,
      "matriz": 5,
      "idend": 1,
      "ala": "5D119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 99,
      "matriz": 5,
      "idend": 1,
      "ala": "5D120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 100,
      "matriz": 5,
      "idend": 1,
      "ala": "5D121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //FORÇAR MATRIZ 5
    {
      "coordx": 32,
      "coordy": 101,
      "matriz": 5,
      "idend": 300,
      "ala": "5D121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 16,
      "matriz": 5,
      "idend": 990,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 17,
      "matriz": 5,
      "idend": 991,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 43,
      "matriz": 5,
      "idend": 992,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 44,
      "matriz": 5,
      "idend": 993,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 77,
      "matriz": 5,
      "idend": 994,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 78,
      "matriz": 5,
      "idend": 995,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //MATRIZ 6
    //BLOCO 5C4
    {
      "coordx": 10,
      "coordy": 1,
      "matriz": 6,
      "idend": 1001,
      "ala": "5C400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 2,
      "matriz": 6,
      "idend": 2001,
      "ala": "5C401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 3,
      "matriz": 6,
      "idend": 3001,
      "ala": "5C402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 4,
      "matriz": 6,
      "idend": 40,
      "ala": "5C403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 5,
      "matriz": 6,
      "idend": 50,
      "ala": "5C404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 6,
      "matriz": 6,
      "idend": 60,
      "ala": "5C405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 7,
      "matriz": 6,
      "idend": 70,
      "ala": "5C406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 8,
      "matriz": 6,
      "idend": 70504,
      "ala": "5C407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 7,
      "coordy": 9,
      "matriz": 6,
      "idend": 1100,
      "ala": "5C408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 10,
      "matriz": 6,
      "idend": 1200,
      "ala": "5C409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 11,
      "matriz": 6,
      "idend": 100,
      "ala": "5C410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 12,
      "matriz": 6,
      "idend": 101,
      "ala": "5C411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 13,
      "matriz": 6,
      "idend": 102,
      "ala": "5C412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 14,
      "matriz": 6,
      "idend": 103,
      "ala": "5C413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 15,
      "matriz": 6,
      "idend": 1040,
      "ala": "5C414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5B4
    {
      "coordx": 24,
      "coordy": 1,
      "matriz": 6,
      "idend": 1050,
      "ala": "5B400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 3,
      "matriz": 6,
      "idend": 105,
      "ala": "5B402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 4,
      "matriz": 6,
      "idend": 1060,
      "ala": "5B403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 5,
      "matriz": 6,
      "idend": 1070,
      "ala": "5B404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 6,
      "matriz": 6,
      "idend": 10800,
      "ala": "5B405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 7,
      "matriz": 6,
      "idend": 109,
      "ala": "5B406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 8,
      "matriz": 6,
      "idend": 1100,
      "ala": "5B407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 9,
      "matriz": 6,
      "idend": 111,
      "ala": "5B408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 10,
      "matriz": 6,
      "idend": 112,
      "ala": "5B409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 11,
      "matriz": 6,
      "idend": 113,
      "ala": "5B410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 12,
      "matriz": 6,
      "idend": 114,
      "ala": "5B411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 13,
      "matriz": 6,
      "idend": 115,
      "ala": "5B412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 14,
      "matriz": 6,
      "idend": 116,
      "ala": "5B413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 15,
      "matriz": 6,
      "idend": 117,
      "ala": "5B414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5A4
    {
      "coordx": 26,
      "coordy": 1,
      "matriz": 6,
      "idend": 1180,
      "ala": "5A400",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 2,
      "matriz": 6,
      "idend": 1190,
      "ala": "5A401",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 3,
      "matriz": 6,
      "idend": 1200,
      "ala": "5A402",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 4,
      "matriz": 6,
      "idend": 1210,
      "ala": "5A403",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 5,
      "matriz": 6,
      "idend": 1220,
      "ala": "5A404",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 6,
      "matriz": 6,
      "idend": 1230,
      "ala": "5A405",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 7,
      "matriz": 6,
      "idend": 124,
      "ala": "5A406",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 8,
      "matriz": 6,
      "idend": 1250,
      "ala": "5A407",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 9,
      "matriz": 6,
      "idend": 126,
      "ala": "5A408",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 10,
      "matriz": 6,
      "idend": 127,
      "ala": "5A409",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 11,
      "matriz": 6,
      "idend": 1280,
      "ala": "5A410",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 12,
      "matriz": 6,
      "idend": 1290,
      "ala": "5A411",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 13,
      "matriz": 6,
      "idend": 1300,
      "ala": "5A412",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 14,
      "matriz": 6,
      "idend": 1310,
      "ala": "5A413",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 15,
      "matriz": 6,
      "idend": 1320,
      "ala": "5A414",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5C3
    {
      "coordx": 3,
      "coordy": 18,
      "matriz": 6,
      "idend": 30,
      "ala": "5C300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 19,
      "matriz": 6,
      "idend": 1,
      "ala": "5C301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 20,
      "matriz": 6,
      "idend": 1,
      "ala": "5C302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 21,
      "matriz": 6,
      "idend": 1,
      "ala": "5C303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 22,
      "matriz": 6,
      "idend": 1,
      "ala": "5C304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 23,
      "matriz": 6,
      "idend": 1,
      "ala": "5C305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 24,
      "matriz": 6,
      "idend": 1,
      "ala": "5C306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 25,
      "matriz": 6,
      "idend": 1,
      "ala": "5C307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 26,
      "matriz": 6,
      "idend": 1,
      "ala": "5C308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 27,
      "matriz": 6,
      "idend": 1,
      "ala": "5C309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 28,
      "matriz": 6,
      "idend": 1,
      "ala": "5C310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 29,
      "matriz": 6,
      "idend": 1,
      "ala": "5C311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 30,
      "matriz": 6,
      "idend": 1,
      "ala": "5C312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 31,
      "matriz": 6,
      "idend": 1,
      "ala": "5C313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 32,
      "matriz": 6,
      "idend": 1,
      "ala": "5C314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 33,
      "matriz": 6,
      "idend": 1,
      "ala": "5C315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 34,
      "matriz": 6,
      "idend": 1,
      "ala": "5C316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 35,
      "matriz": 6,
      "idend": 1,
      "ala": "5C317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 36,
      "matriz": 6,
      "idend": 1,
      "ala": "5C318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 37,
      "matriz": 6,
      "idend": 1,
      "ala": "5C319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 38,
      "matriz": 6,
      "idend": 1,
      "ala": "5C320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 39,
      "matriz": 6,
      "idend": 1,
      "ala": "5C321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 40,
      "matriz": 6,
      "idend": 1,
      "ala": "5C322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 41,
      "matriz": 6,
      "idend": 1,
      "ala": "5C323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 42,
      "matriz": 6,
      "idend": 1,
      "ala": "5C324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5B3
    {
      "coordx": 23,
      "coordy": 18,
      "matriz": 6,
      "idend": 3100,
      "ala": "5B300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 19,
      "matriz": 6,
      "idend": 1,
      "ala": "5B301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 20,
      "matriz": 6,
      "idend": 1,
      "ala": "5B302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 21,
      "matriz": 6,
      "idend": 1,
      "ala": "5B303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 22,
      "matriz": 6,
      "idend": 1,
      "ala": "5B304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 23,
      "matriz": 6,
      "idend": 1,
      "ala": "5B305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 24,
      "matriz": 6,
      "idend": 1,
      "ala": "5B306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 25,
      "matriz": 6,
      "idend": 1,
      "ala": "5B307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 26,
      "matriz": 6,
      "idend": 1,
      "ala": "5B308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 27,
      "matriz": 6,
      "idend": 1,
      "ala": "5B309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 28,
      "matriz": 6,
      "idend": 1,
      "ala": "5B310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 29,
      "matriz": 6,
      "idend": 1,
      "ala": "5B311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 30,
      "matriz": 6,
      "idend": 1,
      "ala": "5B312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 31,
      "matriz": 6,
      "idend": 1,
      "ala": "5B313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 32,
      "matriz": 6,
      "idend": 1,
      "ala": "5B314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 33,
      "matriz": 6,
      "idend": 1,
      "ala": "5B315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 34,
      "matriz": 6,
      "idend": 1,
      "ala": "5B316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 35,
      "matriz": 6,
      "idend": 1,
      "ala": "5B317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 36,
      "matriz": 6,
      "idend": 1,
      "ala": "5B318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 37,
      "matriz": 6,
      "idend": 1,
      "ala": "5B319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 38,
      "matriz": 6,
      "idend": 1,
      "ala": "5B320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 39,
      "matriz": 6,
      "idend": 1,
      "ala": "5B321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 40,
      "matriz": 6,
      "idend": 1,
      "ala": "5B322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 41,
      "matriz": 6,
      "idend": 1,
      "ala": "5B323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 42,
      "matriz": 6,
      "idend": 1,
      "ala": "5B324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5A3
    {
      "coordx": 27,
      "coordy": 18,
      "matriz": 6,
      "idend": 33,
      "ala": "5A300",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 19,
      "matriz": 6,
      "idend": 1,
      "ala": "5A301",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 20,
      "matriz": 6,
      "idend": 1,
      "ala": "5A302",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 21,
      "matriz": 6,
      "idend": 1,
      "ala": "5A303",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 22,
      "matriz": 6,
      "idend": 1,
      "ala": "5A304",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 23,
      "matriz": 6,
      "idend": 1,
      "ala": "5A305",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 24,
      "matriz": 6,
      "idend": 1,
      "ala": "5A306",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 25,
      "matriz": 6,
      "idend": 1,
      "ala": "5A307",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 26,
      "matriz": 6,
      "idend": 1,
      "ala": "5A308",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 27,
      "matriz": 6,
      "idend": 1,
      "ala": "5A309",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 28,
      "matriz": 6,
      "idend": 1,
      "ala": "5A310",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 29,
      "matriz": 6,
      "idend": 1,
      "ala": "5A311",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 30,
      "matriz": 6,
      "idend": 1,
      "ala": "5A312",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 31,
      "matriz": 6,
      "idend": 1,
      "ala": "5A313",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 32,
      "matriz": 6,
      "idend": 1,
      "ala": "5A314",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 33,
      "matriz": 6,
      "idend": 1,
      "ala": "5A315",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 34,
      "matriz": 6,
      "idend": 1,
      "ala": "5A316",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 35,
      "matriz": 6,
      "idend": 1,
      "ala": "5A317",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 36,
      "matriz": 6,
      "idend": 1,
      "ala": "5A318",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 37,
      "matriz": 6,
      "idend": 1,
      "ala": "5A319",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 38,
      "matriz": 6,
      "idend": 1,
      "ala": "5A320",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 39,
      "matriz": 6,
      "idend": 1,
      "ala": "5A321",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 40,
      "matriz": 6,
      "idend": 1,
      "ala": "5A322",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 41,
      "matriz": 6,
      "idend": 1,
      "ala": "5A323",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 42,
      "matriz": 6,
      "idend": 1,
      "ala": "5A324",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5C2
    {
      "coordx": 3,
      "coordy": 45,
      "matriz": 6,
      "idend": 300,
      "ala": "5C200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 46,
      "matriz": 6,
      "idend": 310,
      "ala": "5C201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 47,
      "matriz": 6,
      "idend": 310,
      "ala": "5C202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 48,
      "matriz": 6,
      "idend": 310,
      "ala": "5C203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 49,
      "matriz": 6,
      "idend": 310,
      "ala": "5C204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 50,
      "matriz": 6,
      "idend": 310,
      "ala": "5C205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 51,
      "matriz": 6,
      "idend": 310,
      "ala": "5C206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 52,
      "matriz": 6,
      "idend": 310,
      "ala": "5C207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 53,
      "matriz": 6,
      "idend": 310,
      "ala": "5C208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 54,
      "matriz": 6,
      "idend": 310,
      "ala": "5C209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 55,
      "matriz": 6,
      "idend": 310,
      "ala": "5C210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 56,
      "matriz": 6,
      "idend": 310,
      "ala": "5C211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 57,
      "matriz": 6,
      "idend": 310,
      "ala": "5C212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 58,
      "matriz": 6,
      "idend": 310,
      "ala": "5C213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 59,
      "matriz": 6,
      "idend": 310,
      "ala": "5C214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 60,
      "matriz": 6,
      "idend": 310,
      "ala": "5C215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 61,
      "matriz": 6,
      "idend": 310,
      "ala": "5C216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 62,
      "matriz": 6,
      "idend": 310,
      "ala": "5C217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 63,
      "matriz": 6,
      "idend": 310,
      "ala": "5C218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 64,
      "matriz": 6,
      "idend": 310,
      "ala": "5C219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 65,
      "matriz": 6,
      "idend": 310,
      "ala": "5C220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 66,
      "matriz": 6,
      "idend": 310,
      "ala": "5C221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 67,
      "matriz": 6,
      "idend": 310,
      "ala": "5C222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 68,
      "matriz": 6,
      "idend": 310,
      "ala": "5C223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 69,
      "matriz": 6,
      "idend": 310,
      "ala": "5C224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 70,
      "matriz": 6,
      "idend": 310,
      "ala": "5C225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 71,
      "matriz": 6,
      "idend": 310,
      "ala": "5C226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 72,
      "matriz": 6,
      "idend": 310,
      "ala": "5C227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 73,
      "matriz": 6,
      "idend": 310,
      "ala": "5C228",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 74,
      "matriz": 6,
      "idend": 310,
      "ala": "5C229",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 75,
      "matriz": 6,
      "idend": 310,
      "ala": "5C230",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 76,
      "matriz": 6,
      "idend": 310,
      "ala": "5C231",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5B2
    {
      "coordx": 23,
      "coordy": 45,
      "matriz": 6,
      "idend": 3010,
      "ala": "5B200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 46,
      "matriz": 6,
      "idend": 320,
      "ala": "5B201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 47,
      "matriz": 6,
      "idend": 320,
      "ala": "5B202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 48,
      "matriz": 6,
      "idend": 320,
      "ala": "5B203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 49,
      "matriz": 6,
      "idend": 320,
      "ala": "5B204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 50,
      "matriz": 6,
      "idend": 320,
      "ala": "5B205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 51,
      "matriz": 6,
      "idend": 320,
      "ala": "5B206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 52,
      "matriz": 6,
      "idend": 320,
      "ala": "5B207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 53,
      "matriz": 6,
      "idend": 320,
      "ala": "5B208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 54,
      "matriz": 6,
      "idend": 320,
      "ala": "5B209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 55,
      "matriz": 6,
      "idend": 320,
      "ala": "5B210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 56,
      "matriz": 6,
      "idend": 320,
      "ala": "5B211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 57,
      "matriz": 6,
      "idend": 320,
      "ala": "5B212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 58,
      "matriz": 6,
      "idend": 320,
      "ala": "5B213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 59,
      "matriz": 6,
      "idend": 320,
      "ala": "5B214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 60,
      "matriz": 6,
      "idend": 320,
      "ala": "5B215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 61,
      "matriz": 6,
      "idend": 320,
      "ala": "5B216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 62,
      "matriz": 6,
      "idend": 320,
      "ala": "5B217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 63,
      "matriz": 6,
      "idend": 320,
      "ala": "5B218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 64,
      "matriz": 6,
      "idend": 320,
      "ala": "5B219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 65,
      "matriz": 6,
      "idend": 320,
      "ala": "5B220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 66,
      "matriz": 6,
      "idend": 320,
      "ala": "5B221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 67,
      "matriz": 6,
      "idend": 320,
      "ala": "5B222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 68,
      "matriz": 6,
      "idend": 320,
      "ala": "5B223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 69,
      "matriz": 6,
      "idend": 320,
      "ala": "5B224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 70,
      "matriz": 6,
      "idend": 320,
      "ala": "5B225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 71,
      "matriz": 6,
      "idend": 320,
      "ala": "5B226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 72,
      "matriz": 6,
      "idend": 320,
      "ala": "5B227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 73,
      "matriz": 6,
      "idend": 320,
      "ala": "5B228",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 74,
      "matriz": 6,
      "idend": 320,
      "ala": "5B229",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 75,
      "matriz": 6,
      "idend": 320,
      "ala": "5B230",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 76,
      "matriz": 6,
      "idend": 320,
      "ala": "5B231",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5A2
    {
      "coordx": 27,
      "coordy": 45,
      "matriz": 6,
      "idend": 331,
      "ala": "5A200",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 46,
      "matriz": 6,
      "idend": 330,
      "ala": "5A201",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 47,
      "matriz": 6,
      "idend": 330,
      "ala": "5A202",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 48,
      "matriz": 6,
      "idend": 330,
      "ala": "5A203",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 49,
      "matriz": 6,
      "idend": 330,
      "ala": "5A204",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 50,
      "matriz": 6,
      "idend": 330,
      "ala": "5A205",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 51,
      "matriz": 6,
      "idend": 330,
      "ala": "5A206",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 52,
      "matriz": 6,
      "idend": 330,
      "ala": "5A207",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 53,
      "matriz": 6,
      "idend": 330,
      "ala": "5A208",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 54,
      "matriz": 6,
      "idend": 330,
      "ala": "5A209",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 55,
      "matriz": 6,
      "idend": 330,
      "ala": "5A210",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 56,
      "matriz": 6,
      "idend": 330,
      "ala": "5A211",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 57,
      "matriz": 6,
      "idend": 330,
      "ala": "5A212",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 58,
      "matriz": 6,
      "idend": 330,
      "ala": "5A213",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 59,
      "matriz": 6,
      "idend": 330,
      "ala": "5A214",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 60,
      "matriz": 6,
      "idend": 330,
      "ala": "5A215",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 61,
      "matriz": 6,
      "idend": 330,
      "ala": "5A216",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 62,
      "matriz": 6,
      "idend": 330,
      "ala": "5A217",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 63,
      "matriz": 6,
      "idend": 330,
      "ala": "5A218",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 64,
      "matriz": 6,
      "idend": 330,
      "ala": "5A219",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 65,
      "matriz": 6,
      "idend": 330,
      "ala": "5A220",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 66,
      "matriz": 6,
      "idend": 330,
      "ala": "5A221",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 67,
      "matriz": 6,
      "idend": 330,
      "ala": "5A222",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 68,
      "matriz": 6,
      "idend": 330,
      "ala": "5A223",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 69,
      "matriz": 6,
      "idend": 330,
      "ala": "5A224",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 70,
      "matriz": 6,
      "idend": 330,
      "ala": "5A225",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 71,
      "matriz": 6,
      "idend": 330,
      "ala": "5A226",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 72,
      "matriz": 6,
      "idend": 330,
      "ala": "5A227",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 73,
      "matriz": 6,
      "idend": 330,
      "ala": "5A228",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 74,
      "matriz": 6,
      "idend": 330,
      "ala": "5A229",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 75,
      "matriz": 6,
      "idend": 330,
      "ala": "5A230",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 76,
      "matriz": 6,
      "idend": 330,
      "ala": "5A231",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5C1
    {
      "coordx": 3,
      "coordy": 79,
      "matriz": 6,
      "idend": 341,
      "ala": "5C100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 80,
      "matriz": 6,
      "idend": 340,
      "ala": "5C101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 81,
      "matriz": 6,
      "idend": 340,
      "ala": "5C102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 82,
      "matriz": 6,
      "idend": 340,
      "ala": "5C103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 83,
      "matriz": 6,
      "idend": 340,
      "ala": "5C104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 84,
      "matriz": 6,
      "idend": 340,
      "ala": "5C105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 85,
      "matriz": 6,
      "idend": 340,
      "ala": "5C106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 86,
      "matriz": 6,
      "idend": 340,
      "ala": "5C107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 87,
      "matriz": 6,
      "idend": 340,
      "ala": "5C108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 88,
      "matriz": 6,
      "idend": 340,
      "ala": "5C109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 89,
      "matriz": 6,
      "idend": 340,
      "ala": "5C110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 90,
      "matriz": 6,
      "idend": 340,
      "ala": "5C111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 91,
      "matriz": 6,
      "idend": 340,
      "ala": "5C112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 92,
      "matriz": 6,
      "idend": 340,
      "ala": "5C113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 93,
      "matriz": 6,
      "idend": 340,
      "ala": "5C114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 94,
      "matriz": 6,
      "idend": 340,
      "ala": "5C115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 95,
      "matriz": 6,
      "idend": 340,
      "ala": "5C116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 96,
      "matriz": 6,
      "idend": 340,
      "ala": "5C117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 97,
      "matriz": 6,
      "idend": 340,
      "ala": "5C118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 98,
      "matriz": 6,
      "idend": 340,
      "ala": "5C119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 99,
      "matriz": 6,
      "idend": 340,
      "ala": "5C120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 100,
      "matriz": 6,
      "idend": 340,
      "ala": "5C121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5B1
    {
      "coordx": 23,
      "coordy": 79,
      "matriz": 6,
      "idend": 351,
      "ala": "5B100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 80,
      "matriz": 6,
      "idend": 350,
      "ala": "5B101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 81,
      "matriz": 6,
      "idend": 350,
      "ala": "5B102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 82,
      "matriz": 6,
      "idend": 350,
      "ala": "5B103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 83,
      "matriz": 6,
      "idend": 350,
      "ala": "5B104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 84,
      "matriz": 6,
      "idend": 350,
      "ala": "5B105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 85,
      "matriz": 6,
      "idend": 350,
      "ala": "5B106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 86,
      "matriz": 6,
      "idend": 350,
      "ala": "5B107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 87,
      "matriz": 6,
      "idend": 350,
      "ala": "5B108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 88,
      "matriz": 6,
      "idend": 350,
      "ala": "5B109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 89,
      "matriz": 6,
      "idend": 350,
      "ala": "5B110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 90,
      "matriz": 6,
      "idend": 350,
      "ala": "5B111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 91,
      "matriz": 6,
      "idend": 350,
      "ala": "5B112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 92,
      "matriz": 6,
      "idend": 350,
      "ala": "5B113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 93,
      "matriz": 6,
      "idend": 350,
      "ala": "5B114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 94,
      "matriz": 6,
      "idend": 350,
      "ala": "5B115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 95,
      "matriz": 6,
      "idend": 350,
      "ala": "5B116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 96,
      "matriz": 6,
      "idend": 350,
      "ala": "5B117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 97,
      "matriz": 6,
      "idend": 350,
      "ala": "5B118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 98,
      "matriz": 6,
      "idend": 350,
      "ala": "5B119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 99,
      "matriz": 6,
      "idend": 350,
      "ala": "5B120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 100,
      "matriz": 6,
      "idend": 350,
      "ala": "5B121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //BLOCO 5A1
    {
      "coordx": 27,
      "coordy": 78,
      "matriz": 6,
      "idend": 361,
      "ala": "5A100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 27,
      "coordy": 79,
      "matriz": 6,
      "idend": 362,
      "ala": "5A101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 80,
      "matriz": 6,
      "idend": 360,
      "ala": "5A102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 81,
      "matriz": 6,
      "idend": 360,
      "ala": "5A103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 82,
      "matriz": 6,
      "idend": 360,
      "ala": "5A104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 83,
      "matriz": 6,
      "idend": 360,
      "ala": "5A105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 84,
      "matriz": 6,
      "idend": 360,
      "ala": "5A106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 85,
      "matriz": 6,
      "idend": 360,
      "ala": "5A107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 86,
      "matriz": 6,
      "idend": 360,
      "ala": "5A108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 87,
      "matriz": 6,
      "idend": 360,
      "ala": "5A109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 88,
      "matriz": 6,
      "idend": 360,
      "ala": "5A110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 89,
      "matriz": 6,
      "idend": 360,
      "ala": "5A111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 90,
      "matriz": 6,
      "idend": 360,
      "ala": "5A112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 91,
      "matriz": 6,
      "idend": 360,
      "ala": "5A113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 92,
      "matriz": 6,
      "idend": 360,
      "ala": "5A114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 93,
      "matriz": 6,
      "idend": 360,
      "ala": "5A115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 94,
      "matriz": 6,
      "idend": 360,
      "ala": "5A116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 95,
      "matriz": 6,
      "idend": 360,
      "ala": "5A117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 96,
      "matriz": 6,
      "idend": 360,
      "ala": "5A118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 97,
      "matriz": 6,
      "idend": 360,
      "ala": "5A119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 98,
      "matriz": 6,
      "idend": 360,
      "ala": "5A120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 99,
      "matriz": 6,
      "idend": 360,
      "ala": "5A121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 100,
      "matriz": 6,
      "idend": 360,
      "ala": "5A122",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //FORÇAR MATRIZ 6
    {
      "coordx": 39,
      "coordy": 16,
      "matriz": 6,
      "idend": 500,
      "ala": "6G420",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 101,
      "matriz": 6,
      "idend": 500,
      "ala": "6G420",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    // {
    //     "coordx": 3,
    //     "coordy": 16,
    //     "matriz": 6,
    //     "idend": 961,
    //     "ala": "5C300",
    //     "bloco": "",
    //     "status": 10,
    //     "altura": 0
    // },
    // {
    //     "coordx": 3,
    //     "coordy": 17,
    //     "matriz": 6,
    //     "idend": 960,
    //     "ala": "5C300",
    //     "bloco": "",
    //     "status": 10,
    //     "altura": 0
    // },
    // {
    //     "coordx": 3,
    //     "coordy": 43,
    //     "matriz": 6,
    //     "idend": 962,
    //     "ala": "5C300",
    //     "bloco": "",
    //     "status": 10,
    //     "altura": 0
    // },
    // {
    //     "coordx": 3,
    //     "coordy": 44,
    //     "matriz": 6,
    //     "idend": 963,
    //     "ala": "5C300",
    //     "bloco": "",
    //     "status": 10,
    //     "altura": 0
    // },
    // {
    //     "coordx": 3,
    //     "coordy": 77,
    //     "matriz": 6,
    //     "idend": 964,
    //     "ala": "5C300",
    //     "bloco": "",
    //     "status": 10,
    //     "altura": 0
    // }

    //COLUNAS
    //M1
    {
      "coordx": 13,
      "coordy": 15,
      "matriz": 1,
      "idend": 2,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 15,
      "matriz": 1,
      "idend": 2,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 16,
      "matriz": 1,
      "idend": 3,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 16,
      "matriz": 1,
      "idend": 3,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 34,
      "matriz": 1,
      "idend": 5,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 34,
      "matriz": 1,
      "idend": 5,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 34,
      "matriz": 1,
      "idend": 5,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    //M2
    {
      "coordx": 19,
      "coordy": 11,
      "matriz": 2,
      "idend": 6,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 40,
      "coordy": 11,
      "matriz": 2,
      "idend": 78,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    //M3
    {
      "coordx": 2,
      "coordy": 8,
      "matriz": 3,
      "idend": 2,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 8,
      "matriz": 3,
      "idend": 3,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 8,
      "matriz": 3,
      "idend": 4,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 9,
      "matriz": 3,
      "idend": 5,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 9,
      "matriz": 3,
      "idend": 6,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 9,
      "matriz": 3,
      "idend": 7,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 27,
      "matriz": 3,
      "idend": 9,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 27,
      "matriz": 3,
      "idend": 10,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 27,
      "matriz": 3,
      "idend": 11,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 20,
      "coordy": 7,
      "matriz": 3,
      "idend": 20,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 21,
      "coordy": 7,
      "matriz": 3,
      "idend": 22,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 20,
      "coordy": 8,
      "matriz": 3,
      "idend": 23,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 21,
      "coordy": 8,
      "matriz": 3,
      "idend": 24,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 20,
      "coordy": 27,
      "matriz": 3,
      "idend": 31,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 21,
      "coordy": 27,
      "matriz": 3,
      "idend": 32,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 8,
      "matriz": 3,
      "idend": 232,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 37,
      "coordy": 27,
      "matriz": 3,
      "idend": 129,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 38,
      "coordy": 27,
      "matriz": 3,
      "idend": 130,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 55,
      "coordy": 8,
      "matriz": 3,
      "idend": 131,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 55,
      "coordy": 9,
      "matriz": 3,
      "idend": 132,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 54,
      "coordy": 27,
      "matriz": 3,
      "idend": 133,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 55,
      "coordy": 27,
      "matriz": 3,
      "idend": 134,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    //M4
    {
      "coordx": 10,
      "coordy": 7,
      "matriz": 4,
      "idend": 5,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 8,
      "matriz": 4,
      "idend": 6,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 14,
      "matriz": 4,
      "idend": 7,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 19,
      "matriz": 4,
      "idend": 80,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 20,
      "matriz": 4,
      "idend": 8,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 21,
      "matriz": 4,
      "idend": 10,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 27,
      "matriz": 4,
      "idend": 11,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 28,
      "matriz": 4,
      "idend": 12,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 34,
      "matriz": 4,
      "idend": 13,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 41,
      "matriz": 4,
      "idend": 14,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 47,
      "matriz": 4,
      "idend": 15,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 48,
      "matriz": 4,
      "idend": 16,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 54,
      "matriz": 4,
      "idend": 17,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 55,
      "matriz": 4,
      "idend": 18,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 61,
      "matriz": 4,
      "idend": 19,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 68,
      "matriz": 4,
      "idend": 20,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 74,
      "matriz": 4,
      "idend": 21,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 75,
      "matriz": 4,
      "idend": 22,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 81,
      "matriz": 4,
      "idend": 23,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 88,
      "matriz": 4,
      "idend": 24,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 94,
      "matriz": 4,
      "idend": 2401,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 95,
      "matriz": 4,
      "idend": 2402,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 101,
      "matriz": 4,
      "idend": 240,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 102,
      "matriz": 4,
      "idend": 241,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    //M5
    {
      "coordx": 23,
      "coordy": 7,
      "matriz": 5,
      "idend": 100,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 13,
      "matriz": 5,
      "idend": 101,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 14,
      "matriz": 5,
      "idend": 102,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 21,
      "matriz": 5,
      "idend": 104,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 27,
      "matriz": 5,
      "idend": 110,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 33,
      "matriz": 5,
      "idend": 106,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 40,
      "matriz": 5,
      "idend": 107,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 41,
      "matriz": 5,
      "idend": 108,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 42,
      "matriz": 5,
      "idend": 1080,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 47,
      "matriz": 5,
      "idend": 120,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 48,
      "matriz": 5,
      "idend": 121,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 54,
      "matriz": 5,
      "idend": 122,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 61,
      "matriz": 5,
      "idend": 123,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 67,
      "matriz": 5,
      "idend": 125,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 68,
      "matriz": 5,
      "idend": 128,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 74,
      "matriz": 5,
      "idend": 129,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 80,
      "matriz": 5,
      "idend": 300,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 81,
      "matriz": 5,
      "idend": 301,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 88,
      "matriz": 5,
      "idend": 302,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 94,
      "matriz": 5,
      "idend": 304,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 95,
      "matriz": 5,
      "idend": 305,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    //M6
    {
      "coordx": 16,
      "coordy": 7,
      "matriz": 6,
      "idend": 20,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 13,
      "matriz": 6,
      "idend": 21,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 14,
      "matriz": 6,
      "idend": 22,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 15,
      "matriz": 6,
      "idend": 22,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 20,
      "matriz": 6,
      "idend": 200,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 21,
      "matriz": 6,
      "idend": 201,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 34,
      "matriz": 6,
      "idend": 203,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 40,
      "matriz": 6,
      "idend": 204,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 41,
      "matriz": 6,
      "idend": 205,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 42,
      "matriz": 6,
      "idend": 2050,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 27,
      "matriz": 6,
      "idend": 202,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 47,
      "matriz": 6,
      "idend": 321,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 48,
      "matriz": 6,
      "idend": 323,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 54,
      "matriz": 6,
      "idend": 324,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 61,
      "matriz": 6,
      "idend": 324,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 67,
      "matriz": 6,
      "idend": 325,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 68,
      "matriz": 6,
      "idend": 326,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 74,
      "matriz": 6,
      "idend": 327,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 75,
      "matriz": 6,
      "idend": 32700,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 76,
      "matriz": 6,
      "idend": 3270,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 79,
      "matriz": 6,
      "idend": 3520,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 80,
      "matriz": 6,
      "idend": 352,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 81,
      "matriz": 6,
      "idend": 353,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 88,
      "matriz": 6,
      "idend": 354,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 94,
      "matriz": 6,
      "idend": 355,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 95,
      "matriz": 6,
      "idend": 356,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },

  ];
  constructor() {

  }

}