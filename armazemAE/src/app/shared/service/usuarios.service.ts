import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	dadosUser: any = null;


	constructor(private HttpClient: HttpClient, private _route: Router,) { }



	//trazer usuarios
	getUser(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.HttpClient.get(`${environment.apiEndereco}/auth/usuarios`).subscribe((response: any) => {
				// this.Arraydata = response;
				// this.onDataChanged.next(this.Arraydata);                
				resolve(response);
			}, reject);
		});
	}

	postLogin(login: any, senha: any): Promise<any> {
		return new Promise((resolve, reject) => {
			this.HttpClient.post(`${environment.apiEndereco}/auth/login?usuario=${login}&senha=${senha}`, null).subscribe((response: any) => {
				// this.Arraydata = response;
				// this.onDataChanged.next(this.Arraydata);    
				localStorage.setItem('currentUser', JSON.stringify(response.model));
				this.dadosUser = response.model;
				resolve(response);
			}, reject);
		});
	}

	Logout() {
		this.dadosUser = null;
		localStorage.removeItem('currentUser');
		this._route.navigate(['/login']);
	}

	isAuth() {
		if (JSON.parse(localStorage.getItem('currentUser') as any) == null) {
			return false;
		} else {
			this.dadosUser = JSON.parse(localStorage.getItem('currentUser') as any);
			return true;
		}
	}


}