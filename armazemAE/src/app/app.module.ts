import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BagsService } from './shared/service/bags.service';
import { MapaService } from './shared/service/enderecos.service';
import { ExcelService } from './shared/service/excelService.service';
import { ArmazemComponent } from './views/armazem/armazem.component';
import { LoginComponent } from './views/login/login.component';
import { ModalComponent } from './views/modal-ala/modal.component';
import { ModalCaixaLigComponent } from './views/modal-caixa-lig/modal-caixa-lig.component';
import { ModalCaixaRebComponent } from './views/modal-caixa-reb/modal-caixa-reb.component';
import { ModalCaixaComponent } from './views/modal-caixa-rec/modal-caixa.component';
import { ModalDetalhesComponent } from './views/modal-detalhes/modal-detalhes.component';



@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    ModalCaixaComponent,
    ModalCaixaLigComponent,
    ModalCaixaRebComponent,
    ModalDetalhesComponent,
    LoginComponent,
    ArmazemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NoopAnimationsModule,
    FlexLayoutModule,
    MatIconModule,
    HttpClientModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatMenuModule,
    BrowserAnimationsModule,
  ],
  providers: [MapaService, BagsService, ExcelService],

  bootstrap: [AppComponent]
})
export class AppModule {
}