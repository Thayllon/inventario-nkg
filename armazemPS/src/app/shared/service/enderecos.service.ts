import { Mapa } from "../model/mapa.model";

export class MapaService {

  data: Mapa[] = [
    //Matriz 3
    {
      "coordx": 1,
      "coordy": 1,
      "matriz": 3,
      "idend": 100,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //BLOCOS AMARELOS   
    //Matriz 1
    {
      "coordx": 7,
      "coordy": 1,
      "matriz": 1,
      "idend": 100,
      "ala": "1B19",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 2,
      "matriz": 1,
      "idend": 101,
      "ala": "1B18",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 3,
      "matriz": 1,
      "idend": 102,
      "ala": "1B17",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 4,
      "matriz": 1,
      "idend": 103,
      "ala": "1B16",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 5,
      "matriz": 1,
      "idend": 104,
      "ala": "1B15",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 6,
      "matriz": 1,
      "idend": 105,
      "ala": "1B14",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 7,
      "matriz": 1,
      "idend": 106,
      "ala": "1B13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 8,
      "matriz": 1,
      "idend": 107,
      "ala": "1B12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 9,
      "matriz": 1,
      "idend": 108,
      "ala": "1B11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 10,
      "matriz": 1,
      "idend": 109,
      "ala": "1B10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 11,
      "matriz": 1,
      "idend": 110,
      "ala": "1B9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 12,
      "matriz": 1,
      "idend": 111,
      "ala": "1B8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 13,
      "matriz": 1,
      "idend": 112,
      "ala": "1B7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 7,
      "coordy": 14,
      "matriz": 1,
      "idend": 113,
      "ala": "1I14",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 6,
      "coordy": 15,
      "matriz": 1,
      "idend": 114,
      "ala": "1I13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 16,
      "matriz": 1,
      "idend": 115,
      "ala": "1I12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 17,
      "matriz": 1,
      "idend": 300,
      "ala": "1B6",
      "bloco": "",
      "status": 2,
      "altura": 0
    }, {
      "coordx": 5,
      "coordy": 18,
      "matriz": 1,
      "idend": 301,
      "ala": "1B5",
      "bloco": "",
      "status": 2,
      "altura": 0
    }, {
      "coordx": 5,
      "coordy": 19,
      "matriz": 1,
      "idend": 302,
      "ala": "1B4",
      "bloco": "",
      "status": 2,
      "altura": 0
    }, {
      "coordx": 5,
      "coordy": 20,
      "matriz": 1,
      "idend": 303,
      "ala": "1B3",
      "bloco": "",
      "status": 2,
      "altura": 0
    }, {
      "coordx": 5,
      "coordy": 21,
      "matriz": 1,
      "idend": 304,
      "ala": "1B2",
      "bloco": "",
      "status": 2,
      "altura": 0
    }, {
      "coordx": 5,
      "coordy": 22,
      "matriz": 1,
      "idend": 305,
      "ala": "1B1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 7,
      "matriz": 1,
      "idend": 116,
      "ala": "1I15",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 7,
      "matriz": 1,
      "idend": 117,
      "ala": "1C1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 7,
      "matriz": 1,
      "idend": 118,
      "ala": "1C2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 7,
      "matriz": 1,
      "idend": 119,
      "ala": "1C3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 7,
      "matriz": 1,
      "idend": 120,
      "ala": "1C4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 7,
      "matriz": 1,
      "idend": 121,
      "ala": "1C5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 7,
      "matriz": 1,
      "idend": 122,
      "ala": "1C6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 7,
      "matriz": 1,
      "idend": 123,
      "ala": "1C7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 18,
      "coordy": 7,
      "matriz": 1,
      "idend": 124,
      "ala": "1C8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 7,
      "matriz": 1,
      "idend": 125,
      "ala": "1C9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 20,
      "coordy": 7,
      "matriz": 1,
      "idend": 126,
      "ala": "1C10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 21,
      "coordy": 7,
      "matriz": 1,
      "idend": 127,
      "ala": "1I16",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 27,
      "coordy": 1,
      "matriz": 1,
      "idend": 128,
      "ala": "1D13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 2,
      "matriz": 1,
      "idend": 129,
      "ala": "1D12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 3,
      "matriz": 1,
      "idend": 130,
      "ala": "1D11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 4,
      "matriz": 1,
      "idend": 131,
      "ala": "1D10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 5,
      "matriz": 1,
      "idend": 132,
      "ala": "1D9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 6,
      "matriz": 1,
      "idend": 133,
      "ala": "1D8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 7,
      "matriz": 1,
      "idend": 134,
      "ala": "1D7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 8,
      "matriz": 1,
      "idend": 135,
      "ala": "1D6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 9,
      "matriz": 1,
      "idend": 136,
      "ala": "1D5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 10,
      "matriz": 1,
      "idend": 137,
      "ala": "1D4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 11,
      "matriz": 1,
      "idend": 138,
      "ala": "1D3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 12,
      "matriz": 1,
      "idend": 139,
      "ala": "1D2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 13,
      "matriz": 1,
      "idend": 140,
      "ala": "1D1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 18,
      "coordy": 9,
      "matriz": 1,
      "idend": 141,
      "ala": "1E3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 9,
      "matriz": 1,
      "idend": 142,
      "ala": "1E2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 20,
      "coordy": 9,
      "matriz": 1,
      "idend": 143,
      "ala": "1E1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 17,
      "matriz": 1,
      "idend": 144,
      "ala": "1A22",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 17,
      "matriz": 1,
      "idend": 145,
      "ala": "1A21",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 17,
      "matriz": 1,
      "idend": 146,
      "ala": "1A20",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 17,
      "matriz": 1,
      "idend": 147,
      "ala": "1A19",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 17,
      "matriz": 1,
      "idend": 148,
      "ala": "1A18",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 17,
      "matriz": 1,
      "idend": 149,
      "ala": "1A17",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 17,
      "matriz": 1,
      "idend": 150,
      "ala": "1A16",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 18,
      "coordy": 17,
      "matriz": 1,
      "idend": 151,
      "ala": "1A15",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 17,
      "matriz": 1,
      "idend": 152,
      "ala": "1A14",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 20,
      "coordy": 17,
      "matriz": 1,
      "idend": 153,
      "ala": "1A13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 21,
      "coordy": 17,
      "matriz": 1,
      "idend": 154,
      "ala": "1A12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 22,
      "coordy": 17,
      "matriz": 1,
      "idend": 155,
      "ala": "1A11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 17,
      "matriz": 1,
      "idend": 156,
      "ala": "1A10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 17,
      "matriz": 1,
      "idend": 157,
      "ala": "1A9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 25,
      "coordy": 17,
      "matriz": 1,
      "idend": 158,
      "ala": "1A8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 17,
      "matriz": 1,
      "idend": 159,
      "ala": "1A7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 27,
      "coordy": 17,
      "matriz": 1,
      "idend": 160,
      "ala": "1A6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 28,
      "coordy": 17,
      "matriz": 1,
      "idend": 161,
      "ala": "1A5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 29,
      "coordy": 17,
      "matriz": 1,
      "idend": 162,
      "ala": "1A4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 30,
      "coordy": 17,
      "matriz": 1,
      "idend": 163,
      "ala": "1A3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 17,
      "matriz": 1,
      "idend": 164,
      "ala": "1A2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 32,
      "coordy": 17,
      "matriz": 1,
      "idend": 165,
      "ala": "1A1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 23,
      "matriz": 1,
      "idend": 166,
      "ala": "1I11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 24,
      "matriz": 1,
      "idend": 167,
      "ala": "1I10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 25,
      "matriz": 1,
      "idend": 168,
      "ala": "1I9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 26,
      "matriz": 1,
      "idend": 169,
      "ala": "1I8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 27,
      "matriz": 1,
      "idend": 170,
      "ala": "1I7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 28,
      "matriz": 1,
      "idend": 171,
      "ala": "1I6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 29,
      "matriz": 1,
      "idend": 172,
      "ala": "1I5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 1,
      "coordy": 29,
      "matriz": 1,
      "idend": 173,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    //Forçar Matriz 1
    {
      "coordx": 1,
      "coordy": 30,
      "matriz": 1,
      "idend": 1000,
      "ala": "1I5",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 16,
      "matriz": 1,
      "idend": 1001,
      "ala": "1I5",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //Matriz 2
    {
      "coordx": 5,
      "coordy": 1,
      "matriz": 2,
      "idend": 174,
      "ala": "2H6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 2,
      "matriz": 2,
      "idend": 175,
      "ala": "2H5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 3,
      "matriz": 2,
      "idend": 176,
      "ala": "2H4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 4,
      "matriz": 2,
      "idend": 177,
      "ala": "2H3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 5,
      "matriz": 2,
      "idend": 178,
      "ala": "2H2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 6,
      "matriz": 2,
      "idend": 179,
      "ala": "2H1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 8,
      "matriz": 2,
      "idend": 180,
      "ala": "2G7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 8,
      "matriz": 2,
      "idend": 181,
      "ala": "2G6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 8,
      "matriz": 2,
      "idend": 182,
      "ala": "2G5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 8,
      "matriz": 2,
      "idend": 183,
      "ala": "2G4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 8,
      "matriz": 2,
      "idend": 184,
      "ala": "2G3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 8,
      "matriz": 2,
      "idend": 185,
      "ala": "2G2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 8,
      "matriz": 2,
      "idend": 186,
      "ala": "2G1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 21,
      "coordy": 8,
      "matriz": 2,
      "idend": 187,
      "ala": "2F5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 22,
      "coordy": 8,
      "matriz": 2,
      "idend": 188,
      "ala": "2F4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 8,
      "matriz": 2,
      "idend": 189,
      "ala": "2F3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 24,
      "coordy": 8,
      "matriz": 2,
      "idend": 190,
      "ala": "2F2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 25,
      "coordy": 8,
      "matriz": 2,
      "idend": 191,
      "ala": "2F1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 29,
      "coordy": 8,
      "matriz": 2,
      "idend": 192,
      "ala": "2E5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 30,
      "coordy": 8,
      "matriz": 2,
      "idend": 193,
      "ala": "2E4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 31,
      "coordy": 8,
      "matriz": 2,
      "idend": 194,
      "ala": "2E3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 32,
      "coordy": 8,
      "matriz": 2,
      "idend": 195,
      "ala": "2E2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 33,
      "coordy": 8,
      "matriz": 2,
      "idend": 196,
      "ala": "2E1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 8,
      "matriz": 2,
      "idend": 197,
      "ala": "1I4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 6,
      "coordy": 9,
      "matriz": 2,
      "idend": 198,
      "ala": "1I3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 7,
      "coordy": 10,
      "matriz": 2,
      "idend": 199,
      "ala": "1I2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 11,
      "matriz": 2,
      "idend": 200,
      "ala": "2D13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 12,
      "matriz": 2,
      "idend": 201,
      "ala": "2D12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 13,
      "matriz": 2,
      "idend": 202,
      "ala": "2D11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 14,
      "matriz": 2,
      "idend": 203,
      "ala": "2D10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 15,
      "matriz": 2,
      "idend": 204,
      "ala": "2D9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 16,
      "matriz": 2,
      "idend": 205,
      "ala": "2D8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 17,
      "matriz": 2,
      "idend": 206,
      "ala": "2D7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 18,
      "matriz": 2,
      "idend": 207,
      "ala": "2D6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 19,
      "matriz": 2,
      "idend": 208,
      "ala": "2D5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 20,
      "matriz": 2,
      "idend": 209,
      "ala": "2D4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 21,
      "matriz": 2,
      "idend": 210,
      "ala": "2D3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 22,
      "matriz": 2,
      "idend": 211,
      "ala": "2D2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 23,
      "matriz": 2,
      "idend": 212,
      "ala": "2D1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 11,
      "matriz": 2,
      "idend": 213,
      "ala": "2C13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 12,
      "matriz": 2,
      "idend": 214,
      "ala": "2C12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 13,
      "matriz": 2,
      "idend": 215,
      "ala": "2C11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 14,
      "matriz": 2,
      "idend": 216,
      "ala": "2C10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 15,
      "matriz": 2,
      "idend": 217,
      "ala": "2C9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 16,
      "matriz": 2,
      "idend": 218,
      "ala": "2C8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 17,
      "matriz": 2,
      "idend": 219,
      "ala": "2C7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 18,
      "matriz": 2,
      "idend": 220,
      "ala": "2C6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 19,
      "matriz": 2,
      "idend": 221,
      "ala": "2C5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 20,
      "matriz": 2,
      "idend": 222,
      "ala": "2C4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 21,
      "matriz": 2,
      "idend": 224,
      "ala": "2C3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 22,
      "matriz": 2,
      "idend": 225,
      "ala": "2C2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 23,
      "matriz": 2,
      "idend": 226,
      "ala": "2C1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 11,
      "matriz": 2,
      "idend": 227,
      "ala": "2B13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 12,
      "matriz": 2,
      "idend": 228,
      "ala": "2B12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 13,
      "matriz": 2,
      "idend": 229,
      "ala": "2B11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 14,
      "matriz": 2,
      "idend": 230,
      "ala": "2B10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 15,
      "matriz": 2,
      "idend": 231,
      "ala": "2B9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 16,
      "matriz": 2,
      "idend": 232,
      "ala": "2B8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 17,
      "matriz": 2,
      "idend": 233,
      "ala": "2B7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 18,
      "matriz": 2,
      "idend": 234,
      "ala": "2B6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 19,
      "matriz": 2,
      "idend": 235,
      "ala": "2B5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 20,
      "matriz": 2,
      "idend": 236,
      "ala": "2B4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 21,
      "matriz": 2,
      "idend": 237,
      "ala": "2B3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 22,
      "matriz": 2,
      "idend": 238,
      "ala": "2B2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 23,
      "coordy": 23,
      "matriz": 2,
      "idend": 239,
      "ala": "2B1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 10,
      "matriz": 2,
      "idend": 240,
      "ala": "1I1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 11,
      "matriz": 2,
      "idend": 241,
      "ala": "2A13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 12,
      "matriz": 2,
      "idend": 242,
      "ala": "2A12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 13,
      "matriz": 2,
      "idend": 243,
      "ala": "2A11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 14,
      "matriz": 2,
      "idend": 244,
      "ala": "2A10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 15,
      "matriz": 2,
      "idend": 245,
      "ala": "2A9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 16,
      "matriz": 2,
      "idend": 246,
      "ala": "2A8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 17,
      "matriz": 2,
      "idend": 247,
      "ala": "2A7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 18,
      "matriz": 2,
      "idend": 248,
      "ala": "2A6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 19,
      "matriz": 2,
      "idend": 249,
      "ala": "2A5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 20,
      "matriz": 2,
      "idend": 250,
      "ala": "2A4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 21,
      "matriz": 2,
      "idend": 251,
      "ala": "2A3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 22,
      "matriz": 2,
      "idend": 252,
      "ala": "2A2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 26,
      "coordy": 23,
      "matriz": 2,
      "idend": 253,
      "ala": "2A1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //Forçar Matriz 2
    {
      "coordx": 8,
      "coordy": 24,
      "matriz": 2,
      "idend": 1000,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 34,
      "coordy": 24,
      "matriz": 2,
      "idend": 1001,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
  ];
  constructor() {

  }

}