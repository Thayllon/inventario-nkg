export class Mapa {

    coordy: number; 
    coordx: number; 
    idend: number;
    tag?:string;
    ala: string;
    matriz: number;
    bloco: string; 
    status: number;
    altura?:number;

    constructor(value?: any){
        value = value || {};        
        this.coordx = value.coordx || 0;
        this.coordy = value.coordy || 0;
        this.coordx = value.coordx || 0;
        this.idend = value.idend || 0;
        this.ala = value.ala || '';
        this.matriz = value.matriz || 0;
        this.bloco = value.bloco || '';
        this.status = value.status || 0;
        this.altura = value.altura || 0;
        this.tag = value.tag || '';
    }
}