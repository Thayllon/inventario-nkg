import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { interval, Subject } from 'rxjs';
import { takeUntil } from "rxjs/internal/operators";
import { Mapa } from 'src/app/shared/model/mapa.model';
import { ApiService } from 'src/app/shared/service/api.service';
import { BagsService } from 'src/app/shared/service/bags.service';
import { MapaService } from 'src/app/shared/service/enderecos.service';
import { ExcelService } from 'src/app/shared/service/excelService.service';
import { UserService } from 'src/app/shared/service/usuarios.service';
import { ModalComponent } from '../modal-ala/modal.component';


@Component({
  selector: 'armazem',
  templateUrl: './armazem.component.html',
  styleUrls: ['./armazem.component.css']
})
export class ArmazemComponent {
  blocos: Mapa[] = [];
  blocosAll: Mapa[] = [];
  quadra: any;
  dialogRef: any;
  lote: string = '';
  nome: string = '';

  source = interval(3000);
  private _unsubscribeAll: Subject<any>;

  constructor(private _mapaService: MapaService, private _bagsService: BagsService, private _apiService: ApiService,
    private _matDialog: MatDialog, private excelService: ExcelService, private _route: Router, public _apiServiceUser: UserService,) {

    if (this._apiServiceUser.isAuth() == false) {
      this._route.navigate(['/login']);
    }

    this._unsubscribeAll = new Subject();

    this.source.pipe(takeUntil(this._unsubscribeAll)).subscribe(x => {

      this._apiService.getAll().then((res: any[]) => {
        let enderecosModificados = res.filter((x: any) => {
          const statusEndOld: any = this.blocosAll.find(z => z.idend == x.idend);
          if (statusEndOld.altura != x.altura || statusEndOld.status != x.status) {
            return x;
          }
        });

        if (enderecosModificados.length > 0) {

          for (let item of (enderecosModificados as any[])) {

            let td = document.getElementById(item.idend.toString());

            if (td != null) {

              td.setAttribute('description', item.altura.toString());

              if (item.status == 20) {
                td.style.backgroundColor = 'blue';
              }
              // else if (td.className.includes('Certifi')) {
              //   td.style.backgroundColor = 'brown';
              //}
              else {
                td.style.backgroundColor = 'green';
              }

              this.blocosAll[this.blocosAll.findIndex(x => x.idend == item.idend)] = item;
            }
          }
        }
      });
    });
  }

  ngOnInit() {
    this._apiService.getAll().then(res => {
      this.blocos = [...res, ...JSON.parse(JSON.stringify(this._mapaService.data))];
      this.blocosAll = res;
      this.CriaMapa();
      this.ReloadCss();
    });

    this.Zoom(document.querySelector('.matrizes'));
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  ContaX(matriz: number) {
    try {
      this.quadra = this.blocos.filter(e => e.matriz == matriz);
      return this.quadra.reduce(function (prev: { coordx: number; }, current: { coordx: number; }) {
        return (prev.coordx > current.coordx) ? prev : current
      }).coordx
    } catch (error) {
      console.log(error);
      return 0;
    }
  }

  ContaY(matriz: number) {
    try {
      this.quadra = this.blocos.filter(e => e.matriz == matriz);
      return this.quadra.reduce(function (prev: { coordy: number; }, current: { coordy: number; }) {
        return (prev.coordy > current.coordy) ? prev : current
      }).coordy
    } catch (error) {
      console.log(error);
      return 0;
    }
  }

  ContaMatriz() {
    try {
      var newArr = [];
      for (var i = 1; i < this.blocos.length; i++) {
        if (newArr.indexOf(Number(this.blocos[i].matriz)) === -1) {
          newArr.push(Number(this.blocos[i].matriz));
        }
      }
      return newArr;
    } catch (error) {
      console.log(error);
      return [];
    }
  }

  CriaMapa() {
    const matrizEnderecos: any[] = this.ContaMatriz();
    for (var i = 1; i < matrizEnderecos.length; i++) {
      this.CriaMatriz(this.ContaX(i), this.ContaY(i), i);
    }
  }

  CriaBloco(elem: any) {
    var td = document.createElement('td');
    try {
      const index = this.blocos.findIndex(e => e.idend == elem?.idend);
      if (index > -1) {
        this.blocos.splice(index, 1);
      }
      else {
        td.className = 'Rua';
      }
    }
    catch (error) {
      console.log(error);
      return td;
    }
    try {
      if (elem?.status == 1 || elem?.status == 0) {
        if (elem?.status != null && elem?.altura == 0) { //aqui so pega elem vazio ou com bag
          td.className = 'Inativo ' + elem.ala.toString().toUpperCase();
          var text = document.createTextNode(elem?.altura);
          td.appendChild(text);
        } else {
          td.className = 'Ativo ' + elem.ala.toString().toUpperCase(); //passando ativo para pegar o css

          console.log()

          this._bagsService.data.forEach(data => { ///mudar essa logica
            if (elem.idend == data.idEndereco && data.certificacao != "") {
              td.className = 'Certificado ' + elem.ala.toString().toUpperCase();
            }
          })

          td.setAttribute("id", elem.idend.toString()); //passando o idend como id do td
          // td.addEventListener("click", (event) => {
          //   return this.ListaBagsEndereco((<HTMLElement>event.target).id) //pegando o clique para listar os bags
          // });

          var text = document.createTextNode(elem?.altura); //pegando a altura do json
          td.appendChild(text); //escrevendo a altura na tela
        }
      }
      else if (elem?.status != null && elem?.status == 5) { //aqui so pega elem de coluna
        td.className = 'Coluna ' + elem.ala.toString().toUpperCase();
        td.setAttribute("id", elem.idend.toString());
        var text = document.createTextNode(elem.ala);
        td.appendChild(text);
      }
      else if (elem?.status != null && elem?.status == 20) { //aqui so pega elem de coluna
        td.className = 'Validado ' + elem.ala.toString().toUpperCase();
        td.setAttribute("id", elem.idend.toString());
        var text = document.createTextNode(elem.altura);
        td.appendChild(text);
      }
      else if (elem?.status != null && elem?.status == 10) { //aqui so pega elem vertical
        td.className = 'Vertical ' + elem.ala.toString().toUpperCase();
        td.setAttribute("id", elem.idend.toString());
        var text = document.createTextNode(elem.ala);
        td.appendChild(text);
      }
      else if (elem?.status != null && elem?.status > 1) { //aqui so pega elem do amarelo
        td.className = 'Ala ' + elem.ala.toString().toUpperCase();
        td.setAttribute("id", elem.idend.toString());
        td.addEventListener("click", (event) => {
          return this.ListaAlasEndereco(
            (<HTMLElement>event.target).className, (<HTMLElement>event.target).innerHTML
          )
        });

        var text = document.createTextNode(elem.ala);
        td.appendChild(text);
        switch (elem?.status) {
          case 2: {
            //inserir linha acima
            break;
          }
          default: {
            td.className = 'Rua';
          }
        }
      }
      return td
    } catch (error) {
      console.log(error);
      return td;
    }
  }

  CriaLinha(ala: string, table: any, matriz: number) {
    var tabela = document.getElementById('matriz' + matriz.toString());
    var tr = document.createElement('tr');
    tr.setAttribute("id", ala);
    table!.appendChild(tr);
    tabela!.appendChild(table!);
    return tr
  }

  CriaMatriz(quantidadeLinha: number, quantidadeColuna: number, matriz: number) {
    let elem: any;
    var something = document.getElementsByClassName('matrizes');
    var div = document.createElement('div');
    div.setAttribute('id', 'matriz' + matriz.toString());
    something[0].appendChild(div);
    var table = document.createElement('table');
    table.setAttribute('class', 'matriz' + matriz.toString());
    div.appendChild(table);
    for (var x = 1; x < quantidadeLinha; x++) {
      elem = this.blocos.find(e => e.coordx == x && e.coordy == y)
      var tr = this.CriaLinha('', table, matriz);
      for (var y = 1; y < quantidadeColuna; y++) {
        elem = this.blocos.find(e => e.coordx == x && e.coordy == y && e.matriz == matriz);
        tr.appendChild(this.CriaBloco(elem));
      }
    }
  }

  ReloadCss() {
    var links = document.getElementsByTagName("link");
    for (var cl in links) {
      var link = links[cl];
      if (link.rel === "stylesheet")
        link.href += "";
    }
  }

  ListaAlasEndereco(id: any, ala: any): any {
    this.dialogRef = this._matDialog.open(ModalComponent, {
      panelClass: 'add-lote-dialog',
      data: {
        ala: ala,
        nome: this.nome,
        enderecos: JSON.parse(JSON.stringify(this.blocosAll))
      }
    });

    // this.dialogRef.afterClosed().subscribe(async (response: any[]) => {
    //   if (!response) {
    //     return;
    //   }

    //   //console.log(response)

    //   if (response[0] == 'validar') {

    //     for (let idEndereco of response[1]) {

    //       let td = document.getElementById(idEndereco);
    //       console.log(td)
    //       if (td != null) {
    //         if (td.className != 'Inativo') {
    //           td.style.backgroundColor = 'blue';
    //         }
    //       }
    //     }

    //   } else {

    //     for (let idEndereco of response[1]) {

    //       let td = document.getElementById(idEndereco);

    //       if (td != null) {
    //         if (td.className != 'Inativo') {
    //           if (td.className.includes('Certifi')) {
    //             td.style.backgroundColor = 'brown';
    //           } else {
    //             td.style.backgroundColor = 'green';
    //           }
    //         }
    //       }
    //     }
    //   }
    // });
  }

  ultimosLotes: any[] = [];

  FiltraLotes() {
    if (this.ultimosLotes.length > 0) {
      for (let idEndereco of this.ultimosLotes) {

        let td = document.getElementById(idEndereco.toString());

        if (td != null) {
          if (td.className.includes('Certifi')) {
            td.style.backgroundColor = 'brown';
          } else {
            td.style.backgroundColor = 'green';
          }
        }
      }
    }
    this._apiService.getByLote(this.lote).then((res: any[]) => {

      let enderecos = [...new Set(res.map(x => x.idEndereco))];

      for (let idEndereco of enderecos) {

        let td = document.getElementById(idEndereco.toString());

        if (td != null) {
          td.style.backgroundColor = 'purple';
        }
      }
      this.ultimosLotes = enderecos;
    }, reject => {
      console.log(reject)
    });
  }

  LimparConsulta() {
    if (this.ultimosLotes.length > 0) {
      for (let idEndereco of this.ultimosLotes) {

        let td = document.getElementById(idEndereco.toString());

        if (td != null) {
          if (td.className.includes('Certifi')) {
            td.style.backgroundColor = 'brown';
          } else {
            td.style.backgroundColor = 'green';
          }
        }
      }
    }
    this.lote = "";
  }

  ExportExcel() {
    this._apiService.getByExport().then(res => {
      this.excelService.exportAsExcelFile(res, 'Inventário MZB' + new Date().toISOString());
      console.log(res);
    }, reject => {
      console.log(reject)
    });
  }

  Zoom(matriz: any) {
    //panzoom(matriz);
    // const zoom = matriz.addEventListener("click", (event: any) => {
    //   return <HTMLElement>event.target
    // })
    // console.log(zoom)
  }



}
