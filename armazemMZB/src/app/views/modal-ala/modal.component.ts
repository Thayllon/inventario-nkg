import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/shared/service/api.service';
import { BagsService } from 'src/app/shared/service/bags.service';
import { MapaService } from 'src/app/shared/service/enderecos.service';
import { UserService } from 'src/app/shared/service/usuarios.service';
import { ModalDetalhesComponent } from '../modal-detalhes/modal-detalhes.component';

@Component({
  selector: 'modal-ala',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModalComponent {
  BagsAla: any[] = [];
  EnderecosAla: any[] = [];
  BagsCabecalhoAla: any[] = [];
  pageTitle: string = "";
  isLoading: boolean = false;
  lotes: any;
  cabecalho: any;
  nome: string = "";
  dialogRef: any;
  isValid: boolean = false;



  constructor(public matDialogRef: MatDialogRef<ModalComponent>, private _apiService: ApiService,
    @Inject(MAT_DIALOG_DATA) private _data: any, private _mapaService: MapaService, private _matDialog: MatDialog,
    private _bagsService: BagsService, private _apiServiceUser: UserService,) {
    this.pageTitle = _data.ala;

    this.isLoading = true;

    this.IsValid(_data.enderecos);
    this.EnderecosAla = _data.enderecos;
    this.isLoading = false;

    this._apiService.getByConsultaLote(_data.ala).then(res => {
      this.BagsAla = res;
      this.isLoading = false;
      this.MostraModal();
    }, reject => {
      this.isLoading = false;
      console.log(reject);
    });

    this._apiService.getByConsultaResumoAla(_data.ala).then(res => {
      this.BagsCabecalhoAla = res;
      this.isLoading = false;
      this.CabecalhoModal();
    }, reject => {
      this.isLoading = false;
      console.log(reject);
    });
  }

  MostraModal() {
    this.lotes = [...new Set(this.BagsAla.map(x => x))];

    this.lotes = this.lotes.map((x: any) => {
      return {
        lote: x.lote,
        saldo_kg: x.saldo_kg,
        qtd_bag: x.qtd_bag,
        saldo_scs: Math.trunc(x.saldo_scs)
      }
    });
  }

  CabecalhoModal() {
    this.cabecalho = [...new Set(this.BagsCabecalhoAla.map(x => x))]; //entender isso

    this.cabecalho = this.cabecalho.map((x: any) => {
      return {
        bags: x.bags,
        lotes: x.lotes,
        fracionados: x.fracionados
      }
    });
  }

  // getSaldoBag(LotesAla: any[]) {
  //   return LotesAla.reduce((a, b) => a + Number(b.saldoLote), 0);
  // }

  // getQtdBag(LotesAla: any[]) {
  //   return LotesAla.reduce((a, b) => a + Number(b.qtdBag), 0);
  // }

  // getQtdBagTrans(LotesAla: any[]) {
  //   return LotesAla.reduce((a, b) => a + Number(b.qtdBagTrans), 0);
  // }

  // getSaldoBagTrans(LotesAla: any[]) {
  //   return LotesAla.reduce((a, b) => a + Number(b.saldoLoteTrans), 0);
  // }

  ValidarAla() {
    let enderecos = [...new Set(this.EnderecosAla.map((data: any) => {
      if (data.ala == this.pageTitle) {
        return data.idend;
      }
    }))];

    for (let data of enderecos) {
      this._apiService.postValidacaoAla(data);
    }

    let bags = [...new Set(this.BagsAla.map(x => x))];

    let validando = bags.map((x: any) => {
      return {
        alaInvent: this.pageTitle,
        loteInvent: x.lote,
        qtdKgLoteSist: 0,
        qtdScsLoteSist: 0,
        qtdBagsLoteInvent: x.qtd_bag,
        NrDispositivo: this._apiServiceUser.dadosUser.usuario,
        qtdKgLoteValid: x.saldo_kg,
        qtdScsValid: Math.floor(x.saldo_scs),
        idUser: 0,
        descOperacao: "string",
        statusAla: 1,
      }
    })

    for (let data of validando) {

      console.log(JSON.stringify(data))

      this._apiService.postSalvarValidacaoAla(data)
        .then(res => {
          console.log(res);
        }, reject => {
          console.log(reject)
        });
    }

    for (let idEndereco of enderecos) {

      let td = document.getElementById(idEndereco);

      if (td != null) {
        if (td.className != 'Inativo') {
          td.style.backgroundColor = 'blue';
        }
      }
    }

    this.AvancarAla();
  }

  InvalidarAla() {
    let enderecos = [...new Set(this.EnderecosAla.map((data: any) => {
      if (data.ala == this.pageTitle) {
        return data.idend;
      }
    }))];

    for (let end of enderecos) {
      this._apiService.postInvalidarAla(end);
    }

    this._apiService.postInvalidarAlaLogInventario(this.pageTitle);

    for (let idEndereco of enderecos) {

      let td = document.getElementById(idEndereco);

      if (td != null) {
        if (td.className != 'Inativo') {
          td.style.backgroundColor = 'green';

          // if (td.className.includes('Certifi')) {
          //   td.style.backgroundColor = 'brown';
          // } else if (td.className.includes('Ativo')) {
          //   td.style.backgroundColor = 'green';
          // }
        }
      }
    }
  }

  AvancarAla() {
    let proximaAla = this._mapaService.data.map(x => x.ala)[this._mapaService.data.findIndex(x => x.ala == this.pageTitle) + 1];

    if (proximaAla) {
      this.isLoading = true;
      this.pageTitle = proximaAla;
      this.IsValid(this.EnderecosAla);

      this._apiService.getByConsultaLote(proximaAla).then(res => {
        this.BagsAla = res;
        this.isLoading = false;
        this.MostraModal();
        this.pageTitle = proximaAla;
      }, reject => {
        this.isLoading = false;
        console.log(reject)
      });

      this._apiService.getByConsultaResumoAla(proximaAla).then(res => {
        this.BagsCabecalhoAla = res;
        this.isLoading = false;
        this.CabecalhoModal();
        this.pageTitle = proximaAla;

      }, reject => {
        this.isLoading = false;
        console.log(reject)
      });
    }
  }

  VoltarAla() {
    let proximaAla = this._mapaService.data.map(x => x.ala)[this._mapaService.data.findIndex(x => x.ala == this.pageTitle) - 1];

    if (proximaAla) {
      this.isLoading = true;
      this.pageTitle = proximaAla;
      this.IsValid(this.EnderecosAla);

      this._apiService.getByConsultaLote(proximaAla).then(res => {
        this.BagsAla = res;
        this.isLoading = false;
        this.MostraModal();
        this.pageTitle = proximaAla;
      }, reject => {
        this.isLoading = false;
        console.log(reject)
      });

      this._apiService.getByConsultaResumoAla(proximaAla).then(res => {
        this.BagsCabecalhoAla = res;
        this.isLoading = false;
        this.CabecalhoModal();
        this.pageTitle = proximaAla;
      }, reject => {
        this.isLoading = false;
        console.log(reject)
      });
    }
  }

  AbrirModalDetalhes() {
    this.dialogRef = this._matDialog.open(ModalDetalhesComponent, {
      panelClass: 'add-lote-dialog',
      data: {
        titulo: 'Detalhes',
        ala: this.pageTitle
      }
    });
  }

  IsValid(alas: any[]) {
    this.isValid = alas.filter(x => x.ala == this.pageTitle)[0].status == 20;
  }

}