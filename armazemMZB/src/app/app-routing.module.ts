import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArmazemComponent } from './views/armazem/armazem.component';
import { LoginComponent } from './views/login/login.component';

export const routes: Routes = [
  { path: "", redirectTo: "/armazem", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "armazem", component: ArmazemComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
