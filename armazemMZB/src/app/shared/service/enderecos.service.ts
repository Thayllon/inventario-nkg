import { Mapa } from "../model/mapa.model";

export class MapaService {

  data: Mapa[] = [
    //Matriz 4
    {
      "coordx": 1,
      "coordy": 1,
      "matriz": 4,
      "idend": 100,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //BLOCOS AMARELOS   
    //Matriz 1
    {
      "coordx": 9,
      "coordy": 1,
      "matriz": 1,
      "idend": 1,
      "ala": "1B65",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 2,
      "matriz": 1,
      "idend": 2,
      "ala": "1B66",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 3,
      "matriz": 1,
      "idend": 3,
      "ala": "1B67",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 4,
      "matriz": 1,
      "idend": 4,
      "ala": "1B68",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 5,
      "matriz": 1,
      "idend": 5,
      "ala": "1B69",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 6,
      "matriz": 1,
      "idend": 6,
      "ala": "1B70",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 7,
      "matriz": 1,
      "idend": 7,
      "ala": "1B71",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 8,
      "matriz": 1,
      "idend": 8,
      "ala": "1B72",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 9,
      "matriz": 1,
      "idend": 9,
      "ala": "1B73",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 10,
      "matriz": 1,
      "idend": 10,
      "ala": "1B74",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 11,
      "matriz": 1,
      "idend": 11,
      "ala": "1B75",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 12,
      "matriz": 1,
      "idend": 12,
      "ala": "1B76",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 13,
      "matriz": 1,
      "idend": 13,
      "ala": "1B77",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 14,
      "matriz": 1,
      "idend": 14,
      "ala": "1B78",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 15,
      "matriz": 1,
      "idend": 15,
      "ala": "1B79",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 16,
      "matriz": 1,
      "idend": 16,
      "ala": "1B80",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 17,
      "matriz": 1,
      "idend": 17,
      "ala": "1B81",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 18,
      "matriz": 1,
      "idend": 18,
      "ala": "1B82",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 19,
      "matriz": 1,
      "idend": 19,
      "ala": "1B83",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 20,
      "matriz": 1,
      "idend": 20,
      "ala": "1B84",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 21,
      "matriz": 1,
      "idend": 21,
      "ala": "1B85",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 22,
      "matriz": 1,
      "idend": 22,
      "ala": "1B86",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 23,
      "matriz": 1,
      "idend": 23,
      "ala": "1B87",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 24,
      "matriz": 1,
      "idend": 24,
      "ala": "1B88",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 25,
      "matriz": 1,
      "idend": 25,
      "ala": "1B89",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 26,
      "matriz": 1,
      "idend": 26,
      "ala": "1B90",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 28,
      "matriz": 1,
      "idend": 27,
      "ala": "1B94",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 6,
      "coordy": 28,
      "matriz": 1,
      "idend": 28,
      "ala": "1B93",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 7,
      "coordy": 28,
      "matriz": 1,
      "idend": 29,
      "ala": "1B92",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 28,
      "matriz": 1,
      "idend": 30,
      "ala": "1B91",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 1,
      "matriz": 1,
      "idend": 31,
      "ala": "1B64",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 2,
      "matriz": 1,
      "idend": 32,
      "ala": "1B63",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 3,
      "matriz": 1,
      "idend": 33,
      "ala": "1B62",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 4,
      "matriz": 1,
      "idend": 34,
      "ala": "1B61",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 5,
      "matriz": 1,
      "idend": 35,
      "ala": "1B60",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 6,
      "matriz": 1,
      "idend": 36,
      "ala": "1B59",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 7,
      "matriz": 1,
      "idend": 37,
      "ala": "1B58",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 8,
      "matriz": 1,
      "idend": 38,
      "ala": "1B57",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 9,
      "matriz": 1,
      "idend": 39,
      "ala": "1B56",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 10,
      "matriz": 1,
      "idend": 40,
      "ala": "1B55",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 11,
      "matriz": 1,
      "idend": 41,
      "ala": "1B54",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 12,
      "matriz": 1,
      "idend": 42,
      "ala": "1B53",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 13,
      "matriz": 1,
      "idend": 43,
      "ala": "1B52",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 14,
      "matriz": 1,
      "idend": 44,
      "ala": "1B51",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 15,
      "matriz": 1,
      "idend": 45,
      "ala": "1B50",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 16,
      "matriz": 1,
      "idend": 46,
      "ala": "1B49",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 17,
      "matriz": 1,
      "idend": 47,
      "ala": "1B48",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 20,
      "matriz": 1,
      "idend": 48,
      "ala": "1B47",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 21,
      "matriz": 1,
      "idend": 49,
      "ala": "1B46",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 22,
      "matriz": 1,
      "idend": 50,
      "ala": "1B45",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 23,
      "matriz": 1,
      "idend": 51,
      "ala": "1B44",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 24,
      "matriz": 1,
      "idend": 52,
      "ala": "1B43",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 25,
      "matriz": 1,
      "idend": 53,
      "ala": "1B42",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 26,
      "matriz": 1,
      "idend": 54,
      "ala": "1B41",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 27,
      "matriz": 1,
      "idend": 55,
      "ala": "1B40",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 28,
      "matriz": 1,
      "idend": 56,
      "ala": "1B39",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 29,
      "matriz": 1,
      "idend": 57,
      "ala": "1B38",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 31,
      "matriz": 1,
      "idend": 58,
      "ala": "1B37",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 31,
      "matriz": 1,
      "idend": 59,
      "ala": "1B36",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 31,
      "matriz": 1,
      "idend": 60,
      "ala": "1B35",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //Forçar Matriz 1
    {
      "coordx": 20,
      "coordy": 32,
      "matriz": 1,
      "idend": 1000,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //Matriz 2
    {
      "coordx": 1,
      "coordy": 2,
      "matriz": 2,
      "idend": 1,
      "ala": "1B95",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 2,
      "coordy": 2,
      "matriz": 2,
      "idend": 2,
      "ala": "1B96",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 3,
      "coordy": 2,
      "matriz": 2,
      "idend": 3,
      "ala": "1B97",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 2,
      "matriz": 2,
      "idend": 4,
      "ala": "1B98",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 2,
      "matriz": 2,
      "idend": 5,
      "ala": "1B99",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 6,
      "coordy": 2,
      "matriz": 2,
      "idend": 6,
      "ala": "1B100",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 7,
      "coordy": 2,
      "matriz": 2,
      "idend": 7,
      "ala": "1B101",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 2,
      "matriz": 2,
      "idend": 8,
      "ala": "1B102",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 8,
      "matriz": 2,
      "idend": 9,
      "ala": "1B103",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 9,
      "matriz": 2,
      "idend": 10,
      "ala": "1B104",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 10,
      "matriz": 2,
      "idend": 11,
      "ala": "1B105",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 11,
      "matriz": 2,
      "idend": 12,
      "ala": "1B106",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 12,
      "matriz": 2,
      "idend": 13,
      "ala": "1B107",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 13,
      "matriz": 2,
      "idend": 14,
      "ala": "1B108",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 14,
      "matriz": 2,
      "idend": 15,
      "ala": "1B109",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 15,
      "matriz": 2,
      "idend": 16,
      "ala": "1B110",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 16,
      "matriz": 2,
      "idend": 17,
      "ala": "1B111",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 17,
      "matriz": 2,
      "idend": 18,
      "ala": "1B112",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 18,
      "matriz": 2,
      "idend": 19,
      "ala": "1B113",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 19,
      "matriz": 2,
      "idend": 20,
      "ala": "1B114",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 20,
      "matriz": 2,
      "idend": 21,
      "ala": "1B115",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 21,
      "matriz": 2,
      "idend": 22,
      "ala": "1B116",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 22,
      "matriz": 2,
      "idend": 23,
      "ala": "1B117",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 23,
      "matriz": 2,
      "idend": 24,
      "ala": "1B118",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 24,
      "matriz": 2,
      "idend": 25,
      "ala": "1B119",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 25,
      "matriz": 2,
      "idend": 26,
      "ala": "1B120",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 26,
      "matriz": 2,
      "idend": 27,
      "ala": "1B121",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 27,
      "matriz": 2,
      "idend": 28,
      "ala": "1B122",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 28,
      "matriz": 2,
      "idend": 29,
      "ala": "1B123",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 29,
      "matriz": 2,
      "idend": 30,
      "ala": "1B124",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 30,
      "matriz": 2,
      "idend": 31,
      "ala": "1B125",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 31,
      "matriz": 2,
      "idend": 32,
      "ala": "1B126",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 32,
      "matriz": 2,
      "idend": 33,
      "ala": "1B127",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 33,
      "matriz": 2,
      "idend": 34,
      "ala": "1B128",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 2,
      "matriz": 2,
      "idend": 35,
      "ala": "1B27",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 2,
      "matriz": 2,
      "idend": 36,
      "ala": "1B28",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 2,
      "matriz": 2,
      "idend": 37,
      "ala": "1B29",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 2,
      "matriz": 2,
      "idend": 38,
      "ala": "1B30",
      "bloco": "",
      "status": 2,
      "altura": 0
    },

    {
      "coordx": 16,
      "coordy": 2,
      "matriz": 2,
      "idend": 39,
      "ala": "1B31",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 2,
      "matriz": 2,
      "idend": 40,
      "ala": "1B32",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 18,
      "coordy": 2,
      "matriz": 2,
      "idend": 41,
      "ala": "1B33",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 3,
      "matriz": 2,
      "idend": 42,
      "ala": "1B34",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 8,
      "matriz": 2,
      "idend": 43,
      "ala": "1B26",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 9,
      "matriz": 2,
      "idend": 44,
      "ala": "1B25",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 10,
      "matriz": 2,
      "idend": 45,
      "ala": "1B24",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 11,
      "matriz": 2,
      "idend": 46,
      "ala": "1B23",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 12,
      "matriz": 2,
      "idend": 47,
      "ala": "1B22",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 13,
      "matriz": 2,
      "idend": 48,
      "ala": "1B21",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 14,
      "matriz": 2,
      "idend": 49,
      "ala": "1B20",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 15,
      "matriz": 2,
      "idend": 50,
      "ala": "1B19",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 16,
      "matriz": 2,
      "idend": 51,
      "ala": "1B18",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 17,
      "matriz": 2,
      "idend": 52,
      "ala": "1B17",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 18,
      "matriz": 2,
      "idend": 53,
      "ala": "1B16",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 19,
      "matriz": 2,
      "idend": 54,
      "ala": "1B15",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 20,
      "matriz": 2,
      "idend": 55,
      "ala": "1B14",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 21,
      "matriz": 2,
      "idend": 56,
      "ala": "1B13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 22,
      "matriz": 2,
      "idend": 57,
      "ala": "1B12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 23,
      "matriz": 2,
      "idend": 58,
      "ala": "1B11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 24,
      "matriz": 2,
      "idend": 59,
      "ala": "1B10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 25,
      "matriz": 2,
      "idend": 60,
      "ala": "1B9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 26,
      "matriz": 2,
      "idend": 61,
      "ala": "1B8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 27,
      "matriz": 2,
      "idend": 62,
      "ala": "1B7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 28,
      "matriz": 2,
      "idend": 63,
      "ala": "1B6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 29,
      "matriz": 2,
      "idend": 64,
      "ala": "1B5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 30,
      "matriz": 2,
      "idend": 65,
      "ala": "1B4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 31,
      "matriz": 2,
      "idend": 66,
      "ala": "1B3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 32,
      "matriz": 2,
      "idend": 67,
      "ala": "1B2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 33,
      "matriz": 2,
      "idend": 68,
      "ala": "1B1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //Forçar Matriz 2
    {
      "coordx": 20,
      "coordy": 34,
      "matriz": 2,
      "idend": 1000,
      "ala": "",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    //Matriz 3
    {
      "coordx": 3,
      "coordy": 14,
      "matriz": 3,
      "idend": 1,
      "ala": "1A54",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 4,
      "coordy": 15,
      "matriz": 3,
      "idend": 2,
      "ala": "1A53",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 15,
      "matriz": 3,
      "idend": 3,
      "ala": "1A52",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 6,
      "coordy": 15,
      "matriz": 3,
      "idend": 4,
      "ala": "1A51",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 7,
      "coordy": 15,
      "matriz": 3,
      "idend": 5,
      "ala": "1A50",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 8,
      "coordy": 15,
      "matriz": 3,
      "idend": 6,
      "ala": "1A49",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 9,
      "coordy": 15,
      "matriz": 3,
      "idend": 7,
      "ala": "1A48",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 10,
      "coordy": 15,
      "matriz": 3,
      "idend": 8,
      "ala": "1A47",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 11,
      "coordy": 15,
      "matriz": 3,
      "idend": 9,
      "ala": "1A46",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 12,
      "coordy": 15,
      "matriz": 3,
      "idend": 10,
      "ala": "1A45",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 13,
      "coordy": 15,
      "matriz": 3,
      "idend": 11,
      "ala": "1A44",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 15,
      "matriz": 3,
      "idend": 12,
      "ala": "1A43",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 15,
      "matriz": 3,
      "idend": 13,
      "ala": "1A42",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 17,
      "matriz": 3,
      "idend": 14,
      "ala": "1A41",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 18,
      "matriz": 3,
      "idend": 15,
      "ala": "1A40",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 19,
      "matriz": 3,
      "idend": 16,
      "ala": "1A39",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 20,
      "matriz": 3,
      "idend": 17,
      "ala": "1A38",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 21,
      "matriz": 3,
      "idend": 18,
      "ala": "1A37",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 22,
      "matriz": 3,
      "idend": 19,
      "ala": "1A36",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 23,
      "matriz": 3,
      "idend": 20,
      "ala": "1A35",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 24,
      "matriz": 3,
      "idend": 21,
      "ala": "1A34",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 25,
      "matriz": 3,
      "idend": 22,
      "ala": "1A33",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 26,
      "matriz": 3,
      "idend": 23,
      "ala": "1A32",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 27,
      "matriz": 3,
      "idend": 24,
      "ala": "1A31",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 28,
      "matriz": 3,
      "idend": 25,
      "ala": "1A30",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 29,
      "matriz": 3,
      "idend": 26,
      "ala": "1A29",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 30,
      "matriz": 3,
      "idend": 27,
      "ala": "1A28",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 31,
      "matriz": 3,
      "idend": 28,
      "ala": "1A27",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 34,
      "matriz": 3,
      "idend": 29,
      "ala": "1A26",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 35,
      "matriz": 3,
      "idend": 30,
      "ala": "1A25",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 36,
      "matriz": 3,
      "idend": 31,
      "ala": "1A24",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 37,
      "matriz": 3,
      "idend": 32,
      "ala": "1A23",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 38,
      "matriz": 3,
      "idend": 33,
      "ala": "1A22",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 39,
      "matriz": 3,
      "idend": 34,
      "ala": "1A21",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 40,
      "matriz": 3,
      "idend": 35,
      "ala": "1A20",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 41,
      "matriz": 3,
      "idend": 36,
      "ala": "1A19",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 42,
      "matriz": 3,
      "idend": 37,
      "ala": "1A18",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 43,
      "matriz": 3,
      "idend": 38,
      "ala": "1A17",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 44,
      "matriz": 3,
      "idend": 39,
      "ala": "1A16",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 45,
      "matriz": 3,
      "idend": 40,
      "ala": "1A15",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 46,
      "matriz": 3,
      "idend": 41,
      "ala": "1A14",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 47,
      "matriz": 3,
      "idend": 42,
      "ala": "1A13",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 48,
      "matriz": 3,
      "idend": 43,
      "ala": "1A12",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 49,
      "matriz": 3,
      "idend": 44,
      "ala": "1A11",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 50,
      "matriz": 3,
      "idend": 45,
      "ala": "1A10",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 51,
      "matriz": 3,
      "idend": 46,
      "ala": "1A9",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 52,
      "matriz": 3,
      "idend": 47,
      "ala": "1A8",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 53,
      "matriz": 3,
      "idend": 48,
      "ala": "1A7",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 54,
      "matriz": 3,
      "idend": 49,
      "ala": "1A6",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 55,
      "matriz": 3,
      "idend": 50,
      "ala": "1A5",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 56,
      "matriz": 3,
      "idend": 51,
      "ala": "1A4",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 57,
      "matriz": 3,
      "idend": 52,
      "ala": "1A3",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 58,
      "matriz": 3,
      "idend": 53,
      "ala": "1A2",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    {
      "coordx": 14,
      "coordy": 59,
      "matriz": 3,
      "idend": 54,
      "ala": "1A1",
      "bloco": "",
      "status": 2,
      "altura": 0
    },
    //Forçar Matriz 3
    {
      "coordx": 15,
      "coordy": 60,
      "matriz": 3,
      "idend": 1000,
      "ala": "1A1",
      "bloco": "",
      "status": 10,
      "altura": 0
    },
    {
      "coordx": 17,
      "coordy": 60,
      "matriz": 3,
      "idend": 1001,
      "ala": "1A1",
      "bloco": "",
      "status": 10,
      "altura": 0
    },

    //COLUNAS
    {
      "coordx": 4,
      "coordy": 7,
      "matriz": 2,
      "idend": 2000,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 5,
      "coordy": 7,
      "matriz": 2,
      "idend": 2001,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 15,
      "coordy": 7,
      "matriz": 2,
      "idend": 2002,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 16,
      "coordy": 7,
      "matriz": 2,
      "idend": 2003,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
    {
      "coordx": 19,
      "coordy": 31,
      "matriz": 2,
      "idend": 2004,
      "ala": "",
      "bloco": "",
      "status": 5,
      "altura": 0
    },
  ];
  constructor() {

  }

}