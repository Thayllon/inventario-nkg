const initialRoute = 'http://cocapec-agrosynccoffee.brazilsouth.cloudapp.azure.com';
const initialRoute2 = 'http://agrosync-cocapec-web.brazilsouth.cloudapp.azure.com';

export const environment = {
    production: false,
    hmr: false,
    //=======================>> APIS NKG <<=======================================
    apiInventario: `${initialRoute}:7075/api`,
    apiAccounts: `${initialRoute}:7075/api`
};