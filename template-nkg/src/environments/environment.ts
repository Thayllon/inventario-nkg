const initialRoute = 'http://cocapec-agrosynccoffee.brazilsouth.cloudapp.azure.com';
const apiEndereco = 'http://172.16.10.10';

export const environment = {
    production: false,
    hmr: false,
    //=======================>> APIS NKG <<=======================================
    apiInventario: `${apiEndereco}:7079/api`,
    apiAccounts: `${initialRoute}:7082/api`
};