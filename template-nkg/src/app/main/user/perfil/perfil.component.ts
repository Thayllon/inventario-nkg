import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import { Subject } from "rxjs";
import { Location } from "@angular/common";
import { MatSnackBar } from "@angular/material/snack-bar";
import { fuseAnimations } from "@fuse/animations";
import { AuthenticationService } from "app/helpers/authentication.service";
import { Router } from "@angular/router";
import { UsuarioAccountService } from "app/shared/usuario-account.service";
import { UserDTO } from "app/shared/usuario.model";


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class PerfilComponent implements OnInit {
  usuario: any;
  pageType: string;
  usuarioForm: FormGroup;

  novaSenha: FormControl;
  senhaAtual: FormControl;

  grupos: any;

  password_validation_messages = {
    'password': [
      { type: 'required', message: 'Senha requerida' },
      { type: 'minlength', message: 'A senha deve conter pelo menos 8 caracteres' },
      { type: 'pattern', message: 'Sua senha deve conter pelo menos uma letra e um número' }
    ]}

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
      private _usuarioService: UsuarioAccountService,
      private _formBuilder: FormBuilder,
      private _location: Location,
      private route: Router,
      private _matSnackBar: MatSnackBar,
      private authenticationService: AuthenticationService
  ) {
      // Set the default
      this.usuario = new UserDTO();
      this.senhaAtual = new FormControl('', Validators.compose([
        Validators.minLength(8),
        Validators.required,
        Validators.pattern('^(?=.*[aA-zZ])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$')
   ]));

      this.novaSenha = new FormControl('', Validators.compose([
        Validators.minLength(8),
        Validators.required,
        Validators.pattern('^(?=.*[aA-zZ])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$')
   ]));
   this.novaSenha.dirty

      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
      // Inscrevo para atualizar o banco sobre alterações
      this._usuarioService.getUserById(this.authenticationService.currentUserValue.userDetails.userId)
      .then(user => {
          if (user) {
              this.usuario = new UserDTO(user);
              this.pageType = "edit";
          } else {
              this.pageType = "new";
              this.usuario = new UserDTO();
          }
          //console.log(this.usuario);
          this.usuarioForm = this.createUsuarioForm();
      });
  }

  ngOnDestroy(): void {
      // Unsubscribe para todos subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  createUsuarioForm(): FormGroup {
      return this._formBuilder.group({
        userId: [this.usuario.userId],
        accountId:[this.usuario.accountId],
        firstName: [this.usuario.firstName],
        lastName: [this.usuario.lastName],
        username: [this.usuario.username],
        password: [this.usuario.password],
        email: [this.usuario.email],
        userGroupId: [this.usuario.userGroupId],
        phoneNumber: [this.usuario.phoneNumber],
        activated: [this.usuario.activated],
        mainUser: [this.usuario.mainUser],
        developmentUser: [this.usuario.developmentUser],
        vendedorId: [this.usuario.vendedorId],
        twoFactorPassword: [this.usuario.twoFactorPassword],
        twoFactorEnabled: [this.usuario.twoFactorEnabled]
      });
  }

  updateUsuario() {
      const data = this.usuarioForm.getRawValue();
      
      console.log(data);
      this._usuarioService.update(data).then(
          () => {
              // Aciono a assinatura com os novos dados
              this._usuarioService.onDataChanged.next(data);

              // Show mensagem de sucesso
              this._matSnackBar.open("Dados atualizados com sucesso", "OK", {
                  verticalPosition: "top",
                  duration: 2000
              });

          },
          error => {
              this._matSnackBar.open("Erro ao atualizar dados", "OK", {
                  verticalPosition: "top",
                  duration: 2000
              });
          }
      );
  }

  updateSenha(){
    this._usuarioService.testeLogin(this.authenticationService.currentUserValue.userDetails.username, this.senhaAtual.value)
    .then(res=>{
      if(res){
        //this.usuarioForm.controls['password'].setValue(this.novaSenha);
        //this.updateUsuario();
        let data: any = {
          userId: this.authenticationService.currentUserValue.userDetails.userId,
          oldPassword: this.senhaAtual.value,
          newPassword: this.novaSenha.value
        };

        this._usuarioService.changePassword(data).then(res=>{
          console.log('CHANGE PASSWORD');
          console.log(res);
          if(res == 'Senha atualizada com sucesso'){
            this.senhaAtual.setValue('');
            this.novaSenha.setValue('');
            this._matSnackBar.open("Senha atualizada com sucesso", "OK", {
              verticalPosition: "top",
              duration: 2000
            });
          }
        }, reject =>{

        });
        
      }
      else{
        this._matSnackBar.open("Senha atual incorreta", "OK", {
          verticalPosition: "top",
          duration: 2000
        });
      }
    },
    reject=>{
      this._matSnackBar.open("Senha atual incorreta", "OK", {
        verticalPosition: "top",
        duration: 2000
      });
    });
  }
}

