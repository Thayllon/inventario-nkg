import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";

import localePt from '@angular/common/locales/pt';
import { registerLocaleData, CurrencyPipe } from '@angular/common';
registerLocaleData(localePt);

import { FuseSharedModule } from "@fuse/shared.module";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ConfirmDeleteDialogModule } from 'app/shared/confirm-delete/confirm-delete.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { FormsBasicosModule } from 'app/shared/forms.module';
import { FuseSidebarModule } from '@fuse/components';

import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { PerfilComponent } from './perfil.component';



const routes: Routes = [
    {
        path: "",
        component: PerfilComponent
    }
];

@NgModule({
    declarations: [
        PerfilComponent,

    ],
    imports: [
        RouterModule.forChild(routes),

        MatSnackBarModule,
        MatSortModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatSelectModule,
        ColorPickerModule,
        FuseSharedModule,
        ConfirmDeleteDialogModule,
        FuseSidebarModule,
        FormsBasicosModule
    ],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ],
    entryComponents:[
        
    ]
})
export class PerfilUserModule {}
