import { Injectable } from "@angular/core";
import { CrudService } from "app/shared/crud-service";
import { HttpClient } from "@angular/common/http";
import { environment } from "environments/environment";
import {
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Resolve
} from "@angular/router";
import { Observable, BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class DashBoardService {
    routeParams: any;
    data: any;
    onDataChanged: BehaviorSubject<any>;

    constructor(protected http: HttpClient) { }

}
