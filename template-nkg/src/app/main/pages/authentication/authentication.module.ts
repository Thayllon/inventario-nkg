import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path: 'forgot-password',
        loadChildren: './forgot-password/forgot-password.module#ForgotPasswordModule'
    },
    {
        path: 'forgot-password-2',
        loadChildren: './forgot-password-2/forgot-password-2.module#ForgotPassword2Module'
    },
    {
        path: 'lock',
        loadChildren: './lock/lock.module#LockModule'
    },
    {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
    },
    {
        path: 'login-2',
        loadChildren: './login-2/login-2.module#Login2Module'
    },
    {
        path: 'mail-confirm',
        loadChildren: './mail-confirm/mail-confirm.module#MailConfirmModule'
    },
    {
        path: 'register',
        loadChildren: './register/register.module#RegisterModule'
    },
    {
        path: 'register-2',
        loadChildren: './register-2/register-2.module#Register2Module'
    },
    {
        path: 'reset-passwordd',
        loadChildren: './reset-password/reset-password.module#ResetPasswordModule'
    },
    {
        path: 'reset-password',
        loadChildren: './reset-password-2/reset-password-2.module#ResetPassword2Module'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class AuthenticationModule {

}
