import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { FuseSharedModule } from '@fuse/shared.module';

import { ResetPassword2Component } from 'app/main/pages/authentication/reset-password-2/reset-password-2.component';
import { ResetPasswordService } from './reset-password-2.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const routes = [
    {
        path     : ':token',
        component: ResetPassword2Component,
        resolve: {
            data: ResetPasswordService
        }
    },
    {
        path     : "",
        component: ResetPassword2Component,
        resolve: {
            data: ResetPasswordService
        }
    }

];

@NgModule({
    declarations: [
        ResetPassword2Component
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSnackBarModule,

        FuseSharedModule
    ],
    providers: [ResetPasswordService]
})
export class ResetPassword2Module
{
}
