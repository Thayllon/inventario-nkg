import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ResetPasswordService } from './reset-password-2.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
    selector     : 'reset-password-2',
    templateUrl  : './reset-password-2.component.html',
    styleUrls    : ['./reset-password-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ResetPassword2Component implements OnInit, OnDestroy
{
    resetPasswordForm: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    password_validation_messages = {
        'password': [
          { type: 'required', message: 'Senha requerida' },
          { type: 'minlength', message: 'A senha deve conter pelo menos 8 caracteres' },
          { type: 'pattern', message: 'Sua senha deve conter pelo menos uma letra e um número' }
        ]}

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _matSnackBar: MatSnackBar,
        private route: Router,
        private _resetPassWord: ResetPasswordService,
        private _formBuilder: FormBuilder
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.resetPasswordForm = this._formBuilder.group({
            //name           : ['', Validators.required],
            //email          : ['', [Validators.required, Validators.email]],
            password       : ['', 
            Validators.compose([
                Validators.minLength(8),
                Validators.required,
                Validators.pattern('^(?=.*[aA-zZ])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
                //Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
           ])],
            passwordConfirm: ['', [Validators.compose([
                Validators.minLength(8),
                Validators.required,
                Validators.pattern('^(?=.*[aA-zZ])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
                //Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
           ]), confirmPasswordValidator]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.resetPasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    resetPassword(){
        if(this._resetPassWord.token!=''){
            
            let aux: any = {
                token: this._resetPassWord.token,
                password: this.resetPasswordForm.controls['password'].value
            }
            
            /*this._usuarioService.resetPassword(aux).then((res)=>{
                //console.log('SENHA RESETADA?');
                //console.log(res);

                this._matSnackBar.open(res, "OK", {
                    verticalPosition: "top",
                    duration: 5000
                });

                this.route.navigate(['/pages/auth/login-2']);

            }, reject=>{

            });*/
        }
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {passwordsNotMatching: true};
};
