import { Injectable } from "@angular/core";
import { CrudService } from "app/shared/crud-service";
import { HttpClient } from "@angular/common/http";
import { environment } from "environments/environment";
import {
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Resolve,
    Router
} from "@angular/router";
import { Observable, BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class ResetPasswordService implements Resolve<any> {
    routeParams: any;
    Arraydata: any[];
    data: any;
    //onDataChanged: BehaviorSubject<any>;

    token: string = '';

    constructor(protected http: HttpClient,
        private route: Router) {
    }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;
        console.log('routeParams');
        console.log(this.routeParams.length);
        console.log(this.routeParams.token);
        
        if (this.routeParams.token != undefined) {
            this.token = this.routeParams.token;
        } else {
            this.route.navigate(['/pages/auth/login-2']);
        }
    }
}
