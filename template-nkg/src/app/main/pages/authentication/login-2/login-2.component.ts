import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { AuthenticationService } from "app/helpers/authentication.service";
import { first } from "rxjs/operators";
import { MatSnackBar } from "@angular/material/snack-bar";
import { PermissionModulesService } from "app/helpers/permission-modules.service";

@Component({
    selector: "login-2",
    templateUrl: "./login-2.component.html",
    styleUrls: ["./login-2.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Login2Component implements OnInit {
    loginForm: FormGroup;
    rotaDefault = "/inventario-module/inventario";
    loading: boolean = false;

    password_validation_messages = {
        'password': [
          { type: 'required', message: 'Senha requerida' },
          { type: 'minlength', message: 'A senha deve conter pelo menos 8 caracteres' },
          { type: 'pattern', message: 'Sua senha deve conter pelo menos uma letra e um número' }
        ]}
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private route: Router,
        private _permissionModuleService: PermissionModulesService,
        private authenticationService: AuthenticationService,
        private _matSnackBar: MatSnackBar
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {        
        this.loading = false;
        this.loginForm = this._formBuilder.group({
            usuario: ["", [Validators.required]],
            password: ["", Validators.compose([
                Validators.minLength(8),
                Validators.required,
                Validators.pattern('^(?=.*[aA-zZ])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
                //Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%&]+$') //this is for the letters (both uppercase and lowercase) and numbers validation
           ])]
        });
        
    }

    async entrar() {
        if (!this.loginForm.valid) return;

        this.loading = true;
        this.authenticationService.login(this.loginForm.controls.usuario.value, this.loginForm.controls.password.value).pipe(first())
            .subscribe(async data => {
                console.log(data);

                this.route.navigate([this.rotaDefault]);
                this.loading = false;
                this._matSnackBar.open(
                    "Bem vindo " + this.authenticationService.currentUserValue.userDetails.firstName,null,
                    {
                        verticalPosition: "top",
                        duration: 2000
                    }
                );
                    
                },
                error => {
                    this._matSnackBar.open(
                        "Usuário ou senha incorretos",
                        "OK",
                        {
                            verticalPosition: "top",
                            duration: 2000
                        }
                    );
                    this.loading = false;
                }
            );
    }
}
