import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
//import { UsuariosService } from 'app/main/modulo-contratos-futuros/cadastros/configuracao/usuario/usuarios.service';
//import { UsuarioService } from 'app/main/modulo-contratos-futuros/cadastros/configuracao/usuario/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
    selector     : 'forgot-password-2',
    templateUrl  : './forgot-password-2.component.html',
    styleUrls    : ['./forgot-password-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ForgotPassword2Component implements OnInit
{
    forgotPasswordForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */

    clicou: boolean = false;

    constructor(
        private _fuseConfigService: FuseConfigService,
        //private _usuarioService: UsuarioService,
        private _matSnackBar: MatSnackBar,
        private route: Router,
        private _formBuilder: FormBuilder
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    enviarEmail(){
        //this.clicou = true;
        let email: any = {
            email: this.forgotPasswordForm.controls['email'].value
        }

        /*this._usuarioService.ForgetPassword(this.forgotPasswordForm.controls['email'].value).then((res)=>{
            console.log('FORGET PASSWORD');
            console.log(res);
            if(res == 'Foi enviado um link de validação para o email cadastrado'){

                this._matSnackBar.open(res, "OK", {
                    verticalPosition: "top",
                    duration: 5000
                });

                this.clicou = false;
                this.route.navigate(['/pages/auth/login-2']);
            
            }
        }, reject=>{
            this.clicou = false;
        });*/
    }
}
