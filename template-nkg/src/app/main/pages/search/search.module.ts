import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path: 'classic',
        loadChildren: './classic/search-classic.module#SearchClassicModule'
    },
    {
        path: 'modern',
        loadChildren: './modern/search-modern.module#SearchModernModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class SearchModule {

}
