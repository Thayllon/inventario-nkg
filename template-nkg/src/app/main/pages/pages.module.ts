import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'auth',
        loadChildren: './authentication/authentication.module#AuthenticationModule'
    },
    {
        path        : 'coming-soon',
        loadChildren: './coming-soon/coming-soon.module#ComingSoonModule'
    },
    {
        path        : 'errors',
        loadChildren: './errors/errors.module#ErrorsModule'
    },
    {
        path        : 'faq',
        loadChildren: './faq/faq.module#FaqModule'
    },
    {
        path        : 'invoices',
        loadChildren: './invoices/invoices.module#InvoicesModule'
    },
    {
        path        : 'knowledge-base',
        loadChildren: './knowledge-base/knowledge-base.module#KnowledgeBaseModule'
    },
    {
        path        : 'maintenance',
        loadChildren: './maintenance/maintenence.module#MaintenanceModule'
    },
    {
        path        : 'pricing',
        loadChildren: './pricing/pricing.module#PricingModule'
    },
    {
        path        : 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
    },
    {
        path        : 'search',
        loadChildren: './search/search.module#SearchModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class PagesModule
{

}
