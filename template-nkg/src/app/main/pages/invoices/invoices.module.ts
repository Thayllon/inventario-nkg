import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path: 'compact',
        loadChildren: './compact/compact.module#InvoiceCompactModule'
    },
    {
        path: 'modern',
        loadChildren: './modern/modern.module#InvoiceModernModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class InvoicesModule {

}
