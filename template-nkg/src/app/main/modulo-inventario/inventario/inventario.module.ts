import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localePt);

import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import { AlertModule } from 'ngx-alerts';
import { ConfirmDeleteDialogModule } from 'app/shared/confirm-delete/confirm-delete.module';
import { MatButtonModule, MatCardModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatSelectModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatToolbarModule } from "@angular/material";
import { FuseSharedModule } from "@fuse/shared.module";
import { FormsBasicosModule } from "app/shared/forms.module";
import { InventarioComponent } from "./inventario/inventario.component";
import { InventarioService } from "./services/inventario.service";


const routes: Routes = [
    {
        path: "",
        component: InventarioComponent,
        /*resolve: {
            data: InventarioService
        }*/
    }
];

@NgModule({
    declarations: [InventarioComponent],
    imports: [
        RouterModule.forChild(routes),
        AlertModule.forRoot({maxMessages: 2, timeout: 5000, position: 'right'}),
        ConfirmDeleteDialogModule,
        FormsBasicosModule,
        MatIconModule,
        MatSnackBarModule,
        MatSortModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatInputModule,
        MatToolbarModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatRadioModule,
        MatProgressBarModule,
        FuseSharedModule
    ],
    providers: [
        InventarioService,
        {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
     ],
    entryComponents: [
      ]
})
export class InventarioModule {}