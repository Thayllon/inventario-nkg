import { Injectable } from "@angular/core";
import { CrudService } from "app/shared/crud-service";
import { HttpClient } from "@angular/common/http";
import { environment } from "environments/environment";

import {
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Resolve
} from "@angular/router";
import { Observable, BehaviorSubject } from "rxjs";
import { AuthenticationService } from "app/helpers/authentication.service";
import { DadosInventario } from "../models/inventario.model";

@Injectable({
    providedIn: "root"
})
export class InventarioService extends CrudService<DadosInventario> implements Resolve<any> {
    routeParams: any;

    constructor(protected http: HttpClient,
        private _authenticationService: AuthenticationService) {
        super(http, `${environment.apiInventario}/Inventario`);
    }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;
        if (this.routeParams.id === "new") {
            return new Promise((resolve, reject) => {
                this.onDataChanged.next(false);
                resolve(false);
            });
        } else {
            return this.getById(this.routeParams.id);
        }
    }

    cancel(remessaId: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${environment.apiInventario}/Inventario/cancel`, 
            {
                id: remessaId
            })
            .subscribe((response: any) => {
                resolve(response);
            }, reject);
        });
    }

}
