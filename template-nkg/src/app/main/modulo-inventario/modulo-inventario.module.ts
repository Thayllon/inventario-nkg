import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localePt);
import { AuthGuard } from "app/helpers/auth.guard";
import { FuseSharedModule } from "@fuse/shared.module";

const routes: Routes = [
    {
        path: 'inventario',
        loadChildren: () => import('./inventario/inventario.module').then(m => m.InventarioModule),
        canActivate: [AuthGuard]
    }
];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule
    ],
    providers: [
        AuthGuard
    ]
})
export class ModuloInventarioModule {}