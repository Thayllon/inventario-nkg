export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
    dateExpirateToken: string;
    grupoUsuarioId: string;
    grupoUsuario: any;
    dataAtualizacao: string;
    statusMovimento: number;
    usuarioId: string;
    observacoesMovimento: string;
    statusSincronizacao: number;

    constructor(user?) {
        user = user || {};
        this.id = user.id || 0;
        this.username = user.username || "";
        this.password = user.password || "";
        this.firstName = user.firstName || "";
        this.lastName = user.lastName || "";        
        this.token = user.token || "";
        this.dateExpirateToken = user.dateExpirateToken || "";
        this.grupoUsuarioId = user.grupoUsuarioId || 0;
        this.grupoUsuario = user.grupoUsuario || {};
        this.dataAtualizacao = user.dataAtualizacao || "";
        this.statusMovimento = user.statusMovimento || 0;
        this.usuarioId = user.usuarioId || '';
        this.observacoesMovimento = user.observacoesMovimento || "";
        this.statusSincronizacao = user.statusSincronizacao || 0;
    }
}