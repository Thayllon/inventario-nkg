export class userDetail{
    userId: string;
    accountId: string;
    password: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    developmentUser: boolean;
    mainUser: boolean;
    activated: boolean;
    twoFactorEnabled: boolean;
    twoFactorPassword: string;
    groupName: string;
    userGroupId: string;
    vendedorId: number;
    userImage: string;
    accountName: string;

    constructor(values?){
        values = values || {};

        this.userId = values.userId || "";
        this.accountId = values.accountId || "";
        this.password = values.password || "";
        this.username = values.username || "";
        this.firstName = values.firstName || "";
        this.lastName = values.lastName || "";
        this.email = values.email || "";
        this.phoneNumber = values.phoneNumber || "";
        this.developmentUser = values.developmentUser || false;
        this.mainUser = values.mainUser || false;
        this.activated = values.activated || false;
        this.twoFactorEnabled = values.twoFactorEnabled || false;
        this.twoFactorPassword = values.twoFactorPassword || "";
        this.groupName = values.groupName || "";
        this.userGroupId = values.userGroupId || "";
        this.vendedorId = values.vendedorId || 0;
        this.userImage = values.userImage || "";
        this.accountName = values.accountName || "";
    }
}