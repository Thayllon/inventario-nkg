import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { User } from './user.model';
import { PermissionModulesService } from './permission-modules.service';


@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient,
        private _modules: PermissionModulesService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(usuario: string, senha: string) {
        return this.http.post<any>(`${environment.apiAccounts}/v1/user/login`, { username: usuario, password: senha })
            .pipe(map((user: User) => {
                if (user && user.accessToken) {
                    // armazena detalhes do usuário e credenciais básicas de autenticação no armazenamento local para manter o usuário logado entre as atualizações da página
                    console.log(JSON.stringify(user));
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // remove usuário do armazenamento local para fazer logout do usuário
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this._modules.resetModules();
    }

}
