import { Injectable } from "@angular/core";
import {
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
    CanActivate,
    Router
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticationService } from "./authentication.service";
import { PermissionModulesService } from "./permission-modules.service";

@Injectable({
    providedIn: "root"
})
export class AuthGuard implements CanActivate {
    rotaDefault: string = "pages/auth/login-2";

    controlePorGrupo: boolean = false;

    excessoes: any[] = [
        'meu-perfil'
    ];

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private _modules: PermissionModulesService
    ) {}

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        console.log(currentUser);
        if(currentUser){
            if (currentUser.userDetails!=undefined) {

                console.log('ROTA = ' + route.url[0].path);

                if(!this.controlePorGrupo){
                    return true;
                }

                /*if(this._modules.getModulesPermitidos().length>0){
                    if(this._modules.getModulesPermitidos().filter(x=> x.route.split('/').includes(route.url[0].path)).length > 0
                    || this.excessoes.filter(x=>x == route.url[0].path).length > 0 ){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                else{//por refresh a lista de permissões e perdeu

                    let res = await this._groupUsersService.listPermissions();

                        if(res){
                            for(let item of res){
                                if(item.canRead || item.canWrite){
                                    this._modules.setPermissionEscrita(item.companySystemModuleItemId, item.canWrite);
                                    this._modules.setPermissionLeitura(item.companySystemModuleItemId, item.canRead);
                                }
                            }

                            if(this._modules.getModulesPermitidos().length>0){

                                if(this._modules.getModulesPermitidos().filter(x=> x.route.split('/').includes(route.url[0].path)).length > 0){
                                    this._modules.updateNavigation();
                                    
                                    return true;
                                }
                                else{
                                    this.authenticationService.logout();
                                }
                            }
                            else{
                                return false;
                            }
                            
                        }
                    //});
                }*/
                
            }else{
                this.authenticationService.logout();
            }
        }    
        

        console.log("nao está logado");
        // not logged in so redirect to login page with the return url
        this.router.navigate(
            [this.rotaDefault] /*, { queryParams: { returnUrl: state.url } }*/
        );
        return false;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            // logged in so return true
            console.log(route.url);
            console.log("esta logadoU");
            return true;
        }

        console.log("nao está logado");
        // not logged in so redirect to login page with the return url
        this.router.navigate(
            [this.rotaDefault] /*, { queryParams: { returnUrl: state.url } }*/
        );
        return false;
    }

    canLoad() {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            // logged in so return true
            console.log(this.router.url);
            console.log("esta logado");
            return true;
        }

        console.log("nao está logado");
        // not logged in so redirect to login page with the return url
        this.router.navigate(
            [this.rotaDefault] /*, { queryParams: { returnUrl: state.url } }*/
        );
        return false;
    }
}
