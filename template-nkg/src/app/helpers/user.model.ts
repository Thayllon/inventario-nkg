import { userDetail } from './user-details.model';

export class User {

    accountId: string;
    accessToken: string;
    accountImage: string;
    expiresIn: number;
    usuarioToken: any;
    userDetails: userDetail;

    constructor(user?) {
        user = user || {};
        
        this.accountId = user.accountId || "";
        this.accessToken = user.accessToken || "";
        this.accountImage = user.accountImage || "";
        this.expiresIn = user.expiresIn || 0;
        this.usuarioToken = user.usuarioToken || 0; 
        this.userDetails = new userDetail(user.lastName) || new userDetail(); 
    }
}