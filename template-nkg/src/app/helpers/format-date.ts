import { Injectable, Component } from '@angular/core';
import { DatePipe } from '@angular/common';

@Injectable({
    providedIn: 'root'
})

export class FormatDate {

    constructor(private datePipe: DatePipe) {

    }

    public formatTo_yyyy_mm_dd(date: string) {
        return this.datePipe.transform(date, 'yyyy/MM/dd');
    }

    public formatTo_yyyy_mm_dd_h_m_s(date: string) {
        return this.datePipe.transform(date, 'yyyy/MM/dd hh:mm:ss');
    }

    public formatTo_dd_mm_yyyy(date: string) {
        return this.datePipe.transform(date, 'dd/MM/yyyy');
    }

    public formatTo_dd_mm_yyyy_h_m_s(date: string) {
        return this.datePipe.transform(date, 'dd/MM/yyyy hh:mm:ss');
    }

    public atualDate() {
        var today = new Date();
        return today.toJSON();
    }
}
