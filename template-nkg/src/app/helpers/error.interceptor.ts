import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import { ErrorMessagesService } from 'app/shared/printers/error-messages.service';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    rotasIgnoradas = [
        'GrupoNegocio',
        'DadosAdicionaisPropriedade/SelecionarPorPropriedadeId',
        'DadosAdicionaisPropriedade/SelecionarPorParceiroId',
        'Interveniente/SelecionarPorParceiroId'
    ]
    constructor(private authenticationService: AuthenticationService,
        private _errorPrint: ErrorMessagesService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // logout automático se a resposta 401 for retornada da API
                this.authenticationService.logout();
                location.reload();
            }

            if(this.rotasIgnoradas.filter((value)=> { if(String(err.url).includes(value)) return true; }).length == 0 ){
                this._errorPrint.printErrors(err);
            }
            return throwError(err);
        }))
    }
}