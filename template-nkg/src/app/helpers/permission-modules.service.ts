import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";
import { isUndefined } from "lodash";
import { PermissionModule } from "./permission-modules.model";

@Injectable({
    providedIn: 'root'
  })
  export class PermissionModulesService {
    routeParams: any;
  
    systemModules: PermissionModule[] = [
        {
            id: 1,
            descricao: 'INVENTÁRIO',
            permiteEscrita: false,
            permiteLeitura: false,
            route: 'inventario',
            icon: 'dashboard',
            navigationName: 'analytics'
        }
        
    ];

    constructor(private _fuseNavigationService: FuseNavigationService,
        private _matSnackBar: MatSnackBar) {
    }


    getModules(): PermissionModule[] {
        return this.systemModules;
    }

    getModuleById(id: number): PermissionModule {
        return this.systemModules.find(x=>x.id == id);
    }

    getModulesPermitidos(): PermissionModule[] {
        return this.systemModules.filter(x=>x.permiteEscrita == true || x.permiteLeitura == true);
    }

    setPermissionAll(id: number, permitir: boolean) {
        this.systemModules.find(x=>x.id == id).permiteEscrita = permitir;
        this.systemModules.find(x=>x.id == id).permiteLeitura = permitir;
    }

    setPermissionEscrita(id: number, permitir: boolean) {
        try{
            //console.log('id = ' + id + ' permite? ' + permitir);
            if(this.systemModules.filter(x=>x.id == id).length>0){
                this.systemModules.find(x=>x.id == id).permiteEscrita = permitir;
            }
        }catch{

        }
    }

    isPermited(navigationName: string): boolean {
        //return true;
        if(this.systemModules.filter(x=>x.navigationName == navigationName).length>0){
            if(this.systemModules.find(x=>x.navigationName == navigationName).permiteEscrita){
               return true;
            }
            else{
                this._matSnackBar.open("Não possui permissão para esta ação.", "OK", {
                    verticalPosition: "top",
                    duration: 2000
                });
                return false;
            }
        }
        else{
            this._matSnackBar.open("Módulo não cadastrado. Não possui permissão para esta ação.", "OK", {
                verticalPosition: "top",
                duration: 2000
            });
            return false;
        }        
    }

    setPermissionLeitura(id: number, permitir: boolean) {
        try{
            if(this.systemModules.filter(x=>x.id == id).length>0){
                this.systemModules.find(x=>x.id == id).permiteLeitura = permitir;
            }
        }catch{

        }
    }

    ajustaChildrens(filho) : boolean{
        //console.log(filho.id);
        if(filho.children != null && filho.children != undefined){
            let cont = 0;

            for(let item of filho.children){
                if(item.children != null && item.children != undefined){
                    if(this.ajustaChildrens(item)){
                        cont = cont + 1;
                    }
                }
                else{
                    if(item.hidden == true){
                        cont = cont + 1;
                    }
                }
            }

            if(cont == filho.children.length){
                this._fuseNavigationService.updateNavigationItem(filho.id, {
                    hidden: true
                });
                return true;
            }
            else{
                this._fuseNavigationService.updateNavigationItem(filho.id, {
                    hidden: false
                });
                return false;
            }
        }

    }

    updateNavigation(){
        this.systemModules.forEach((module)=>{

            this._fuseNavigationService.updateNavigationItem(module.navigationName, {
                hidden: false
            });

            if(!module.permiteEscrita && !module.permiteLeitura){
                this._fuseNavigationService.updateNavigationItem(module.navigationName, {
                    hidden: true
                });
            }
        });

        console.log(this._fuseNavigationService.getNavigation('main'));

        for(let group of this._fuseNavigationService.getNavigation('main')){
            
            this.ajustaChildrens(group);
        }
    }

    resetModules(){
        this.systemModules.forEach((module)=>{
            module.permiteEscrita = false;
            module.permiteLeitura = false;
        });
    }
    
}