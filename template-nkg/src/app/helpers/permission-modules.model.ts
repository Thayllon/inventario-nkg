export class PermissionModule{
    id: number;
    descricao: string;
    route: string;
    permiteLeitura: boolean;
    permiteEscrita: boolean;
    icon: string;
    navigationName: string;


    constructor(permission?){
        permission = permission || {};

        this.id = permission.id || 0;
        this.descricao = permission.descricao || '';
        this.route = permission.route || '';
        this.permiteLeitura = permission.permiteLeitura || false;
        this.permiteEscrita = permission.permiteEscrita || false;
        this.icon = permission.icon || '';
        this.navigationName = permission.navigationName || '';
    }
}