import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { catchError, map, take } from "rxjs/operators";
import { BehaviorSubject, Observable, throwError } from "rxjs";

export class CrudService<T> {
    Arraydata: any[];
    data: any;
    onDataChanged: BehaviorSubject<any>;

    constructor(protected http: HttpClient, private API_URL) {
        this.onDataChanged = new BehaviorSubject({});
    }

    getAll(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.API_URL}`).subscribe((response: any) => {
                this.Arraydata = response;
                this.onDataChanged.next(this.Arraydata);                
                resolve(response);
            }, reject);
        });
    }

    getById(id: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .get(`${this.API_URL}/${id}`)
                .subscribe((response: any) => {
                    this.data = response;
                    this.onDataChanged.next(this.data);
                    resolve(response);
                    //console.log(response);
                }, reject);
        });
    }

    update(record: T): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .put(`${this.API_URL}`, record)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    create(record: T): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.API_URL}`, record)
            .subscribe((response: any) => {
                resolve(response);
            }, reject);
        });
    }

    delete(id: number): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .delete(`${this.API_URL}/${id}`)
                .subscribe((response: any) => {
                    resolve(response);
                }, reject=>{
                    
                });
        });
    }
}
