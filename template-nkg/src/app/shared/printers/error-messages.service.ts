import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessagesService {

  private timeOut = 3000;

  constructor(private _matSnackBar: MatSnackBar) { }

  printErrors(errorResponse){
    console.log(errorResponse);
    console.log(errorResponse.error);
    if(errorResponse.error != null){
      if(errorResponse.error.errors != undefined){
        errorResponse.error.errors.Mensagens.forEach((message, index)=>{
          setTimeout(() => {
            
            this._matSnackBar.open(message, 'OK', {
                duration: this.timeOut,
                verticalPosition: 'top',
            });
  
        }, index * (this.timeOut+500));
  
          });
      }
      else{
        this._matSnackBar.open('Erro na requisição', 'OK', {
          duration: this.timeOut,
          verticalPosition: 'top',
        });
      }
    }
    else{
      this._matSnackBar.open('Erro na requisição', 'OK', {
        duration: this.timeOut,
        verticalPosition: 'top',
      });
    }
    
  }

}
