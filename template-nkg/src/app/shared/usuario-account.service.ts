import { Injectable } from "@angular/core";
import { CrudService } from "app/shared/crud-service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "environments/environment";
import {
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Resolve
} from "@angular/router";
import { Observable, BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class UsuarioAccountService extends CrudService<any>{
    routeParams: any;

    constructor(protected http: HttpClient) {
        super(http, `${environment.apiAccounts}/v1/user/`);
    }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;
        if (this.routeParams.id === "new") {
            return new Promise((resolve, reject) => {
                this.onDataChanged.next(false);
                resolve(false);
            });
        } else {
            return this.getUserById(this.routeParams.id);
        }
    }

    getUserById(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .get(`${environment.apiAccounts}/v1/user/get-user?userId=${id}`)
                .subscribe((response: any) => {
                    this.data = response;
                    this.onDataChanged.next(this.data);
                    resolve(response);
                    //console.log(response);
                }, reject);
        });
    }

    register(record: any): Promise<any> {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        
        return new Promise((resolve, reject) => {
            this.http.post(`${environment.apiAccounts}/v1/user/register`, record, {responseType: 'text'}).subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject
                
            );
        });
    }

    changePassword(record: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${environment.apiAccounts}/v1/user/change-password`, record,{responseType: 'text'}).subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject
            );
        });
    }

    resetPassword(record: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${environment.apiAccounts}/v1/user/reset-password`, record).subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject
            );
        });
    }

    ForgetPassword(email: any): Promise<any> {

        return new Promise((resolve, reject) => {
            this.http.post(`${environment.apiAccounts}/v1/user/forget-password/${email}`, '').subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject => {
                    console.log(reject);
                }
            );
        });
    }

    testeLogin(usuario: string, senha: string): Promise<any>  {
        return new Promise((resolve, reject) => {
            this.http.post(`${environment.apiAccounts}/v1/user/login`, { username: usuario, password: senha }).subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject => {
                    resolve(null);
                }
            );
        });
    }

    updateUser(record: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${environment.apiAccounts}/v1/user/edit`, record,{responseType: 'text'}).subscribe(
                (response: any) => {
                    resolve(response);
                },
                reject
            );
        });
    }
}
