export class Email {
    id: number;
    enderecoEmail: string;
    emailPrincipal: boolean;
    dataAtualizacao: string;
    statusMovimento: number;
    usuarioId: string;
    observacoesMovimento: string;
    statusSincronizacao: number;

    constructor(email?) {
        email = email || {};
        this.id = email.id || 0;
        this.enderecoEmail = email.enderecoEmail || "";
        this.emailPrincipal = email.emailPrincipal || false;
        this.dataAtualizacao = email.dataAtualizacao || "";
        this.statusMovimento = email.statusMovimento || 0;
        this.usuarioId = email.usuarioId || '';
        this.observacoesMovimento = email.observacoesMovimento || "";
        this.statusSincronizacao = email.statusSincronizacao || 0;
    }
}