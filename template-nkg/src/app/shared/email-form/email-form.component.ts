import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CalendarEvent } from 'angular-calendar';
import { MatColors } from '@fuse/mat-colors';
import { Email } from './email.model';

@Component({
  selector: 'app-email-form',
  templateUrl: './email-form.component.html',
  styleUrls: ['./email-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EmailFormComponent {
  action: string;
  email: Email;
  emailForm: FormGroup;
  dialogTitle: string;
  presetColors = MatColors.presets;

  constructor(
      public matDialogRef: MatDialogRef<EmailFormComponent>,
      @Inject(MAT_DIALOG_DATA) private _data: any,
      private _formBuilder: FormBuilder
  )
  {
      this.email = _data.email;
      this.action = _data.action;

      if ( this.action === 'edit' )
      {
          this.dialogTitle = this.email.enderecoEmail;
      }
      else
      {
          this.dialogTitle = 'Novo email';
          this.email = new Email();
      }

      this.emailForm = this.createEmailForm();
  }

  createEmailForm(): FormGroup
  {
    return this._formBuilder.group({
      id: [this.email.id],
      enderecoEmail: [this.email.enderecoEmail],
      emailPrincipal: [this.email.emailPrincipal],
      dataAtualizacao: [this.email.dataAtualizacao],
      statusMovimento: [this.email.statusMovimento],
      usuarioId: [this.email.usuarioId],
      observacoesMovimento: [this.email.observacoesMovimento],
      statusSincronizacao: [this.email.statusSincronizacao],
    });
  }
}
