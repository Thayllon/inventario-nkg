import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { EmailFormComponent } from './email-form/email-form.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ColorPickerModule } from 'ngx-color-picker';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatAutocompleteModule, MatCardModule, MatCheckboxModule, MatExpansionModule, MatProgressSpinnerModule, MatSelectModule } from '@angular/material';

@NgModule({
    declarations: [
        EmailFormComponent
    ],
    imports: [
        MatSnackBarModule,
        MatSortModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatToolbarModule,
        MatCardModule,
        MatTooltipModule,
        ColorPickerModule,
        FuseSharedModule,
    ],
    exports: [
        MatSnackBarModule,
        MatSortModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatToolbarModule,
        MatCardModule,
        MatTooltipModule,
        ColorPickerModule,
        FuseSharedModule,
    ],
    entryComponents: [
        EmailFormComponent
    ],
})
export class FormsBasicosModule {

}
