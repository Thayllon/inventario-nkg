import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class DateConvertService {

     private monthNames = ["JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"];

    constructor() {
    }

    format_DD_MM_YYYY(date){

        let data = date ? new Date(date) : new Date();
        let dia  = data.getDate().toString();
        let diaF = (dia.length == 1) ? '0'+dia : dia;
        let mes  = (data.getMonth()+1).toString(); //+1 pois no getMonth Janeiro começa com zero.
        let mesF = (mes.length == 1) ? '0'+mes : mes;
        let anoF = data.getFullYear();
        return diaF+"/"+mesF+"/"+anoF;
    }

    format_HH_MM_SS(date){

        let data = date ? new Date(date) : new Date();
        let hora  = data.getHours();
        let minuto = data.getMinutes();
        let segundos = data.getSeconds();
        return hora+":"+minuto+":"+segundos;
    }

    getMes(mesNumber): string {
        return this.monthNames[mesNumber];
    }

    getDifDates(dataInicio: any, dataFim: any){
        var date1 = new Date(dataInicio);
        var date2 = new Date(dataFim);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        return diffDays;
    }

}
