//============================<ENUNS CONTRATOS FUTUROS>==============================
export enum OrigemLancamento {
    Manual=1,
    ERP_Proprio=2,
    ERP_Terceiro=3,
}

export enum SituacaoMovimento {
    Pendente = 1,
    Firmado = 2
}

export enum SituacaoAutorizacao {
    Pendente = 1,
    Baixada = 2,
    Travada = 3,
    Expirada = 4,
    Digitação = 7,
    Cancelada = 9
}

export enum TipoFinalidade {
    FUTURO = 1,
    'TROCA CASEIRA' = 2,
    'TROCA EMPRESA' = 3,
    CPR = 4
}

export enum TipoEquivalencia{
    Sacas = 1,
    Financeiro = 2
    
}

export enum SituacaoContrato{
    Pendente = 1,
    Travado=2,
    Firmado = 3,
    Cancelado = 9
}

export enum SituacaoPedido{
    Pendente = 1,
    Faturado=8,
    Cancelado = 9
}

export enum TipoSaldo{
    Todos = 0,
    Aberto = 1,
    Fechado = 2    
}

export enum TipoVinculo{
    Todos = 1,
    SemDiferenca=2
}

export enum EnumSituacaoContrato{
    Pendente = "Pendente",
    Travado="Travado",
    Firmado = "Firmado",
    Cancelado = "Cancelado"
}

export enum SituacaoVinculoTermo{
    Aberto = 1,
    Fechado = 2
}

export enum EnumSituacaoVinculoTermo{
    Aberto = "Aberto",
    Fechado = "Fechado"
}

export enum SituacaoAjuste{
    Pendente = 1,
    Firmado = 3
}

export enum RegistroCetip {
    Banco = 1,
    'Nosso_Pendente' = 2,
    'Nosso_Registrado' = 3,
    'N_A'=9
}

export enum TipoDerivativo{
    Principal = 1,
    Complementar = 2
}

export enum TipoOperacaoDerivativo{
    Compra = 1,
    Venda = 2
}

export enum FinalidadeDerivativo {
    'Proteção de Futuro' = 1,
    'Proteção Exportação' = 2,
    'Proteção Estoque' = 3
}

export enum SituacaoTermo{
    Pendente = 1,
    Vigente = 2,
    Encerrado= 3
}

export enum EnumSituacaoTermo{
    Pendente = "Pendente",
    Vigente = "Vigente",
    Encerrado= "Encerrado"
}

export enum ModalidadePosicaoPAD{
    'Posição/Banco'=2,
    Estimativa=1
}

export enum ModalidadeSafraComercial{
    'Venda Futura' = 1,
    'CPR' = 2,
    'CPR Credi' = 3,
    'Outros' = 4,
    'Leilão de Opções' = 5,
    'Credi Veículos' = 6,
    'Troca Sacas' = 7,
    'Troca Financeira' = 8,
    'Mora Venda Futura' = 9,
    'Mora Troca' = 10
}

export enum EstadoCivil {
    'UNIÃO ESTÁVEL'=7,
    'SOLTEIRO'=1,
    'CASADO'=2,
    'CONCUBINATO'=3,
    'SEPARADO JUDICIALM'=4,
    'DIVORCIADO'=5,
    'VIÚVO'=6
}

export enum RegimeCasamento {
    'COM TOTAL BENS A LEI'=1,
    'COMUNH PARCIAL BENS'=2,
    'SEPARAÇÃO TOTAL BENS'=3,
    'COM TOTAL DE BENS'=4,
    'SEPARAÇÃO PARC BENS'=5
}

export enum SituacaoSolicitacao {
    'EmAprovacaoComite'=0,
    'DevolutivaComite'=1
}

export enum TipoPropriedade {
    'PropriedadeRural' = 1
}

export enum Unidades {
    'COCAPEC - MATRIZ' = 1,
    'COCAPEC - PEDREGULHO' = 2,
    'COCAPEC - CLARAVAL' = 3,
    'COCAPEC - ARMAZÉM GERAL CEAGESP' = 4,
    'COCAPEC - IBIRACI' = 5,
    'COCAPEC - CAPETINGA' = 6,
    'COCAPEC - DEPOSITO PEDREGULHO' = 7,
    'COCAPEC - ARMAZÉM GERAO II FRANCA' = 8,
    'COCAPEC - ARMAZÉM GERAL IBIRACI ' = 9,
    'COCAPEC - DEPÓSITO CLARAVAL' = 10,
    'COCAPEC - CRISTAIS PAULISTA' = 11
}

export enum StatusSolicitacao {
    Pendente=0,
    Baixado=1,
    Expirado=4,
    Cancelado=9
}

export enum TipoFiador {
    FIADOR = 1,
    PROCURADOR = 2,
    DIRETOR = 3,
    'GRUPO NEGOCIO' = 4
}

//============================</ENUNS CONTRATOS FUTUROS>==============================

//============================<ENUNS GUIAS>===========================================
export enum TipoFechamento {
    PARCIAL = 0,
    TOTAL = 1
}

export enum TipoOperacaoMov {
    ENTRADA = 0,
    SAIDA = 1
}

export enum StatusOS {
    PENDENTE = 0,
    FIRMADO = 1,
    EM_EXECUCAO = 2,
    CONCLUIDA = 3,
    BLOQUEADA = 4,
    CANCELADA = 9
}

export enum TipoCalculoTipoOS {

    'REBENEFÍCIO' = 1,
    'PILHA/LIGA/TRANSF.' = 2,
    'VENDA' = 3,
    'DEVOLUÇÃO/TROCA TITUL.' = 4
}

export enum TipoCobrancaRetencao {
    'FIXO SOBRE TOTAL' = 0,
    'POR SACA' = 1,
    'PROP. DIAS MÊS' = 2
}

export enum TipoTaxaRetencao {
    'PORCENTAGEM' = 0,
    'VALOR' = 1
}

export enum TipoTitularRemessa {
    'COOPERADO' = 1,
    'TERCEIRO' = 2
}

export enum EnumStatusRemessa {
    PENDENTE = 0,
    EMITIDO = 1,
    FATURADO = 2 ,
    CANCELADO = 3
}

export enum EnumTipoRemessa{
    PONTUAL = 0 ,
    MENSAL = 1
}

export enum TipoDespersonalizacao {
    GERAL = 1,
    DESPERSONALIZADO = 2,
    NORMAL = 3
}

export function EnumtoArray(enumm) : any[] {
    return Object.keys(enumm)
                  .filter(k => typeof enumm[k] === 'number')
                  .map(text => ({ text, value: enumm[text] }));
}