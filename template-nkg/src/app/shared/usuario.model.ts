
export class UserDTO
{
    userId: string;
    accountId: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;	
    email: string;	
    phoneNumber: string;	
    developmentUser: boolean;
    mainUser: boolean;
    activated: boolean;
    twoFactorEnabled: boolean;
    twoFactorPassword: string;	
    userGroupId: string;
    vendedorId: number;

    constructor(user?){
      user = user || {};

        this.userId = user.userId || "";
        this.accountId = user.accountId || "";
        this.username = user.username || "";
        this.password = user.password || "";
        this.firstName = user.firstName || "";
        this.lastName = user.lastName || "";
        this.email = user.email || "";
        this.phoneNumber = user.phoneNumber || "";
        this.developmentUser = user.developmentUser || false;	
        this.mainUser = user.mainUser || false;
        this.activated = user.activated || false;
        this.twoFactorEnabled = user.twoFactorEnabled || false;
        this.twoFactorPassword = user.twoFactorPassword || "";
        this.userGroupId = user.userGroupId || "";
        this.vendedorId= user.vendedorId || 0;

    }
}