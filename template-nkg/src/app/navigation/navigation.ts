import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'funcoes',
        title: 'Funções',
        //translate: 'NAV.APPLICATIONS',
        type: 'group',
        icon: 'apps',
        children: [
            {
                id: 'analytics',
                title: 'Inventário',
                type: 'item',
                icon: 'dashboard',
                url: '/inventario-module/inventario'
            }
        ]
    },
    {
        id: 'solici',
        title: 'GERENCIAMENTO',
        type: 'group',
        icon: 'apps',
        children: [
            {
                id: 'item1',
                title: 'Gerenciamento 1',
                type: 'item',
                icon: 'question_answer',
                url: '#'
            },
            {
                id: 'item2',
                title: 'Gerenciamento 2',
                type: 'item',
                icon: 'equalizer',
                url: '#'
            }
           
        ]
    },
    {
        id: 'cadastros',
        title: 'Configuração',
        //translate: 'NAV.APPLICATIONS',
        type: 'group',
        icon: 'apps',
        children: [
            {
                id: 'config1',
                title: 'Config 1',
                type: 'item',
                icon: 'subject',
                url: '#'
            },
            {
                id: 'config2',
                title: 'Config 2',
                type: 'item',
                icon: 'subject',
                url: '#'
            },
            {
                id: 'usuario',
                title: 'Usuários',
                type: 'item',
                icon: 'person',
                url: '#'
            }
        ]
    }
];
