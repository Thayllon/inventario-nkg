import { ErrorHandler, Injectable, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { InMemoryWebApiModule } from "angular-in-memory-web-api";
import { TranslateModule } from "@ngx-translate/core";
import "hammerjs";

import { FuseModule } from "@fuse/fuse.module";
import { FuseSharedModule } from "@fuse/shared.module";
import {
    FuseProgressBarModule,
    FuseSidebarModule,
    FuseThemeOptionsModule
} from "@fuse/components";

import { fuseConfig } from "app/fuse-config";


import { FakeDbService } from "app/fake-db/fake-db.service";
import { AppComponent } from "app/app.component";
import { AppStoreModule } from "app/store/store.module";
import { LayoutModule } from "app/layout/layout.module";
import { AuthGuard } from "./helpers/auth.guard";
import { AuthenticationService } from "./helpers/authentication.service";
import { JwtInterceptor } from "./helpers/jwt.interceptor";
import { ErrorInterceptor } from "./helpers/error.interceptor";
import { CurrencyPipe, DatePipe } from '@angular/common';
import { MatSnackBarModule } from '@angular/material';
import { ExcelService } from './helpers/excelService.service';
import { PermissionModulesService } from "./helpers/permission-modules.service";

const appRoutes: Routes = [
    {
        path: "apps",
        loadChildren: "./main/apps/apps.module#AppsModule",
        canActivate: [AuthGuard]
    },
    {
        path: "pages",
        loadChildren: "./main/pages/pages.module#PagesModule"
    },
    {
        path: "ui",
        loadChildren: "./main/ui/ui.module#UIModule"
    },
    {
        path: "documentation",
        loadChildren:
            "./main/documentation/documentation.module#DocumentationModule"
    },
    {
        path: "angular-material-elements",
        loadChildren:
            "./main/angular-material-elements/angular-material-elements.module#AngularMaterialElementsModule"
    },
    {
        path: "inventario-module",
        loadChildren: () => import('./main/modulo-inventario/modulo-inventario.module').then(m => m.ModuloInventarioModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'meu-perfil',
        loadChildren: () => import('./main/user/perfil/perfil.module').then(m => m.PerfilUserModule),
        canActivate: [AuthGuard]
    },
    {
        path: "**",
        redirectTo: "pages/auth/login-2"
    }
];

@NgModule({
    declarations: [ AppComponent ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay: 0,
            passThruUnknownUrl: true
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        AppStoreModule
    ],
    entryComponents: [],
    providers: [
        AuthGuard,
        AuthenticationService,
        PermissionModulesService,
        ExcelService,
        DatePipe,
        //FAKES DB ----- REMOVER DEPOIS QUE A API TIVER FUNCIONANDO
        CurrencyPipe,
        //UsuarioService,
        //ErrorHandlerService,
        //{ provide: ErrorHandler, useClass: ErrorHandlerService},
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
