export class AnalyticsDashboardDb {
    public static projects = [
        {
            'name': 'Comercial'
        },
        {
            'name': 'CRM'
        },
        {
            'name': 'Financeiro'
        },
        {
            'name': 'Estoque'
        },
    ];

    public static widgets = {
        widget1: {
            chartType: 'line',
            datasets: {
                '2016': [
                    {
                        label: 'Sales',
                        data: [1.9, 3, 3.4, 2.2, 2.9, 3.9, 2.5, 3.8, 4.1, 3.8, 3.2, 2.9],
                        fill: 'start'

                    }
                ],
                '2017': [
                    {
                        label: 'Sales',
                        data: [2.2, 2.9, 3.9, 2.5, 3.8, 3.2, 2.9, 1.9, 3, 3.4, 4.1, 3.8],
                        fill: 'start'

                    }
                ],
                '2018': [
                    {
                        label: 'Sales',
                        data: [3.9, 2.5, 3.8, 4.1, 1.9, 3, 3.8, 3.2, 2.9, 3.4, 2.2, 2.9],
                        fill: 'start'

                    }
                ],
                '2019': [
                    {
                        label: 'Sales',
                        data: [3.9, 2.5, 3.8, 4.1, 1.9, 3, 3.8, 3.2, 2.9, 3.4, 2.2, 2.9],
                        fill: 'start'

                    }
                ]

            },
            labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5',
                    pointBackgroundColor: '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    },
                    line: {
                        tension: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18
                            },
                            ticks: {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 1.5,
                                max: 5,
                                stepSize: 0.5
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        },
        widget2: {
            conversion: {
                value: 492,
                ofTarget: 13
            },
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 100,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget3: {
            impressions: {
                value: '87k',
                ofTarget: 12
            },
            chartType: 'line',
            datasets: [
                {
                    label: 'Impression',
                    data: [67000, 54000, 82000, 57000, 72000, 57000, 87000, 72000, 89000, 98700, 112000, 136000, 110000, 149000, 98000],
                    fill: false
                }
            ],
            labels: ['Jan 1', 'Jan 2', 'Jan 3', 'Jan 4', 'Jan 5', 'Jan 6', 'Jan 7', 'Jan 8', 'Jan 9', 'Jan 10', 'Jan 11', 'Jan 12', 'Jan 13', 'Jan 14', 'Jan 15'],
            colors: [
                {
                    borderColor: '#5c84f1'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                elements: {
                    point: {
                        radius: 2,
                        borderWidth: 1,
                        hoverRadius: 2,
                        hoverBorderWidth: 1
                    },
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                // min: 100,
                                // max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget4: {
            visits: {
                value: 882,
                ofTarget: -9
            },
            chartType: 'bar',
            datasets: [
                {
                    label: 'Visits',
                    data: [432, 428, 327, 363, 456, 267, 231]
                }
            ],
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#f44336',
                    backgroundColor: '#f44336'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 150,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget5: {
            chartType: 'line',
            datasets: {
                'yesterday': [
                    {
                        label: 'Sacas',
                        data: [100, 214, 285, 121, 350, 400, 480, 210, 154, 130, 87, 64],
                        fill: 'start'

                    },
                    {
                        label: 'Kilogramas',
                        data: [2000, 4200, 4500, 1980, 4754, 4985, 2315, 1100, 900, 875, 600, 550],
                        fill: 'start'
                    }
                ],
                'today': [
                    {
                        label: 'Sacas',
                        data: [410, 380, 320, 290, 190, 390, 250, 380, 300, 340, 220, 290],
                        fill: 'start'
                    },
                    {
                        label: 'Kilogramas',
                        data: [3000, 3400, 4100, 3800, 2200, 3200, 2900, 1900, 2900, 3900, 2500, 3800],
                        fill: 'start'

                    }
                ]
            },
            labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            colors: [
                {
                    borderColor: '#3949ab',
                    backgroundColor: '#3949ab',
                    pointBackgroundColor: '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: 'rgba(30, 136, 229, 0.87)',
                    backgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            value: {
                VV: {
                    title: 'Valor total vendas',
                    valor: 528954
                },
                VD: {
                    title: 'Valor total despesas',
                    valor: 9548
                },
                VM: {
                    title: 'Valor médio despesas',
                    valor: 795
                }
            },
            options: {
                spanGaps: false,
                gradient: true,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    position: 'nearest',
                    mode: 'index',
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32,
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: true
                            },
                            ticks: {
                                fontColor: 'rgba(0,0,0,0.54)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                stepSize: 1000
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        },
        widget6: {
            markers: [
                {
                    lat: 52,
                    lng: -73,
                    label: '120'
                },
                {
                    lat: 37,
                    lng: -104,
                    label: '498'
                },
                {
                    lat: 21,
                    lng: -7,
                    label: '443'
                },
                {
                    lat: 55,
                    lng: 75,
                    label: '332'
                },
                {
                    lat: 51,
                    lng: 7,
                    label: '50'
                },
                {
                    lat: 31,
                    lng: 12,
                    label: '221'
                },
                {
                    lat: 45,
                    lng: 44,
                    label: '455'
                },
                {
                    lat: -26,
                    lng: 134,
                    label: '231'
                },
                {
                    lat: -9,
                    lng: -60,
                    label: '67'
                },
                {
                    lat: 33,
                    lng: 104,
                    label: '665'
                }
            ],
            styles: [
                {
                    'featureType': 'administrative',
                    'elementType': 'labels.text.fill',
                    'stylers': [
                        {
                            'color': '#444444'
                        }
                    ]
                },
                {
                    'featureType': 'landscape',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'color': '#f2f2f2'
                        }
                    ]
                },
                {
                    'featureType': 'poi',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'road',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'saturation': -100
                        },
                        {
                            'lightness': 45
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'simplified'
                        }
                    ]
                },
                {
                    'featureType': 'road.arterial',
                    'elementType': 'labels.icon',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'transit',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'water',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'color': '#039be5'
                        },
                        {
                            'visibility': 'on'
                        }
                    ]
                }
            ]
        },
        widget7: {
            scheme: {
                domain: ['#00FF7F', '#FF0000']
            },
            devices: [
                {
                    name: 'Vinculado',
                    value: 23570
                },
                {
                    name: 'Não Vinculado',
                    value: 2630
                }
            ]
        },
        widget79: {
            scheme: {
                domain: ['#00FF7F', '#FF0000']
            },
            devices: [
                {
                    name: 'Vinculado',
                    value: 45670
                },
                {
                    name: 'Não Vinculado',
                    value: 21450
                }
            ]
        },
        widget8: {
            title: 'Despesas com prospecção',
            scheme: {
                domain: ['#5c84f1']
            },
            today: '12,540',
            change: {
                value: 321,
                percentage: 2.05
            },
            data: [
                {
                    name: 'Despesas',
                    series: [
                        {
                            name: 'Jan',
                            value: 785
                        },
                        {
                            name: 'Fev',
                            value: 456
                        },
                        {
                            name: 'Mar',
                            value: 325
                        },
                        {
                            name: 'Abr',
                            value: 548
                        },
                        {
                            name: 'Mai',
                            value: 754
                        },
                        {
                            name: 'Jun',
                            value: 895
                        },
                        {
                            name: 'Jul',
                            value: 1523
                        },
                        {
                            name: 'Ago',
                            value: 1125
                        },
                        {
                            name: 'Set',
                            value: 892
                        },
                        {
                            name: 'Out',
                            value: 649
                        },
                        {
                            name: 'Nov',
                            value: 523
                        },
                        {
                            name: 'Dez',
                            value: 385
                        }
                    ]
                }
            ],
            value: {
                valorTotal: {
                    title: 'VALOR TOTAL',
                    totalGastos: 7265
                },
                valorMedio: {
                    title: 'VALOR MÉDIO',
                    mediaGastos: 600
                }
            },
            dataMin: 535,
            dataMax: 545,
        },
        widget9: {
            rows: [
                {
                    title: 'Holiday Travel',
                    clicks: 3621,
                    conversion: 90
                },
                {
                    title: 'Get Away Deals',
                    clicks: 703,
                    conversion: 7
                },
                {
                    title: 'Airfare',
                    clicks: 532,
                    conversion: 0
                },
                {
                    title: 'Vacation',
                    clicks: 201,
                    conversion: 8
                },
                {
                    title: 'Hotels',
                    clicks: 94,
                    conversion: 4
                }
            ]
        },
        'widget10': {
            'title': 'Compra/Venda (em milhões de reais) (EM DESENVOLVIMENTO)',
            'ranges': {
                'TW': 'Filtro 1',
                'LW': 'Filtro 2',
                '2W': 'Filtro 3'
            },
            'mainChart': {
                '2W': [
                    {
                        'name': 'Jan',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 32
                            },
                            {
                                'name': 'compras',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Fev',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 32
                            },
                            {
                                'name': 'compras',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Mar',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 39
                            },
                            {
                                'name': 'compras',
                                'value': 9
                            }
                        ]
                    },
                    {
                        'name': 'Abr',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 27
                            },
                            {
                                'name': 'compras',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Mai',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 18
                            },
                            {
                                'name': 'compras',
                                'value': 7
                            }
                        ]
                    },
                    {
                        'name': 'Jun',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 17
                            },
                            {
                                'name': 'compras',
                                'value': 6
                            }
                        ]
                    },
                    {
                        'name': 'Jul',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 31
                            },
                            {
                                'name': 'compras',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'Ago',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 15
                            },
                            {
                                'name': 'compras',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'Set',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 28
                            },
                            {
                                'name': 'compras',
                                'value': 21
                            }
                        ]
                    },
                    {
                        'name': 'Out',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 20
                            },
                            {
                                'name': 'compras',
                                'value': 13
                            }
                        ]
                    },
                    {
                        'name': 'Nov',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 24
                            },
                            {
                                'name': 'compras',
                                'value': 18
                            }
                        ]
                    },
                    {
                        'name': 'Dez',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 30
                            },
                            {
                                'name': 'compras',
                                'value': 20
                            }
                        ]
                    }
                ],
                'LW': [
                    {
                        'name': 'Jan',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 37
                            },
                            {
                                'name': 'compras',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Fev',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 24
                            },
                            {
                                'name': 'compras',
                                'value': 8
                            }
                        ]
                    },
                    {
                        'name': 'Mar',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 51
                            },
                            {
                                'name': 'compras',
                                'value': 7
                            }
                        ]
                    },
                    {
                        'name': 'Abr',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 31
                            },
                            {
                                'name': 'compras',
                                'value': 13
                            }
                        ]
                    },
                    {
                        'name': 'Mai',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 29
                            },
                            {
                                'name': 'compras',
                                'value': 7
                            }
                        ]
                    },
                    {
                        'name': 'Jun',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 17
                            },
                            {
                                'name': 'compras',
                                'value': 6
                            }
                        ]
                    },
                    {
                        'name': 'Jul',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 31
                            },
                            {
                                'name': 'compras',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'Ago',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 15
                            },
                            {
                                'name': 'compras',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'Set',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 28
                            },
                            {
                                'name': 'compras',
                                'value': 21
                            }
                        ]
                    },
                    {
                        'name': 'Out',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 20
                            },
                            {
                                'name': 'compras',
                                'value': 13
                            }
                        ]
                    },
                    {
                        'name': 'Nov',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 24
                            },
                            {
                                'name': 'compras',
                                'value': 18
                            }
                        ]
                    },
                    {
                        'name': 'Dez',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 30
                            },
                            {
                                'name': 'compras',
                                'value': 20
                            }
                        ]
                    }
                ],
                'TW': [
                    {
                        'name': 'Jan',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 42
                            },
                            {
                                'name': 'compras',
                                'value': 11
                            }
                        ]
                    },
                    {
                        'name': 'Fev',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 28
                            },
                            {
                                'name': 'compras',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'Mar',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 43
                            },
                            {
                                'name': 'compras',
                                'value': 8
                            }
                        ]
                    },
                    {
                        'name': 'Abr',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 34
                            },
                            {
                                'name': 'compras',
                                'value': 11
                            }
                        ]
                    },
                    {
                        'name': 'Mai',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 20
                            },
                            {
                                'name': 'compras',
                                'value': 8
                            }
                        ]
                    },
                    {
                        'name': 'Jun',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 25
                            },
                            {
                                'name': 'compras',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'Jul',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 22
                            },
                            {
                                'name': 'compras',
                                'value': 17
                            }
                        ]
                    },
                    {
                        'name': 'Ago',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 15
                            },
                            {
                                'name': 'compras',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'Set',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 28
                            },
                            {
                                'name': 'compras',
                                'value': 21
                            }
                        ]
                    },
                    {
                        'name': 'Out',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 20
                            },
                            {
                                'name': 'compras',
                                'value': 13
                            }
                        ]
                    },
                    {
                        'name': 'Nov',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 24
                            },
                            {
                                'name': 'compras',
                                'value': 18
                            }
                        ]
                    },
                    {
                        'name': 'Dez',
                        'series': [
                            {
                                'name': 'vendas',
                                'value': 50
                            },
                            {
                                'name': 'compras',
                                'value': 20
                            }
                        ]
                    }
                ]
            },
            'supporting': {
                'created': {
                    'label': 'TOTAL DE VENDAS',
                    'count': {
                        '2W': '150.325,54',
                        'LW': '50.262,51',
                        'TW': '75.542,87'
                    },
                    'chart': {
                        '2W': [
                            {
                                'name': 'CREATED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'LW': [
                            {
                                'name': 'Created',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'TW': [
                            {
                                'name': 'Created',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 2
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 1
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 4
                                    }
                                ]
                            }
                        ]
                    }
                },
                'reOpened': {
                    'label': 'TOTAL DE COMPRAS',
                    'count': {
                        '2W': '15.254,12',
                        'LW': '4.256,84',
                        'TW': '7.745,92'
                    },
                    'chart': {
                        '2W': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'LW': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 1
                                    }
                                ]
                            }
                        ],
                        'TW': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 2
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 1
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 4
                                    }
                                ]
                            }
                        ]
                    }
                },

            }
        },
        'widget11': {
            'title': 'Próximas entregas',
            'table': {
                'columns': [
                    {
                        'title': 'Data'
                    },
                    {
                        'title': 'CTR'
                    },
                    {
                        'title': 'Cliente'
                    },
                    {
                        'title': 'Quantidade'
                    },
                    {
                        'title': 'Status'
                    }
                ],
                'rows': [
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '18/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0002',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Olam',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Pendente',
                            'classes': 'orange',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '19/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0003',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'COCAPEC',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '500',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Pronta',
                            'classes': 'accent',
                            'icon': 'trending_up'
                        }
                    ],
                    [
                        {
                            'value': '20/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0004',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'COOMAP',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '650',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Pendente',
                            'classes': 'orange',
                            'icon': 'trending_up'
                        }
                    ],
                    [
                        {
                            'value': '23/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0005',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '450',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Amostra',
                            'classes': 'primary',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ],
                    [
                        {
                            'value': '17/10/19',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '0001',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Brasilsync',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '250',
                            'classes': '',
                            'icon': ''
                        },
                        {
                            'value': 'Concluído',
                            'classes': 'green',
                            'icon': 'trending_flat'
                        }
                    ]
                ]
            }
        },
        'widget12': {
            'title': 'Total comercializado por tipo de café (EM DESENVOLVIMENTO)',
            'ranges': {
                'CP': 'Compra',
                'VD': 'Venda'
            },
            'mainChart': {
                'CP': [
                    {
                        'name': 'MK',
                        'value': 25
                    },
                    {
                        'name': 'B/C',
                        'value': 25
                    },
                    {
                        'name': 'P17',
                        'value': 18
                    },
                    {
                        'name': 'P18',
                        'value': 16
                    },
                    {
                        'name': 'P19',
                        'value': 15
                    }
                ],
                'VD': [
                    {
                        'name': 'MK',
                        'value': 50
                    },
                    {
                        'name': 'B/C',
                        'value': 64
                    },
                    {
                        'name': 'P17',
                        'value': 35
                    },
                    {
                        'name': 'P18',
                        'value': 32
                    },
                    {
                        'name': 'P19',
                        'value': 28
                    }
                ]
            },
            'footerLeft': {
                'title': 'Total',
                'count': {
                    'CP': 99,
                    'VD': 209
                }
            },
            'footerRight': {
                'title': 'Tasks Completed',
                'count': {
                    'CP': 99,
                    'VD': 260
                }
            }
        },
        'widget13': {
            'title': 'Peneiras',
            'ranges': {
                'DP': 'Disponível',
                'BL': 'Bloqueado'
            },
            'mainChart': {
                'DP': [
                    {
                        'name': 'P17',
                        'value': 20
                    },
                    {
                        'name': 'P19',
                        'value': 15
                    },
                    {
                        'name': 'MK',
                        'value': 10
                    },
                    {
                        'name': 'K',
                        'value': 10
                    },
                    {
                        'name': 'BC',
                        'value': 45
                    }
                ],
                'BL': [
                    {
                        'name': 'P17',
                        'value': 20
                    },
                    {
                        'name': 'P19',
                        'value': 15
                    },
                    {
                        'name': 'MK',
                        'value': 10
                    },
                    {
                        'name': 'K',
                        'value': 10
                    },
                    {
                        'name': 'BC',
                        'value': 45
                    }
                ]
            },
            'footerLeft': {
                'title': 'Total',
                'count': {
                    'DP': 100,
                    'BL': 100
                }
            },
            'footerRight': {
                'title': 'Tasks Completed',
                'count': {
                    'DP': 99,
                    'BL': 99
                }
            }
        },
        'widget14': {
            'title': 'Bebidas',
            'ranges': {
                'DP': 'Disponível',
                'BL': 'Bloqueado'
            },
            'mainChart': {
                'DP': [
                    {
                        'name': 'Dura',
                        'value': 20
                    },
                    {
                        'name': 'Mole',
                        'value': 15
                    },
                    {
                        'name': 'Riada',
                        'value': 40
                    },
                    {
                        'name': 'Rio',
                        'value': 10
                    }
                ],
                'BL': [
                    {
                        'name': 'Dura',
                        'value': 20
                    },
                    {
                        'name': 'Mole',
                        'value': 25
                    },
                    {
                        'name': 'Riada',
                        'value': 15
                    },
                    {
                        'name': 'Rio',
                        'value': 15
                    },
                    {
                        'name': 'Café Arábica',
                        'value': 25
                    }
                ]
            },
            'footerLeft': {
                'title': 'Total',
                'count': {
                    'DP': 100,
                    'BL': 100
                }
            },
            'footerRight': {
                'title': 'Tasks Completed',
                'count': {
                    'DP': 99,
                    'BL': 99
                }
            }
        },
        'widget15': {
            'title': 'Certificados',
            'ranges': {
                'DP': 'Disponível',
                'BL': 'Bloqueado'
            },
            'mainChart': {
                'DP': [
                    {
                        'name': '4C',
                        'value': 10
                    },
                    {
                        'name': 'FT',
                        'value': 15
                    },
                    {
                        'name': 'UTZ',
                        'value': 75
                    }
                ],
                'BL': [
                    {
                        'name': '4C',
                        'value': 15
                    },
                    {
                        'name': 'FT',
                        'value': 20
                    },
                    {
                        'name': 'UTZ',
                        'value': 65
                    }
                ]
            },
            'footerLeft': {
                'title': 'Total',
                'count': {
                    'DP': 100,
                    'BL': 100
                }
            },
            'footerRight': {
                'title': 'Tasks Completed',
                'count': {
                    'DP': 99,
                    'BL': 99
                }
            }
        },
        'widget16': {
            'title': 'Peneira/Bebida',
            'ranges': {
                'TW': 'Filtro 1',
                'LW': 'Filtro 2',
                '2W': 'Filtro 3'
            },
            'mainChart': {
                '2W': [
                    {
                        'name': 'BC',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 30
                            },
                            {
                                'name': 'Mole',
                                'value': 15
                            },
                            {
                                'name': 'Riada',
                                'value': 42
                            },
                            {
                                'name': 'Rio',
                                'value': 10
                            }
                        ]
                    },
                    {
                        'name': 'P17',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 19
                            },
                            {
                                'name': 'Mole',
                                'value': 13
                            },
                            {
                                'name': 'Riada',
                                'value': 48
                            },
                            {
                                'name': 'Rio',
                                'value': 9
                            }
                        ]
                    },
                    {
                        'name': 'P19',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 34
                            },
                            {
                                'name': 'Mole',
                                'value': 12
                            },
                            {
                                'name': 'Riada',
                                'value': 26
                            },
                            {
                                'name': 'Rio',
                                'value': 21
                            }
                        ]
                    },
                    {
                        'name': 'MK',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 37
                            },
                            {
                                'name': 'Mole',
                                'value': 22
                            },
                            {
                                'name': 'Riada',
                                'value': 45
                            },
                            {
                                'name': 'Rio',
                                'value': 17
                            }
                        ]
                    },
                    {
                        'name': 'K',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 14
                            },
                            {
                                'name': 'Mole',
                                'value': 33
                            },
                            {
                                'name': 'Riada',
                                'value': 24
                            },
                            {
                                'name': 'Rio',
                                'value': 19
                            }
                        ]
                    }
                ],
                'LW': [
                    {
                        'name': 'BC',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 23
                            },
                            {
                                'name': 'Mole',
                                'value': 12
                            },
                            {
                                'name': 'Riada',
                                'value': 47
                            },
                            {
                                'name': 'Rio',
                                'value': 13
                            }
                        ]
                    },
                    {
                        'name': 'P17',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 24
                            },
                            {
                                'name': 'Mole',
                                'value': 17
                            },
                            {
                                'name': 'Riada',
                                'value': 45
                            },
                            {
                                'name': 'Rio',
                                'value': 14
                            }
                        ]
                    },
                    {
                        'name': 'P19',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 23
                            },
                            {
                                'name': 'Mole',
                                'value': 28
                            },
                            {
                                'name': 'Riada',
                                'value': 21
                            },
                            {
                                'name': 'Rio',
                                'value': 17
                            }
                        ]
                    },
                    {
                        'name': 'MK',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 30
                            },
                            {
                                'name': 'Mole',
                                'value': 27
                            },
                            {
                                'name': 'Riada',
                                'value': 42
                            },
                            {
                                'name': 'Rio',
                                'value': 23
                            }
                        ]
                    },
                    {
                        'name': 'K',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 19
                            },
                            {
                                'name': 'Mole',
                                'value': 24
                            },
                            {
                                'name': 'Riada',
                                'value': 36
                            },
                            {
                                'name': 'Rio',
                                'value': 17
                            }
                        ]
                    }
                ],
                'TW': [
                    {
                        'name': 'BC',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 23
                            },
                            {
                                'name': 'Mole',
                                'value': 18
                            },
                            {
                                'name': 'Riada',
                                'value': 31
                            },
                            {
                                'name': 'Rio',
                                'value': 7
                            }
                        ]
                    },
                    {
                        'name': 'P17',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 25
                            },
                            {
                                'name': 'Mole',
                                'value': 18
                            },
                            {
                                'name': 'Riada',
                                'value': 41
                            },
                            {
                                'name': 'Rio',
                                'value': 15
                            }
                        ]
                    },
                    {
                        'name': 'P19',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 39
                            },
                            {
                                'name': 'Mole',
                                'value': 15
                            },
                            {
                                'name': 'Riada',
                                'value': 21
                            },
                            {
                                'name': 'Rio',
                                'value': 17
                            }
                        ]
                    },
                    {
                        'name': 'MK',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 43
                            },
                            {
                                'name': 'Mole',
                                'value': 18
                            },
                            {
                                'name': 'Riada',
                                'value': 37
                            },
                            {
                                'name': 'Rio',
                                'value': 13
                            }
                        ]
                    },
                    {
                        'name': 'K',
                        'series': [
                            {
                                'name': 'Dura',
                                'value': 18
                            },
                            {
                                'name': 'Mole',
                                'value': 30
                            },
                            {
                                'name': 'Riada',
                                'value': 21
                            },
                            {
                                'name': 'Rio',
                                'value': 28
                            }
                        ]
                    }
                ]
            },
            'supporting': {
                'created': {
                    'label': 'TOTAL DE VENDAS',
                    'count': {
                        '2W': '150.325,54',
                        'LW': '50.262,51',
                        'TW': '75.542,87'
                    },
                    'chart': {
                        '2W': [
                            {
                                'name': 'CREATED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'LW': [
                            {
                                'name': 'Created',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'TW': [
                            {
                                'name': 'Created',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 2
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 1
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 4
                                    }
                                ]
                            }
                        ]
                    }
                },
                // 'closed'   : {
                //     'label': 'CLOSED',
                //     'count': {
                //         '2W': 27,
                //         'LW': 31,
                //         'TW': 26
                //     },
                //     'chart': {
                //         '2W': [
                //             {
                //                 'name'  : 'CLOSED',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 3
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 2
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 1
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 8
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 8
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 4
                //                     }
                //                 ]
                //             }
                //         ],
                //         'LW': [
                //             {
                //                 'name'  : 'CLOSED',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 7
                //                     }
                //                 ]
                //             }
                //         ],
                //         'TW': [
                //             {
                //                 'name'  : 'CLOSED',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 3
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 7
                //                     }
                //                 ]
                //             }
                //         ]
                //     }
                // },
                'reOpened': {
                    'label': 'TOTAL DE COMPRAS',
                    'count': {
                        '2W': '15.254,12',
                        'LW': '4.256,84',
                        'TW': '7.745,92'
                    },
                    'chart': {
                        '2W': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'LW': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 1
                                    }
                                ]
                            }
                        ],
                        'TW': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 2
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 1
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 4
                                    }
                                ]
                            }
                        ]
                    }
                },
                // 'wontFix'  : {
                //     'label': 'WON\'T FIX',
                //     'count': {
                //         '2W': 6,
                //         'LW': 3,
                //         'TW': 4
                //     },
                //     'chart': {
                //         '2W': [
                //             {
                //                 'name'  : 'WON\'T FIX',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 3
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 2
                //                     }
                //                 ]
                //             }
                //         ],
                //         'LW': [
                //             {
                //                 'name'  : 'WON\'T FIX',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 3
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 7
                //                     }
                //                 ]
                //             }
                //         ],
                //         'TW': [
                //             {
                //                 'name'  : 'WON\'T FIX',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 7
                //                     }
                //                 ]
                //             }
                //         ]
                //     }
                // },
                // 'needsTest': {
                //     'label': 'NEEDS TEST',
                //     'count': {
                //         '2W': 10,
                //         'LW': 7,
                //         'TW': 8
                //     },
                //     'chart': {
                //         '2W': [
                //             {
                //                 'name'  : 'NEEDS TEST',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 7
                //                     }
                //                 ]
                //             }
                //         ],
                //         'LW': [
                //             {
                //                 'name'  : 'NEEDS TEST',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 8
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 8
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 1
                //                     }
                //                 ]
                //             }
                //         ],
                //         'TW': [
                //             {
                //                 'name'  : 'NEEDS TEST',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 3
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 7
                //                     }
                //                 ]
                //             }
                //         ]
                //     }
                // }
                // 'fixed'    : {
                //     'label': 'FIXED',
                //     'count': {
                //         '2W': 21,
                //         'LW': 17,
                //         'TW': 14
                //     },
                //     'chart': {
                //         '2W': [
                //             {
                //                 'name'  : 'FIXED',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 8
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 8
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 1
                //                     }
                //                 ]
                //             }
                //         ],
                //         'LW': [
                //             {
                //                 'name'  : 'FIXED',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 7
                //                     }
                //                 ]
                //             }
                //         ],
                //         'TW': [
                //             {
                //                 'name'  : 'FIXED',
                //                 'series': [
                //                     {
                //                         'name' : 'Mon',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Tue',
                //                         'value': 7
                //                     },
                //                     {
                //                         'name' : 'Wed',
                //                         'value': 4
                //                     },
                //                     {
                //                         'name' : 'Thu',
                //                         'value': 6
                //                     },
                //                     {
                //                         'name' : 'Fri',
                //                         'value': 5
                //                     },
                //                     {
                //                         'name' : 'Sat',
                //                         'value': 3
                //                     },
                //                     {
                //                         'name' : 'Sun',
                //                         'value': 2
                //                     }
                //                 ]
                //             }
                //         ]
                //     }
                // }
            }
        },
        'widget17': {
            'title': 'Compra de café certificado',
            'table': {
                'columns': [
                    {
                        'title': 'Certificação'
                    },
                    {
                        'title': 'Sacas'
                    },
                    {
                        'title': 'Valor'
                    }
                ],
                'rows': [
                    [
                        {
                            'value': 'Fairtrade',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '2500',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '15.250',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'UTZ',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '2800',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '17.458',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': '4C',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '1225',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '9.189',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'Forest',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '875',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '8.452',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'Sem certificação',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '8458',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '25.634',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ]
                ]
            }
        },
        'widget18': {
            'title': 'Venda de café certificado',
            'ranges': {
                'EF': 'Efetuado',
                'FT': 'Futuro'
            },
            'table': {
                'columns': [
                    {
                        'title': 'Certificação'
                    },
                    {
                        'title': 'Sacas'
                    },
                    {
                        'title': 'Valor'
                    }
                ]
            },
            'rows': {
                'EF': [
                    [
                        {
                            'value': 'Fairtrade',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '4685',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '32.355',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'UTZ',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '5215',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '28.448',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': '4C',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '3154',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '18.547',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'Forest',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '1254',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '13.294',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'Sem certificação',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '18458',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '215.274',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ]
                ],
                'FT': [
                    [
                        {
                            'value': 'Fairtrade',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '5879',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '15.541',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'UTZ',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '4257',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '18.415',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': '4C',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '3562',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '18.189',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'Forest',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '1542',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '13.548',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ],
                    [
                        {
                            'value': 'Sem certificação',
                            'classes': 'text-bold',
                            'icon': ''
                        },
                        {
                            'value': '18645',
                            'classes': 'text-success',
                            'icon': ''
                        },
                        {
                            'value': '45.637',
                            'classes': 'text-success',
                            'icon': ''
                        }
                    ]
                ]
            }

        }, 
        'widget19': {
            'title': 'Operações',
            'ranges': {
                'OP': 'Operação'
            },
            'mainChart': {
                'OP': [
                    {
                        'name': 'ACC',
                        'value': 8287
                    },
                    {
                        'name': 'Hedge',
                        'value': 4259
                    }
                ]
            },
            'footerLeft': {
                'title': 'Total',
                'count': {
                    'OP': 12546
                }
            }
        },
        'widget20': {
            'title': 'Estoque',
            'ranges': {
                'AT': 'Atual',
                'RS': 'Reservado',
                'BL': 'Bloqueado'
            },
            'mainChart': {
                'AT': [
                    {
                        'name': 'Próprio',
                        'value': 250000,
                        'id': 1
                    },
                    {
                        'name': 'Terceiro',
                        'value': 120000,
                        'id': 2
                    },
                    {
                        'name': 'Cooperado',
                        'value': 220000,
                        'id': 3
                    },
                    {
                        'name': 'Batter',
                        'value': 150000,
                        'id': 4
                    }
                ],
                'RS': [
                    {
                        'name': 'Embarque',
                        'value': 25000,
                        'id': 1
                    },
                    {
                        'name': 'Maquinação',
                        'value': 12546,
                        'id': 1
                    }
                ],
                'BL': [
                    {
                        'name': 'Total',
                        'value': 5587,
                        'id': 1
                    }
                ]

            },
            'footerLeft': {
                'title': 'Total',
                'count': {
                    'AT': 740000,
                    'RS': 37546,
                    'BL': 5587
                }
            }
        },
        widget21: {
            chartType: 'line',
            datasets: {
                chart: [
                    {
                        label: 'Despesas',
                        data: [100, 214, 285, 121, 350, 400, 480, 210, 154, 130, 87, 64],
                        fill: 'start'

                    },
                    {
                        label: 'Vendas',
                        data: [2000, 4200, 4500, 1980, 4754, 4800, 2315, 1100, 900, 875, 600, 550],
                        fill: 'start'
                    }
                ],
            },
            labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            colors: [
                {
                    borderColor: '#3949ab',
                    backgroundColor: '#3949ab',
                    pointBackgroundColor: '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: 'rgba(30, 136, 229, 0.87)',
                    backgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            value: {
                VV: {
                    title: 'Valor total vendas',
                    valor: 528954
                },
                VD: {
                    title: 'Valor total despesas',
                    valor: 9548
                },
                VM: {
                    title: 'Valor médio despesas',
                    valor: 795
                }
            },
            options: {
                spanGaps: false,
                gradient: true,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                roundDomains: true,
                tooltips: {
                    position: 'nearest',
                    mode: 'index',
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32,
                        bottom: 16
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: true
                            },
                            ticks: {
                                fontColor: 'rgba(0,0,0,0.54)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                stepSize: 1000
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        },
        'widget22': {
            'title': 'Total de visitas e negociações efetivadas',
            'ranges': {
                'TW': 'Filtro 1',
                'LW': 'Filtro 2',
                '2W': 'Filtro 3'
            },
            'mainChart': {
                '2W': [
                    {
                        'name': 'Junho',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 30
                            },
                            {
                                'name': 'Negociações',
                                'value': 15
                            }
                        ]
                    },
                    {
                        'name': 'Julho',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 19
                            },
                            {
                                'name': 'Negociações',
                                'value': 13
                            }
                        ]
                    },
                    {
                        'name': 'Agosto',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 34
                            },
                            {
                                'name': 'Negociações',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Setembro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 37
                            },
                            {
                                'name': 'Negociações',
                                'value': 22
                            }
                        ]
                    },
                    {
                        'name': 'Outubro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 33
                            },
                            {
                                'name': 'Negociações',
                                'value': 18
                            }
                        ]
                    },
                    {
                        'name': 'Novembro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 12
                            },
                            {
                                'name': 'Negociações',
                                'value': 4
                            }
                        ]
                    }
                ],
                'LW': [
                    {
                        'name': 'Junho',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 23
                            },
                            {
                                'name': 'Negociações',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Julho',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 24
                            },
                            {
                                'name': 'Negociações',
                                'value': 17
                            }
                        ]
                    },
                    {
                        'name': 'Agosto',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 28
                            },
                            {
                                'name': 'Negociações',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Setembro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 30
                            },
                            {
                                'name': 'Negociações',
                                'value': 25
                            }
                        ]
                    },
                    {
                        'name': 'Outubro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 19
                            },
                            {
                                'name': 'Negociações',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Novembro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 24
                            },
                            {
                                'name': 'Negociações',
                                'value': 19
                            }
                        ]
                    }
                ],
                'TW': [
                    {
                        'name': 'Juho',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 23
                            },
                            {
                                'name': 'Negociações',
                                'value': 18
                            }
                        ]
                    },
                    {
                        'name': 'Julho',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 25
                            },
                            {
                                'name': 'Negociações',
                                'value': 19
                            }
                        ]
                    },
                    {
                        'name': 'Agosto',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 33
                            },
                            {
                                'name': 'Negociações',
                                'value': 14
                            }
                        ]
                    },
                    {
                        'name': 'Setembro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 43
                            },
                            {
                                'name': 'Negociações',
                                'value': 33
                            }
                        ]
                    },
                    {
                        'name': 'Outubro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 22
                            },
                            {
                                'name': 'Negociações',
                                'value': 12
                            }
                        ]
                    },
                    {
                        'name': 'Novembro',
                        'series': [
                            {
                                'name': 'Visitas',
                                'value': 26
                            },
                            {
                                'name': 'Negociações',
                                'value': 22
                            }
                        ]
                    }
                ]
            },
            'supporting': {
                'created': {
                    'label': 'TOTAL DE VENDAS',
                    'count': {
                        '2W': '150.325,54',
                        'LW': '50.262,51',
                        'TW': '75.542,87'
                    },
                    'chart': {
                        '2W': [
                            {
                                'name': 'CREATED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'LW': [
                            {
                                'name': 'Created',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'TW': [
                            {
                                'name': 'Created',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 2
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 1
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 4
                                    }
                                ]
                            }
                        ]
                    }
                },
                'reOpened': {
                    'label': 'TOTAL DE COMPRAS',
                    'count': {
                        '2W': '15.254,12',
                        'LW': '4.256,84',
                        'TW': '7.745,92'
                    },
                    'chart': {
                        '2W': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 7
                                    }
                                ]
                            }
                        ],
                        'LW': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 5
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 7
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 6
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 1
                                    }
                                ]
                            }
                        ],
                        'TW': [
                            {
                                'name': 'RE-OPENED',
                                'series': [
                                    {
                                        'name': 'Mon',
                                        'value': 3
                                    },
                                    {
                                        'name': 'Tue',
                                        'value': 2
                                    },
                                    {
                                        'name': 'Wed',
                                        'value': 1
                                    },
                                    {
                                        'name': 'Thu',
                                        'value': 4
                                    },
                                    {
                                        'name': 'Fri',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sat',
                                        'value': 8
                                    },
                                    {
                                        'name': 'Sun',
                                        'value': 4
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        },
        widget23: {
            ranges: {
                'GE': 'Ger',
                'MI': 'Int',
                'ME': 'Ext'
            },
            content: {
                'MI': {
                    title: 'Mercado Interno',
                    value: 542,
                    ofTarget: 6
                },
                'ME': {
                    title: 'Mercado Externo',
                    value: 238,
                    ofTarget: -3
                },
                'GE': {
                    title: 'Mercado Geral',
                    value: 780,
                    ofTarget: 1.5
                }
            }
        },
        widget24: {
            ranges: {
                'GE': 'Ger',
                'MI': 'Int',
                'ME': 'Ext'
            },
            content: {
                'MI': {
                    title: 'Mercado Interno',
                    value: 56,
                    ofTarget: 11
                },
                'ME': {
                    title: 'Mercado Externo',
                    value: 12,
                    ofTarget: 2
                },
                'GE': {
                    title: 'Mercado Geral',
                    value: 68,
                    ofTarget: 7.5
                }
            }
        },
        widget25: {
            ranges: {
                'GE': 'Ger',
                'MI': 'Int',
                'ME': 'Ext'
            },
            content: {
                'MI': {
                    title: 'Mercado Interno',
                    value: 105,
                    ofTarget: -2
                },
                'ME': {
                    title: 'Mercado Externo',
                    value: 83,
                    ofTarget: 10
                },
                'GE': {
                    title: 'Mercado Geral',
                    value: 188,
                    ofTarget: 4
                }
            }
        },
        widget26: {
            ranges: {
                'GE': 'Ger',
                'MI': 'Int',
                'ME': 'Ext'
            },
            content: {
                'MI': {
                    valorTitle: 'Valor total',
                    sacasTitle: 'Sacas',
                    sacas: 1256,
                    value: 15215,
                    ofTarget: 5
                },
                'ME': {
                    valorTitle: 'Valor total',
                    sacasTitle: 'Sacas',
                    sacas: 857,
                    value: 12534,
                    ofTarget: 7
                },
                'GE': {
                    valorTitle: 'Valor total',
                    sacasTitle: 'Sacas',
                    sacas: 2113,
                    value: 27749,
                    ofTarget: 6
                }
            }
        },
        widget27: {
            content: {
                valorTitle: 'Valores negociados',
                sacasTitle: 'Sacas negociadas',
                sacas: 154125,
                value: 10254156,
                ofTarget: 15
            }
        },
        widget28: {
            content: {
                negociacoes: 'Negociações efetuadas',
                clientes: 'Clientes visitados',
                negEfet: 85,
                cliVis: 135,
                ofTarget: -2
            }
        },
        widget29: {
            content: {
                despesas: 'Despesas com amostras',
                mediaDespesas: 'Média de despesas',
                despValor: 2564,
                medValor: 265,
                ofTarget: 3
            }
        },
        widget30: {
            content: {
                amostras: 'Amostras ofertadas',
                negociacoes: 'Negociações efetivadas',
                amostValor: 1542,
                negValor: 1254,
                ofTarget: 15
            }
        }

    }


};

